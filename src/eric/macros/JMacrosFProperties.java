/* 
 
Copyright 2006 Eric Hakenholz

This file is part of C.a.R. software.

    C.a.R. is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    C.a.R. is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */
 
 
 package eric.macros;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;

import javax.swing.JComponent;
import eric.JEricPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import rene.gui.Global;

public class JMacrosFProperties extends JEricPanel {
	/**
	 * 
	 */
	
	private final JTable table;
	private final DefaultTableModel model;
	private final JMacrosInspector JMI;

	public JMacrosFProperties(final JMacrosInspector jmi) {
		super(new GridLayout(1, 0));
		JMI = jmi;
		this.setFocusable(false);
		model = new DefaultTableModel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Class getColumnClass(final int columnIndex) {
				if ((columnIndex == 2) || (columnIndex == 3)) {
					return Integer.class;
				} else {
					return super.getColumnClass(columnIndex);
				}
			}
		};

		table = new JTable(model) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(final int row, final int col) {
					return false;
			}

			@Override
			public boolean isCellSelected(final int row, final int col) {
				return false;
			}


			@Override
			public TableCellRenderer getCellRenderer(final int row,
					final int column) {
				final Object value = getValueAt(row, column);
				if (value != null) {
					return getDefaultRenderer(value.getClass());
				}
				return getDefaultRenderer(String.class);
			}
		};
		table.setCellSelectionEnabled(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setShowGrid(true);
		table.setGridColor(Color.lightGray);
		table.setRowHeight(20);

		table.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0, false), "none");
		table.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(
				KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), "none");

		model.addColumn(Global.Loc("mi.tab.type"));
		model.addColumn(Global.Loc("mi.tab.name"));
		model.addColumn("Construction index");		// Dibs : Construction index -> ExecuteMacro
		model.addColumn("Click index");			//        Click index  -> ExecuteMacroAsBuilt
		

		// model.addRow(new Object[]{new String(""),new String(""),new
		// String(""),new Boolean(false), new Boolean(false)});

		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		table.getColumnModel().getColumn(0).setPreferredWidth(45);
		table.getColumnModel().getColumn(1).setPreferredWidth(45);
		table.getColumnModel().getColumn(2).setPreferredWidth(112);
		table.getColumnModel().getColumn(3).setPreferredWidth(80);

		final JScrollPane scrollPane = new JScrollPane(table);
		scrollPane
		.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		add(scrollPane);
	}

	public int getRowCount() {

		return model.getRowCount();
	}

	public Object getValueAt(final int i, final int j) {
		return model.getValueAt(i, j);
	}

	public void stopCellEditing() {
		final TableCellEditor editor = table.getCellEditor();
		if (editor != null) {
			editor.stopCellEditing();
		}
	}

	public String getOType(final int row) {
		return (String) model.getValueAt(row, 0);
	}

	public String getOName(final int row) {
		return (String) model.getValueAt(row, 1);
	}

	public String getOFIndex(final int row) {
		return (String) model.getValueAt(row, 2);
	}

	public String getOFIndex2(final int row) {
		return (String) model.getValueAt(row, 3);
	}


	public void setOType(final String what, final int row) {
		model.setValueAt(new String(what), row, 0);
	}

	public void setOName(final String what, final int row) {
		model.setValueAt(new String(what), row, 1);
	}


	public void addRow(final String type, final String name,
			final int index, final int index2) {
		model.addRow(new Object[] { new String(type), new String(name),
				new String(String.valueOf(index)), new String(String.valueOf(index2)) });
	}

	public void removeAllRows() {
		while (model.getRowCount() > 0)
			model.removeRow(0);
	}

}
