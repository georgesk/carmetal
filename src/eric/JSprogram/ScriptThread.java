/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eric.JSprogram;

import eric.GUI.palette.JIcon;
import eric.GUI.palette.PaletteManager;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eric.JZirkelCanvas;
import java.util.ArrayList;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.ContextFactory;
import org.mozilla.javascript.RhinoException;
import org.mozilla.javascript.Script;
import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.ImporterTopLevel;
import rene.gui.Global;
import rene.zirkel.ZirkelCanvas;
import rene.zirkel.construction.Construction;
import rene.zirkel.objects.ConstructionObject;

/**
 *
 * @author erichake with addons by Patrice Debrabant
 */
public class ScriptThread extends Thread {

    private volatile Context CX=null;
    private volatile ScriptableObject SCOPE;
    private String SCRIPT="";
    private ScriptItem ITEM;
    private Pattern p;
    private Matcher m;
    private boolean matchFound = false;
    private ZirkelCanvas ZC;
    private Construction C;
    private volatile ConstructionObject JSO=null; // only for InteractiveInput
    private volatile boolean validII=true; // only for InteractiveInput
    private volatile boolean isRunning=false;
    private volatile boolean isActionScript=false;
    private volatile boolean busyActionScript=false;
    private volatile boolean executeActionScript=false;
    private volatile boolean actionScriptInProgress=false;
    private volatile boolean killme=false;
    private volatile boolean stopme=false;
    private JSOuputConsole CONSOLE=new JSOuputConsole();
    private int nbRepeter=0;

    public ScriptThread(ScriptItem item) {
        super();
        ITEM=item;
        ZC=JZirkelCanvas.getCurrentZC();
        C=ZC.getConstruction();
    }

    public ZirkelCanvas getZC() {
        return ZC;
    }

    public Construction getC() {
        return C;
    }

    public ScriptableObject getSCOPE() {
        return SCOPE;
    }

    public JSOuputConsole getCONSOLE() {
        return CONSOLE;
    }

    public void setFileName(String name) {
        ITEM.setFileName(name);
    }

    public String getFileName() {
        return ITEM.getFileName();
    }

    public void openEditor() {
        ITEM.openEditor();
    }

    // setJSO,getJSO,setValidII,getValidII only for InteractiveInput :
    public void setJSO(ConstructionObject o){
        JSO=o;
    }
    public ConstructionObject getJSO(){
        return JSO;
    }
    public void invalidII(){
        stopme=false;
        validII=false;
    }
    public boolean isValidII() {
        return validII;
    }

    @Override
    public void run() {
        try {
            isRunning=true;

            CX=Context.enter();
            CX.setOptimizationLevel(9);
            SCOPE = new ImporterTopLevel(CX);
            SCOPE.defineFunctionProperties(JSFunctions.getKeywords(), JSFunctions.class, ScriptableObject.DONTENUM);

            if (isActionScript) {
                Script scp=CX.compileString(SCRIPT, ITEM.getScriptName(),0, null);
                while (busyActionScript) {
                    if (executeActionScript) {
                        executeActionScript=false;
                        try {
                            scp.exec(CX, SCOPE);
//                            CX.evaluateString(SCOPE, SCRIPT, ITEM.getScriptName(), 0, null);
                        } catch (Error er) {
                        }
                    }
                }
            } else {
                try {
                    CX.evaluateString(SCOPE, SCRIPT, ITEM.getScriptName(), 0, null);
                } catch (Error er) {
                    System.out.println("error !");
                }
            }
        } catch (RhinoException e) {
            ITEM.sendErrorToEditor(e.getMessage());
        } catch (Exception e) {
        } finally {
            Context.exit();
            ZC.dovalidate();
            ZC.repaint();
            isRunning=false;
            Global.loadProperties(Global.getHomeDirectory()+"carmetal_config.txt");
            Global.initProperties();
            eric.JGlobalPreferences.initPreferences();
            PaletteManager.setSelected_with_clic("move",true);
        }
    }


    public boolean isRunning() {
        return isRunning;
    }
    //ajout PM
    public boolean isStopped(){
	return stopme;
    }

    public void stopme() {
        if (isRunning) {
            stopme=true;
        }
    }

    public void restartme() {
        stopme=false;
    }

    public void killme() {
        validII=false; // Au cas où le kill intervient en plein InteractiveInput
        stopme=false;
        killme=true;
        interrupt();
    }

    public void runme() {
        Global.saveProperties("CaR Properties");
    	SCRIPT=ITEM.getScriptSource();
        p = Pattern.compile("^pseudo-code");
        m = p.matcher(SCRIPT);
        if (m.find()) {
            SCRIPT= m.replaceFirst("//pseudo-code");
            p = Pattern.compile("^(\\s*)fin(\\W)", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"//fin"+String.valueOf(m.group(2)));
                 m = p.matcher(SCRIPT);
                 matchFound = m.find();
                }
            p = Pattern.compile("^(\\s*)Fin(\\W)", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"//Fin"+String.valueOf(m.group(2)));
                 m = p.matcher(SCRIPT);
                 matchFound = m.find();
                } 
            p=Pattern.compile("\\Q//\\E[^\n]*", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
             while (matchFound) {
                SCRIPT= m.replaceFirst("");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                } 
            SCRIPT=SCRIPT+"\n"; 
            p = Pattern.compile("Avancer\\(([^\n]*)\\)[ ;]*\n", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
             while (matchFound) {
                SCRIPT= m.replaceFirst("Avancer("+String.valueOf(m.group(1))+")[0]\n");
                 m = p.matcher(SCRIPT);
                 matchFound = m.find();
                }
            p = Pattern.compile("AvancerDP\\(([^\n]*)\\)[ ;]*\n", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
             while (matchFound) {
                SCRIPT= m.replaceFirst("AvancerDP("+String.valueOf(m.group(1))+")[0]\n");
                 m = p.matcher(SCRIPT);
                 matchFound = m.find();
                }
            p = Pattern.compile("Reculer\\(([^\n]*)\\)[ ;]*\n", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
             while (matchFound) {
                SCRIPT= m.replaceFirst("Reculer("+String.valueOf(m.group(1))+")[0]\n");
                 m = p.matcher(SCRIPT);
                 matchFound = m.find();
                }
            p = Pattern.compile("^Tant\\s", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst("tant ");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                } 
            p = Pattern.compile("(^[^\"]*)\\sTant\\s", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" tant ");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sTant\\s", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" tant "+String.valueOf(m.group(2)));
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("^Pour\\s", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst("pour ");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)\\sPour\\s", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" pour ");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sPour\\s", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" pour ");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("^Répéter\\s", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst("répéter ");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)\\sRépéter\\s", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" répéter ");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sRépéter\\s", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" répéter ");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("^Si\\s", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst("si ");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)\\sSi\\s", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" si ");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sSi\\)", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" si ");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                } 
            p = Pattern.compile("(^[^\"]*)=", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"+--+");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)=", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"+--+");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            SCRIPT=SCRIPT.replace("+--+","==");
            p = Pattern.compile("(^[^\"]*)(?:<-)", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"=");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)(?:<-)", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"=");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)⟵", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"=");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)⟵", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"=");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)←", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"=");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)←", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"=");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)≤", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"<=");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)≤", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"<=");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)≥", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+">=");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)≥", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+">=");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)≠", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"!=");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)≠", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"!=");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)modulo", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"%");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)modulo", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"%");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)×", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"*");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)×", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"*");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)∞", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"Infinity");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)∞", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"Infinity");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*):", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"/");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*):", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"/");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            SCRIPT=SCRIPT.replace(",\"fin\")",",\"thin\")");
            
            //SCRIPT=SCRIPT.replace("fin","Fin");
            //SCRIPT=SCRIPT.replace("Fin","//Fin");
            //SCRIPT=SCRIPT.replace(",",".");
            p = Pattern.compile("^afficher\\s", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst("Afficher ");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)\\safficher\\s", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" Afficher ");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\safficher\\s", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" Afficher ");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("^Println\\(", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst("Printline(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)\\sPrintln\\(", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" Printline(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sPrintln\\(", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" Printline(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)ln\\(", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"log(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)ln\\(", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"log(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)val_approchée_par_excès\\(", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"ceil(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)val_approchée_par_excès\\(", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"ceil(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("^Printline\\(", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst("Println(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*\\s)Printline\\(", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"Println(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*\\s)Printline\\(", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"Println(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)arrondi\\(", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"round(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)arrondi\\(", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"round(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)troncature\\(", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"floor(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)troncature\\(", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"floor\\(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)racine\\(", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"sqrt(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)racine\\(", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"sqrt(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)puissance\\(", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"pow(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)puissance\\(", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"pow(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)nombre_aleatoire\\(", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"random(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)nombre_aleatoire\\(", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"random(");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*[^.\"])PI", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"Math.PI");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*[^.\"])PI", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"Math.PI");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*[^.\"]).taille", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+".length");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*[^.\"]).taille", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+".length");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            String[] mathkeynames2={"sin", "cos", "tan", "asin", "acos", "atan",
            "abs", "ceil", "round", "floor","exp", "log", "sqrt", "random", "pow", "min", "max"};
            for (int i=0; i<mathkeynames2.length; i++) {
                p = Pattern.compile("(^[^\"]*[^.a-zA-Z_0-9])"+mathkeynames2[i]+"\\(", Pattern.MULTILINE);
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                while (matchFound) {
                    SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"Math."+mathkeynames2[i]+"(");
                    m = p.matcher(SCRIPT);
                    matchFound = m.find();
                    }
                p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*[^.a-zA-Z_0-9])"+mathkeynames2[i]+"\\(", Pattern.MULTILINE);
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                while (matchFound) {
                    SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"Math."+mathkeynames2[i]+"(");
                    m = p.matcher(SCRIPT);
                    matchFound = m.find();
                     }
            }
            p = Pattern.compile("^Afficher\\s([^\n]*)\n", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst("Println("+String.valueOf(m.group(1))+")\n");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)\\sAfficher\\s([^\n]*)\n", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" Println("+String.valueOf(m.group(2))+")\n");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sAfficher\\s([^\n]*)\n", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" Println("+String.valueOf(m.group(2))+")\n");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("^tant\\sque\\s([^\n]*)\n", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
             while (matchFound&&m.groupCount()>=1) {
                SCRIPT= m.replaceFirst("while ("+String.valueOf(m.group(1))+")\n");
                 m = p.matcher(SCRIPT);
                 matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)\\stant(?:\\s)que\\s([^\n]*)\n", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" while ("+String.valueOf(m.group(2))+")\n");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                } 
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\stant(?:\\s)que\\s([^\n]*)\n", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
             while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" while ("+String.valueOf(m.group(2))+")\n");
        	m = p.matcher(SCRIPT);
                matchFound = m.find();
                 }
            p = Pattern.compile("^si\\s([^\n]*)\n", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
             while (matchFound&&m.groupCount()>=1) {
                SCRIPT= m.replaceFirst("if ("+String.valueOf(m.group(1))+")\n");
                 m = p.matcher(SCRIPT);
                 matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)\\ssi\\s([^\n]*)\n", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" if ("+String.valueOf(m.group(2))+")\n");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                } 
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\ssi\\s([^\n]*)\n", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
             while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" if ("+String.valueOf(m.group(2))+")\n");
        	m = p.matcher(SCRIPT);
                matchFound = m.find();
                 }
            p = Pattern.compile("^Si\\s([^\n]*)\n", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
             while (matchFound&&m.groupCount()>=1) {
                SCRIPT= m.replaceFirst("if ("+String.valueOf(m.group(1))+")\n");
                 m = p.matcher(SCRIPT);
                 matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)\\sSi\\s([^\n]*)\n", Pattern.MULTILINE); //tour de passe-passe de base
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" if ("+String.valueOf(m.group(2))+")\n");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                } 
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sSi\\s([^\n]*)\n", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
             while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" if ("+String.valueOf(m.group(2))+")\n");
        	m = p.matcher(SCRIPT);
                matchFound = m.find();
                 }  
            int nbOuverts=0;
            String[] lesLignes= SCRIPT.split("\n");
            int indentationLignePrec=0;
            int indentationLigneActu=0;
            Boolean cherchonsUnEspace=true;
            int ancienneLigne=0;
            for (int i=1; i<lesLignes.length; i++){
                if (Pattern.compile("^[\\s]*$").matcher(lesLignes[i]).find()) {
                    lesLignes[i-1]=lesLignes[i-1]+";";
                    continue;
                }
                indentationLigneActu=0;
                cherchonsUnEspace=true;
                while (cherchonsUnEspace) {
                    cherchonsUnEspace=false;
                    p = Pattern.compile("^ ");
                    m = p.matcher(lesLignes[i]);
                    matchFound = m.find();
                    if (matchFound) {
                        lesLignes[i]= m.replaceFirst("");
                        indentationLigneActu++;
                        cherchonsUnEspace=true;
                    }
                }
                if (!(indentationLigneActu%3==0)) {
                    ITEM.sendErrorToEditor("indent error: must be of 3 spaces");
                    killme();
                    ITEM.getPanel().Restore();
                    //return;
                }
                for (int j=0; j<indentationLigneActu; j++) {
                    lesLignes[i]=" "+lesLignes[i];
                }
                if (indentationLigneActu>indentationLignePrec) {
                    lesLignes[i-1]=lesLignes[i-1]+"{";
                    nbOuverts++;
                }
                if (indentationLigneActu==indentationLignePrec) {
                    lesLignes[i-1]=lesLignes[i-1]+";";
                }
                while (indentationLigneActu<indentationLignePrec) {
                    lesLignes[ancienneLigne]=lesLignes[ancienneLigne]+";}";
                    indentationLignePrec-=3;
                    nbOuverts--;
                }
                indentationLignePrec=indentationLigneActu;
                ancienneLigne=i;
            } 
            lesLignes[ancienneLigne]+=";";
            lesLignes[lesLignes.length-1]+="\n";
            for (int k=0; k<nbOuverts; k++) {
                    lesLignes[lesLignes.length-1]=lesLignes[lesLignes.length-1]+"}";
                }
            
            SCRIPT="";
            for (int i=0; i<lesLignes.length-1; i++){ 
                SCRIPT=SCRIPT+lesLignes[i]+"\n";
            p = Pattern.compile(";(\n+\\s*)\\{", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
             while (matchFound&&m.groupCount()>=1) {
                SCRIPT= m.replaceFirst("{"+String.valueOf(m.group(1)));
                 m = p.matcher(SCRIPT);
                 matchFound = m.find();
                }     
            }
            SCRIPT=SCRIPT+lesLignes[lesLignes.length-1];
            p = Pattern.compile("^[\\s]*;$", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst("");
                 m = p.matcher(SCRIPT);
                 matchFound = m.find();
                }
            p = Pattern.compile("^;;", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(";");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*);;", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+";");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*);;", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+";");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("^\\};", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst("}");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]*)\\};", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"}");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\};", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"}");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
        }
        p = Pattern.compile("(^[^\"]*):=", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"=");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
            p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*):=", Pattern.MULTILINE);
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            while (matchFound) {
                SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"=");
                m = p.matcher(SCRIPT);
                matchFound = m.find();
                }
        p = Pattern.compile("^Tant\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst("tant ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }    
        p = Pattern.compile("(^[^\"]*)\\sTant\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" tant ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sTant\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" tant ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("^Si\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst("si ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]*)\\sSi\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" si ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sSi\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" si ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("^Pour\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst("pour ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]*)\\sPour\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" pour ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sPour\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" pour ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("^Répéter\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst("répéter ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]*)\\sRépéter\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" répéter ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sRépéter\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" répéter ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        SCRIPT=SCRIPT.replace(",\"fin\")",",\"thin\")");
        SCRIPT=SCRIPT.replace(",\"épais\")",",\"thick\")"); 
        SCRIPT=SCRIPT.replace(",\"carré\")",",\"square\")");
        SCRIPT=SCRIPT.replace(",\"cercle\")",",\"circle\")");
        SCRIPT=SCRIPT.replace(",\"diamant\")",",\"diamond\")");
        SCRIPT=SCRIPT.replace(",\"croixPlus\")",",\"cross\")");
        SCRIPT=SCRIPT.replace(",\"croix\")",",\"dcross\")");
        
        SCRIPT=SCRIPT.replace(",\"vert\")",",\"green\")");
        SCRIPT=SCRIPT.replace(",\"bleu\")",",\"blue\")");
        SCRIPT=SCRIPT.replace(",\"marron\")",",\"brown\")");
        SCRIPT=SCRIPT.replace(",\"rouge\")",",\"red\")");
        SCRIPT=SCRIPT.replace(",\"noir\")",",\"black\")");
        
        SCRIPT=SCRIPT.replace(",\"montrervaleur\")",",\"showvalue\")");
        SCRIPT=SCRIPT.replace(",\"montrernom\")",",\"showname\")");
        SCRIPT=SCRIPT.replace(",\"fond\")",",\"background\")");
        SCRIPT=SCRIPT.replace(",\"caché\")",",\"hidden\")");
        SCRIPT=SCRIPT.replace(",\"supercaché\")",",\"superhidden\")");
        
        SCRIPT=SCRIPT.replace("(\"polygone\",","(\"area\",");
        SCRIPT=SCRIPT.replace("(\"rempli\",","(\"filled\",");
        SCRIPT=SCRIPT.replace("(\"opaque\",","(\"solid\",");
        SCRIPT=SCRIPT.replace("(\"cercle\",","(\"circle\",");
        SCRIPT=SCRIPT.replace("(\"droite\",","(\"line\",");
        SCRIPT=SCRIPT.replace("(\"texte\",","(\"text\",");
        SCRIPT=SCRIPT.replace("(\"couleur0\",","(\"color0\",");
        SCRIPT=SCRIPT.replace("(\"couleur1\",","(\"color1\",");
        SCRIPT=SCRIPT.replace("(\"couleur2\",","(\"color2\",");
        SCRIPT=SCRIPT.replace("(\"couleur3\",","(\"color3\",");
        SCRIPT=SCRIPT.replace("(\"couleur4\",","(\"color4\",");
        SCRIPT=SCRIPT.replace("(\"couleur5\",","(\"color5\",");
        SCRIPT=SCRIPT.replace("(\"épaisseur0\",","(\"thickness0\",");
        SCRIPT=SCRIPT.replace("(\"épaisseur1\",","(\"thickness1\",");
        SCRIPT=SCRIPT.replace("(\"épaisseur2\",","(\"thickness2\",");
        SCRIPT=SCRIPT.replace("(\"montrervaleur\",","(\"showvalue\",");
        SCRIPT=SCRIPT.replace("(\"montrernom\",","(\"showname\",");
        SCRIPT=SCRIPT.replace("(\"obtus\",","(\"obtuse\",");
        
        p = Pattern.compile("(^[^\"]*)vrai", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"true");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)vrai", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"true");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]*)faux", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"false");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)faux", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"false");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("^si\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst("if ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]*)\\ssi\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" if ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\ssi\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" if ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("^sinon(\\W)", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst("else"+String.valueOf(m.group(1)));
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]*)\\ssinon(\\W)", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" else"+String.valueOf(m.group(2)));
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\ssinon", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" else");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("^bifurquer\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst("switch ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]*)\\sbifurquer\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" switch ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sbifurquer\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" switch ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("^cas\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst("case ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]*)\\scas\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" case ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\scas\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" case ");
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("^rompre([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
        	SCRIPT= m.replaceFirst("break"+String.valueOf(m.group(1)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]*)\\srompre([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" break"+String.valueOf(m.group(2)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\srompre([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" break"+String.valueOf(m.group(2)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("^par\\sdéfaut([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
        	SCRIPT= m.replaceFirst("default"+String.valueOf(m.group(1)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]*)\\spar\\sdéfaut([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" default"+String.valueOf(m.group(2)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\spar\\sdéfaut([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" default"+String.valueOf(m.group(2)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("^pour\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        if (matchFound) {
        	SCRIPT= m.replaceAll("for ");
        }
        p = Pattern.compile("(^[^\"]*)\\spour\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" for ");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\spour\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" for ");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]*)\\sallant\\sde\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" from ");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sallant\\sde\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" from ");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]*)\\sà\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" to ");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sà\\s", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" to ");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("^tant\\sque\\s(\\W)", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
        	SCRIPT= m.replaceFirst("while "+String.valueOf(m.group(1)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]*)\\stant\\sque([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" while"+String.valueOf(m.group(2)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\stant\\sque([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" while"+String.valueOf(m.group(2)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("^faire(\\W)", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
        	SCRIPT= m.replaceFirst("do"+String.valueOf(m.group(1)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]*)\\sfaire([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" do"+String.valueOf(m.group(2)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sfaire([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" do"+String.valueOf(m.group(2)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]*)\\sjusqu'à\\s([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" until "+String.valueOf(m.group(2)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sjusqu'à\\s([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" until "+String.valueOf(m.group(2)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]*)\\suntil[\\s]*\\(([^;]+);", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" while (!("+String.valueOf(m.group(2))+")");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\suntil[\\s]*\\(([^;]+);", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" while (!("+String.valueOf(m.group(2))+")");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("^fonction(\\W)", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
        	SCRIPT= m.replaceFirst("function"+String.valueOf(m.group(1)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]*)\\sfonction([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" function"+String.valueOf(m.group(2)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sfonction([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" function"+String.valueOf(m.group(2)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("^retourner(\\W)", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
        	SCRIPT= m.replaceFirst("return"+String.valueOf(m.group(1)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]*)\\sretourner([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" return"+String.valueOf(m.group(2)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)\\sretourner([\\W&&[^\"]])", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=2) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+" return"+String.valueOf(m.group(2)));
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("^for\\s+([a-zA-Z][a-zA-Z_0-9]*)\\s+from\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s+to\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s*\\{", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=3) {
        	SCRIPT= m.replaceFirst("for ("+m.group(1)+"="+String.valueOf(m.group(2))+";"+String.valueOf(m.group(1))+"<="+String.valueOf(m.group(3))+";"+String.valueOf(m.group(1))+"="+String.valueOf(m.group(1))+"+1){");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]*)for\\s+([a-zA-Z][a-zA-Z_0-9]*)\\s+from\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s+to\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s*\\{", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=3) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"for ("+String.valueOf(m.group(2))+"="+String.valueOf(m.group(3))+";"+String.valueOf(m.group(2))+"<="+String.valueOf(m.group(4))+";"+String.valueOf(m.group(2))+"="+String.valueOf(m.group(2))+"+1){");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)for\\s+([a-zA-Z][a-zA-Z_0-9]*)\\s+from\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s+to\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s*\\{", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=3) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"for ("+String.valueOf(m.group(2))+"="+String.valueOf(m.group(3))+";"+String.valueOf(m.group(2))+"<="+String.valueOf(m.group(4))+";"+String.valueOf(m.group(2))+"="+String.valueOf(m.group(2))+"+1){");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("^for\\s+var\\s+([a-zA-Z][a-zA-Z_0-9]*)\\s+from\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s+to\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s*\\{", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=3) {
        	SCRIPT= m.replaceFirst("for (var "+m.group(1)+"="+String.valueOf(m.group(2))+";"+String.valueOf(m.group(1))+"<="+String.valueOf(m.group(3))+";"+String.valueOf(m.group(1))+"="+String.valueOf(m.group(1))+"+1){");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]*)for\\s+var\\s+([a-zA-Z][a-zA-Z_0-9]*)\\s+from\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s+to\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s*\\{", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=3) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"for (var "+String.valueOf(m.group(2))+"="+String.valueOf(m.group(3))+";"+String.valueOf(m.group(2))+"<="+String.valueOf(m.group(4))+";"+String.valueOf(m.group(2))+"="+String.valueOf(m.group(2))+"+1){");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)for\\s+var\\s+([a-zA-Z][a-zA-Z_0-9]*)\\s+from\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s+to\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s*\\{", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=3) {
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"for (var "+String.valueOf(m.group(2))+"="+String.valueOf(m.group(3))+";"+String.valueOf(m.group(2))+"<="+String.valueOf(m.group(4))+";"+String.valueOf(m.group(2))+"="+String.valueOf(m.group(2))+"+1){");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("^répéter\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s+fois\\s*\\{", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
                nbRepeter++;
        	SCRIPT= m.replaceFirst("for (var compteurZZ"+nbRepeter+"=0;compteurZZ"+nbRepeter+"<"+String.valueOf(m.group(1))+";compteurZZ"+nbRepeter+"++){");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]*)répéter\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s+fois\\s*\\{", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
                nbRepeter++;
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"for (var compteurZZ"+nbRepeter+"=0;compteurZZ"+nbRepeter+"<"+String.valueOf(m.group(2))+";compteurZZ"+nbRepeter+"++){");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)répéter\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s+fois\\s*\\{", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
                nbRepeter++;
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"for (var compteurZZ"+nbRepeter+"=0;compteurZZ"+nbRepeter+"<"+String.valueOf(m.group(2))+";compteurZZ"+nbRepeter+"++){");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("^repeat\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s+times\\s*\\{", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
                nbRepeter++;
        	SCRIPT= m.replaceFirst("for (var compteurZZ"+nbRepeter+"=0;compteurZZ"+nbRepeter+"<"+String.valueOf(m.group(1))+";compteurZZ"+nbRepeter+"++){");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]*)repeat\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s+times\\s*\\{", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
                nbRepeter++;
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"for (var compteurZZ"+nbRepeter+"=0;compteurZZ"+nbRepeter+"<"+String.valueOf(m.group(2))+";compteurZZ"+nbRepeter+"++){");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"]*\"[^\"]*)+[^\"\n]*)repeat\\s+([a-zA-Z_0-9\\[\\]\\-\\+\\*/\\.\\(\\)]+)\\s+times\\s*\\{", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound&&m.groupCount()>=1) {
                nbRepeter++;
        	SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+"for (var compteurZZ"+nbRepeter+"=0;compteurZZ"+nbRepeter+"<"+String.valueOf(m.group(2))+";compteurZZ"+nbRepeter+"++){");
        	m = p.matcher(SCRIPT);
            matchFound = m.find();
        }
        //if (!ZC.isEuclidian()) {
        p = Pattern.compile("(^[^\"]*)\"([^\"\n]*)distDP\\(([^)\n]*)\\)([^\"\n]*)\"", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+String.valueOf(m.group(2))+"ExecuteMacro(\"@builtin@/DP_bi_distance2\",\"Hz,"+String.valueOf(m.group(3))+",CH\")"+String.valueOf(m.group(4)));
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        p = Pattern.compile("(^[^\"]+(?:\"[^\"\n]*\"[^\"\n]*)+)\"([^\"\n]*)distDP\\(([^)\n]*)\\)([^\"\n]*)\"", Pattern.MULTILINE);
        m = p.matcher(SCRIPT);
        matchFound = m.find();
        while (matchFound) {
            SCRIPT= m.replaceFirst(String.valueOf(m.group(1))+String.valueOf(m.group(2))+"ExecuteMacro(\"@builtin@/DP_bi_distance2\",\"Hz,"+String.valueOf(m.group(3))+",CH\")"+String.valueOf(m.group(4)));
            m = p.matcher(SCRIPT);
            matchFound = m.find();
            }
        //}
        System.out.println(SCRIPT);
	//ITEM.getPanel().Backup();
	JZirkelCanvas.getCurrentZC().getScriptsPanel().Backup();
        setPriority(Thread.MIN_PRIORITY);
        start();
    }
    
    String getSCRIPT() {
            return SCRIPT;
    }
    
    String getSCRIPTi() {
            return ITEM.getScriptSource();
    }

    /*****************************************
     * PARTIE RESERVEE AUX ACTION-SCRIPTS :
     *****************************************/
    public void runActionScript() {
        if (actionScriptInProgress) {
            executeActionScript=true;
        }
    }

    public void stopActionScript() {
        if (actionScriptInProgress) {
            busyActionScript=false;
            actionScriptInProgress=false;
        }
    }

    public void prepareActionScript(final String pointName) {
        isActionScript=true;
        busyActionScript=true;
        actionScriptInProgress=true;
        SCRIPT=ITEM.getScriptSource().replace("$name", pointName);
	ITEM.getPanel().Backup();
        setPriority(Thread.MAX_PRIORITY);
        start();
    }

    /***************************************************
     * APPELE UNE SEULE FOIS AU LANCEMENT DE L'APPLI :
     ***************************************************/
    static public void InitContextFactory() {
        ContextFactory.initGlobal(new ContextFactory() {

            @Override
            protected Context makeContext() {
                Context cx=super.makeContext();
                cx.setInstructionObserverThreshold(100);
                return cx;
            }

            @Override
            protected void observeInstructionCount(Context cx, int instructionCount) {
                ScriptThread th=(ScriptThread) Thread.currentThread();
                while (th.stopme) {
                }
                if (th.killme) {
                    Error er=new Error() {

                        @Override
                        public String getMessage() {
                            return "Script killed...";
                        }
                    };
                    throw er;
                }
                if (th.isActionScript) {
                    if (th.executeActionScript||!th.busyActionScript) {
                        Error er=new Error() {

                            @Override
                            public String getMessage() {
                                return "Action Script killed...";
                            }
                        };
                        throw er;
                    }
                }

            }
        });
    }
}
