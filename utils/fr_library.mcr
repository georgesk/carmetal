<?xml version="1.0" encoding="utf-8"?>
<CaR>
<Macro Name="Segments barrés (codage)/Angle droit" showduplicates="true">
<Parameter name="P4">P4</Parameter>
<Parameter name="P5">P5</Parameter>
<Parameter name="P6">P6</Parameter>
<Objects>
<Point name="P4" n="0" mainparameter="true" x="-5.0345994649750265" y="-4.774189147821145">Point à -5.0346, -4.77419</Point>
<Point name="P5" n="1" mainparameter="true" x="-0.624984761169314" y="-5.399173908990457">Point à -0.62498, -5.39917</Point>
<Segment name="s1" n="2" hidden="super" large="true" from="P4" to="P5">Segment de P4 à P5</Segment>
<Point name="P6" n="3" mainparameter="true" x="-0.9496908594038302" y="-2.787136217815591">Point à -0.94969, -2.78714</Point>
<Segment name="s2" n="4" hidden="super" large="true" from="P5" to="P6">Segment de P5 à P6</Segment>
<Circle name="c1" n="5" hidden="super" large="true" fixed="windoww/24" midpoint="P5" acute="true">Cercle de centre P5 de rayon windoww/24</Circle>
<Intersection name="I1" n="6" hidden="super" large="true" first="s1" second="c1" shape="circle" which="second">Intersection entre s1 et c1</Intersection>
<Parallel name="par1" n="7" hidden="super" large="true" point="I1" line="s2">Parallèle passant par I1 à s2</Parallel>
<Intersection name="I2" n="8" hidden="super" large="true" first="s2" second="c1" shape="circle" which="first">Intersection entre s2 et c1</Intersection>
<Parallel name="par2" n="9" hidden="super" large="true" point="I2" line="s1">Parallèle passant par I2 à s1</Parallel>
<Intersection name="I3" n="10" hidden="super" large="true" first="par2" second="par1" shape="circle">Intersection entre par2 et par1</Intersection>
<Segment name="s3" n="11" target="true" large="true" from="I1" to="I3">Segment de I1 à I3</Segment>
<Segment name="s4" n="12" target="true" large="true" from="I3" to="I2">Segment de I3 à I2</Segment>
</Objects>
</Macro>
<Macro Name="Segments barrés (codage)/Ajout 1 barre" showduplicates="true">
<Parameter name="s1">s1</Parameter>
<Objects>
<Point name="P1" n="0" parameter="true" x="-5.459854014598541" y="5.153284671532846">Point à -5.45985, 5.15328</Point>
<Point name="P2" n="1" parameter="true" x="4.2919708029197094" y="-0.8321167883211675">Point à 4.29197, -0.83212</Point>
<Segment name="s1" n="2" mainparameter="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Midpoint name="M1" n="3" hidden="super" first="P1" second="P2" shape="circle">Milieu de P1 et P2</Midpoint>
<Circle name="c1" n="4" hidden="super" fixed="6/pixel" midpoint="M1" acute="true">Cercle de centre M1 de rayon 6/pixel</Circle>
<Angle name="a1" n="5" hidden="super" unit="∞" first="P2" root="M1" fixed="60" acute="true">Angle P2 - M1 de mesure 60</Angle>
<Intersection name="I1" n="6" hidden="super" first="a1" second="c1" shape="circle" which="first">Intersection entre a1 et c1</Intersection>
<Point name="P3" n="7" hidden="super" x="2*x(M1)-x(I1)" actx="-0.6366908345173167" y="2*y(M1)-y(I1)" acty="2.1319916546408813" shape="circle" fixed="true">Point à &quot;2*x(M1)-x(I1)&quot;, &quot;2*y(M1)-y(I1)&quot;</Point>
<Segment name="s2" n="8" target="true" from="I1" to="P3">Segment de I1 à P3</Segment>
</Objects>
</Macro>
<Macro Name="Segments barrés (codage)/Ajout 2 barres" showduplicates="true">
<Parameter name="s1">s1</Parameter>
<Objects>
<Point name="P1" n="0" parameter="true" x="-2.014598540145985" y="4.131386861313868">Point à -2.0146, 4.13139</Point>
<Point name="P2" n="1" parameter="true" x="2.627737226277372" y="1.6204379562043805">Point à 2.62774, 1.62044</Point>
<Segment name="s1" n="2" mainparameter="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Midpoint name="M1" n="3" hidden="super" first="P1" second="P2" shape="circle">Milieu de P1 et P2</Midpoint>
<Circle name="c1" n="4" hidden="super" fixed="3/pixel" midpoint="M1" acute="true">Cercle de centre M1 de rayon 3/pixel</Circle>
<Intersection name="I1" n="5" hidden="super" first="s1" second="c1" shape="circle" which="first">Intersection entre s1 et c1</Intersection>
<Circle name="c2" n="6" hidden="super" fixed="6/pixel" midpoint="I1" acute="true">Cercle de centre I1 de rayon 6/pixel</Circle>
<Angle name="a1" n="7" hidden="super" unit="∞" first="P2" root="I1" fixed="60" acute="true">Angle P2 - I1 de mesure 60</Angle>
<Intersection name="I2" n="8" hidden="super" first="a1" second="c2" shape="circle" which="first">Intersection entre a1 et c2</Intersection>
<Point name="P3" n="9" hidden="super" x="2*x(I1)-x(I2)" actx="0.28184872958399265" y="2*y(I1)-y(I2)" acty="2.830208018717375" shape="circle" fixed="true">Point à &quot;2*x(I1)-x(I2)&quot;, &quot;2*y(I1)-y(I2)&quot;</Point>
<Point name="P4" n="10" hidden="super" x="2*x(M1)-x(I2)" actx="0.22907384579648948" y="2*y(M1)-y(I2)" acty="2.8587529244137606" shape="circle" fixed="true">Point à &quot;2*x(M1)-x(I2)&quot;, &quot;2*y(M1)-y(I2)&quot;</Point>
<Point name="P5" n="11" hidden="super" x="2*x(M1)-x(P3)" actx="0.33128995654739435" y="2*y(M1)-y(P3)" acty="2.9216167988008737" shape="circle" fixed="true">Point à &quot;2*x(M1)-x(P3)&quot;, &quot;2*y(M1)-y(P3)&quot;</Point>
<Segment name="s2" n="12" target="true" from="P3" to="I2">Segment de P3 à I2</Segment>
<Segment name="s3" n="13" target="true" from="P5" to="P4">Segment de P5 à P4</Segment>
</Objects>
</Macro>
<Macro Name="Segments barrés (codage)/Ajout 3 barres" showduplicates="true">
<Parameter name="s1">s1</Parameter>
<Objects>
<Point name="P1" n="0" parameter="true" x="-4.583941605839426" y="4.481751824817528">Point à -4.58394, 4.48175</Point>
<Point name="P2" n="1" parameter="true" x="-1.0802919708029217" y="0.423357664233577">Point à -1.08029, 0.42336</Point>
<Segment name="s1" n="2" mainparameter="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Midpoint name="M1" n="3" hidden="super" first="P1" second="P2" shape="circle">Milieu de P1 et P2</Midpoint>
<Circle name="c1" n="4" hidden="super" fixed="6/pixel" midpoint="M1" acute="true">Cercle de centre M1 de rayon 6/pixel</Circle>
<Angle name="a1" n="5" hidden="super" unit="∞" first="P2" root="M1" fixed="60" acute="true">Angle P2 - M1 de mesure 60</Angle>
<Intersection name="I1" n="6" hidden="super" first="a1" second="c1" shape="circle" which="first">Intersection entre a1 et c1</Intersection>
<Point name="P3" n="7" hidden="super" x="2*x(M1)-x(I1)" actx="-2.891053169554677" y="2*y(M1)-y(I1)" acty="2.4413073907434755" shape="circle" fixed="true">Point à &quot;2*x(M1)-x(I1)&quot;, &quot;2*y(M1)-y(I1)&quot;</Point>
<Segment name="s2" n="8" target="true" from="I1" to="P3">Segment de I1 à P3</Segment>
<Intersection name="I2" n="9" hidden="super" first="s1" second="c1" shape="circle" which="first">Intersection entre s1 et c1</Intersection>
<Intersection name="I3" n="10" hidden="super" first="s1" second="c1" shape="circle" which="second">Intersection entre s1 et c1</Intersection>
<Circle name="c2" n="11" hidden="super" through="M1" midpoint="I3" acute="true">Cercle de centre I3 passant par M1</Circle>
<Circle name="c3" n="12" hidden="super" through="M1" midpoint="I2" acute="true">Cercle de centre I2 passant par M1</Circle>
<Intersection name="I4" n="13" hidden="super" first="c3" second="c1" shape="circle" which="second">Intersection entre c3 et c1</Intersection>
<Point name="P4" n="14" hidden="super" x="2*x(I2)-x(I4)" actx="-2.7339717223702897" y="2*y(I2)-y(I4)" acty="2.4183853718433292" shape="circle" fixed="true">Point à &quot;2*x(I2)-x(I4)&quot;, &quot;2*y(I2)-y(I4)&quot;</Point>
<Intersection name="I5" n="15" hidden="super" first="c2" second="c1" shape="circle" which="second">Intersection entre c2 et c1</Intersection>
<Point name="P5" n="16" hidden="super" x="2*x(I3)-x(I5)" actx="-2.9302618542720578" y="2*y(I3)-y(I5)" acty="2.4867241172077756" shape="circle" fixed="true">Point à &quot;2*x(I3)-x(I5)&quot;, &quot;2*y(I3)-y(I5)&quot;</Point>
<Segment name="s3" n="17" target="true" from="P5" to="I5">Segment de P5 à I5</Segment>
<Segment name="s4" n="18" target="true" from="I4" to="P4">Segment de I4 à P4</Segment>
</Objects>
</Macro>
<Macro Name="Segments barrés (codage)/Ajout croix" showduplicates="true">
<Parameter name="s1">s1</Parameter>
<Objects>
<Point name="P1" n="0" parameter="true" x="-4.175182481751826" y="2.8175182481751815">Point à -4.17518, 2.81752</Point>
<Point name="P2" n="1" parameter="true" x="1.705681499059306" y="-0.9869954741747665">Point à 1.70568, -0.987</Point>
<Segment name="s1" n="2" color="3" mainparameter="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Midpoint name="M1" n="3" color="3" hidden="super" first="P1" second="P2" shape="circle">Milieu de P1 et P2</Midpoint>
<Circle name="c1" n="4" color="3" hidden="super" fixed="6/pixel" midpoint="M1" acute="true">Cercle de centre M1 de rayon 6/pixel</Circle>
<Angle name="a1" n="5" color="3" hidden="super" unit="∞" first="P2" root="M1" fixed="60" acute="true">Angle P2 - M1 de mesure 60</Angle>
<Intersection name="I1" n="6" color="3" hidden="super" first="a1" second="c1" shape="circle" which="first">Intersection entre a1 et c1</Intersection>
<Point name="P3" n="7" color="3" hidden="super" x="2*x(M1)-x(I1)" actx="-1.2881632995755412" y="2*y(M1)-y(I1)" acty="0.8879287706405898" shape="circle" fixed="true">Point à &quot;2*x(M1)-x(I1)&quot;, &quot;2*y(M1)-y(I1)&quot;</Point>
<Segment name="s2" n="8" color="3" target="true" from="I1" to="P3">Segment de I1 à P3</Segment>
<Plumb name="perp1" n="9" color="3" hidden="super" point="I1" line="s1" valid="true">Perpendiculaire passant par I1 à s1</Plumb>
<Intersection name="I2" n="10" color="3" hidden="super" first="perp1" second="s1" shape="circle">Intersection entre perp1 et s1</Intersection>
<Point name="P4" n="11" color="3" hidden="super" x="2*x(I2)-x(I1)" actx="-1.2377861553415777" y="2*y(I2)-y(I1)" acty="0.8553382300063748" shape="circle" fixed="true">Point à &quot;2*x(I2)-x(I1)&quot;, &quot;2*y(I2)-y(I1)&quot;</Point>
<Point name="P5" n="12" color="3" hidden="super" x="2*x(M1)-x(P4)" actx="-1.2317148273509426" y="2*y(M1)-y(P4)" acty="0.9751845439940403" shape="circle" fixed="true">Point à &quot;2*x(M1)-x(P4)&quot;, &quot;2*y(M1)-y(P4)&quot;</Point>
<Segment name="s3" n="13" color="3" target="true" from="P5" to="P4">Segment de P5 à P4</Segment>
</Objects>
</Macro>
<Macro Name="Segments barrés (codage)/Ajout cercle" showduplicates="true">
<Parameter name="s1">s1</Parameter>
<Objects>
<Point name="P1" n="0" parameter="true" x="-4.175182481751826" y="2.8175182481751815">Point à -4.17518, 2.81752</Point>
<Point name="P2" n="1" parameter="true" x="1.705681499059306" y="-0.9869954741747665">Point à 1.70568, -0.987</Point>
<Segment name="s1" n="2" color="3" mainparameter="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Midpoint name="M1" n="3" color="3" hidden="super" first="P1" second="P2" shape="circle">Milieu de P1 et P2</Midpoint>
<Circle name="c1" n="4" color="3" target="true" fixed="6/pixel" midpoint="M1" acute="true">Cercle de centre M1 de rayon 6/pixel</Circle>
</Objects>
</Macro>
<Macro Name="Segments barrés (codage)/Ajout Z" showduplicates="true">
<Parameter name="s1">s1</Parameter>
<Objects>
<Point name="P1" n="0" parameter="true" x="-4.175182481751826" y="2.8175182481751815">Point à -4.17518, 2.81752</Point>
<Point name="P2" n="1" parameter="true" x="-0.46849235957923524" y="-0.48227154662568333">Point à -0.46849, -0.48227</Point>
<Segment name="s1" n="2" color="3" mainparameter="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Midpoint name="M1" n="3" color="3" hidden="super" first="P1" second="P2" shape="circle">Milieu de P1 et P2</Midpoint>
<Plumb name="perp1" n="4" color="3" hidden="super" point="M1" line="s1" valid="true">Perpendiculaire passant par M1 à s1</Plumb>
<Circle name="c1" n="5" color="3" hidden="super" fixed="6/pixel" midpoint="M1" acute="true">Cercle de centre M1 de rayon 6/pixel</Circle>
<Intersection name="I1" n="6" color="3" hidden="super" first="perp1" second="c1" shape="circle" which="first">Intersection entre perp1 et c1</Intersection>
<Parallel name="par1" n="7" color="3" hidden="super" point="I1" line="s1">Parallèle passant par I1 à s1</Parallel>
<Intersection name="I2" n="8" color="3" hidden="super" first="perp1" second="c1" shape="circle" which="second">Intersection entre perp1 et c1</Intersection>
<Parallel name="par2" n="9" color="3" hidden="super" point="I2" line="s1">Parallèle passant par I2 à s1</Parallel>
<Circle name="c2" n="10" color="3" hidden="super" fixed="12/pixel" midpoint="I2" acute="true">Cercle de centre I2 de rayon 12/pixel</Circle>
<Circle name="c3" n="11" color="3" hidden="super" fixed="c2" midpoint="I1" acute="true">Cercle de centre I1 de rayon c2</Circle>
<Intersection name="I3" n="12" color="3" hidden="super" first="par1" second="c3" shape="circle" which="second">Intersection entre par1 et c3</Intersection>
<Midpoint name="M2" n="13" color="3" hidden="super" first="I3" second="I1" shape="circle">Milieu de I3 et I1</Midpoint>
<Intersection name="I4" n="14" color="3" hidden="super" first="par2" second="c2" shape="circle" which="first">Intersection entre par2 et c2</Intersection>
<Midpoint name="M3" n="15" color="3" hidden="super" first="I2" second="I4" shape="circle">Milieu de I2 et I4</Midpoint>
<Intersection name="I5" n="16" color="3" hidden="super" first="par2" second="c2" shape="circle" which="second">Intersection entre par2 et c2</Intersection>
<Intersection name="I6" n="17" color="3" hidden="super" first="par1" second="c3" shape="circle" which="first">Intersection entre par1 et c3</Intersection>
<Function name="f1" n="18" color="3" type="thick" target="true" large="true" x="(t==0)*x(I5)+(t==1)*x(M2)+(t==2)*x(M3)+(t==3)*x(I6)" y="(t==0)*y(I5)+(t==1)*y(M2)+(t==2)*y(M3)+(t==3)*y(I6)" var="t" min="0" max="3" d="1" shape="cross">Fonction ( (t==0)*x(I5)+(t==1)*x(M2)+(t==2)*x(M3)+(t==3)*x(I6) , (t==0)*y(I5)+(t==1)*y(M2)+(t==2)*y(M3)+(t==3)*y(I6) )</Function>
</Objects>
</Macro>
<Macro Name="Segments barrés (codage)/Segment 1 barre" showduplicates="true">
<Parameter name="P1">P1</Parameter>
<Parameter name="P2">P2</Parameter>
<Objects>
<Point name="P1" n="0" mainparameter="true" x="-5.459854014598541" y="5.153284671532846">Point à -5.45985, 5.15328</Point>
<Point name="P2" n="1" mainparameter="true" x="4.2919708029197094" y="-0.8321167883211675">Point à 4.29197, -0.83212</Point>
<Segment name="s1" n="2" target="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Midpoint name="M1" n="3" hidden="super" first="P1" second="P2" shape="circle">Milieu de P1 et P2</Midpoint>
<Circle name="c1" n="4" hidden="super" fixed="6/pixel" midpoint="M1" acute="true">Cercle de centre M1 de rayon 6/pixel</Circle>
<Angle name="a1" n="5" hidden="super" unit="∞" first="P2" root="M1" fixed="60" acute="true">Angle P2 - M1 de mesure 60</Angle>
<Intersection name="I1" n="6" hidden="super" first="a1" second="c1" shape="circle" which="first">Intersection entre a1 et c1</Intersection>
<Point name="P3" n="7" hidden="super" x="2*x(M1)-x(I1)" actx="-0.6366908345173167" y="2*y(M1)-y(I1)" acty="2.1319916546408813" shape="circle" fixed="true">Point à &quot;2*x(M1)-x(I1)&quot;, &quot;2*y(M1)-y(I1)&quot;</Point>
<Segment name="s2" n="8" target="true" from="I1" to="P3">Segment de I1 à P3</Segment>
</Objects>
</Macro>
<Macro Name="Segments barrés (codage)/Segment 2 barres" showduplicates="true">
<Parameter name="P1">P1</Parameter>
<Parameter name="P2">P2</Parameter>
<Objects>
<Point name="P1" n="0" mainparameter="true" x="-2.014598540145985" y="4.131386861313868">Point à -2.0146, 4.13139</Point>
<Point name="P2" n="1" mainparameter="true" x="2.627737226277372" y="1.6204379562043805">Point à 2.62774, 1.62044</Point>
<Segment name="s1" n="2" target="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Midpoint name="M1" n="3" hidden="super" first="P1" second="P2" shape="circle">Milieu de P1 et P2</Midpoint>
<Circle name="c1" n="4" hidden="super" fixed="3/pixel" midpoint="M1" acute="true">Cercle de centre M1 de rayon 3/pixel</Circle>
<Intersection name="I1" n="5" hidden="super" first="s1" second="c1" shape="circle" which="first">Intersection entre s1 et c1</Intersection>
<Circle name="c2" n="6" hidden="super" fixed="6/pixel" midpoint="I1" acute="true">Cercle de centre I1 de rayon 6/pixel</Circle>
<Angle name="a1" n="7" hidden="super" unit="∞" first="P2" root="I1" fixed="60" acute="true">Angle P2 - I1 de mesure 60</Angle>
<Intersection name="I2" n="8" hidden="super" first="a1" second="c2" shape="circle" which="first">Intersection entre a1 et c2</Intersection>
<Point name="P3" n="9" hidden="super" x="2*x(I1)-x(I2)" actx="0.28184872958399265" y="2*y(I1)-y(I2)" acty="2.830208018717375" shape="circle" fixed="true">Point à &quot;2*x(I1)-x(I2)&quot;, &quot;2*y(I1)-y(I2)&quot;</Point>
<Point name="P4" n="10" hidden="super" x="2*x(M1)-x(I2)" actx="0.22907384579648948" y="2*y(M1)-y(I2)" acty="2.8587529244137606" shape="circle" fixed="true">Point à &quot;2*x(M1)-x(I2)&quot;, &quot;2*y(M1)-y(I2)&quot;</Point>
<Point name="P5" n="11" hidden="super" x="2*x(M1)-x(P3)" actx="0.33128995654739435" y="2*y(M1)-y(P3)" acty="2.9216167988008737" shape="circle" fixed="true">Point à &quot;2*x(M1)-x(P3)&quot;, &quot;2*y(M1)-y(P3)&quot;</Point>
<Segment name="s2" n="12" target="true" from="P3" to="I2">Segment de P3 à I2</Segment>
<Segment name="s3" n="13" target="true" from="P5" to="P4">Segment de P5 à P4</Segment>
</Objects>
</Macro>
<Macro Name="Segments barrés (codage)/Segment 3 barres" showduplicates="true">
<Parameter name="P1">P1</Parameter>
<Parameter name="P2">P2</Parameter>
<Objects>
<Point name="P1" n="0" mainparameter="true" x="-4.583941605839426" y="4.481751824817528">Point à -4.58394, 4.48175</Point>
<Point name="P2" n="1" mainparameter="true" x="-1.0802919708029217" y="0.423357664233577">Point à -1.08029, 0.42336</Point>
<Segment name="s1" n="2" target="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Midpoint name="M1" n="3" hidden="super" first="P1" second="P2" shape="circle">Milieu de P1 et P2</Midpoint>
<Circle name="c1" n="4" hidden="super" fixed="6/pixel" midpoint="M1" acute="true">Cercle de centre M1 de rayon 6/pixel</Circle>
<Angle name="a1" n="5" hidden="super" unit="∞" first="P2" root="M1" fixed="60" acute="true">Angle P2 - M1 de mesure 60</Angle>
<Intersection name="I1" n="6" hidden="super" first="a1" second="c1" shape="circle" which="first">Intersection entre a1 et c1</Intersection>
<Point name="P3" n="7" hidden="super" x="2*x(M1)-x(I1)" actx="-2.891053169554677" y="2*y(M1)-y(I1)" acty="2.4413073907434755" shape="circle" fixed="true">Point à &quot;2*x(M1)-x(I1)&quot;, &quot;2*y(M1)-y(I1)&quot;</Point>
<Segment name="s2" n="8" target="true" from="I1" to="P3">Segment de I1 à P3</Segment>
<Intersection name="I2" n="9" hidden="super" first="s1" second="c1" shape="circle" which="first">Intersection entre s1 et c1</Intersection>
<Intersection name="I3" n="10" hidden="super" first="s1" second="c1" shape="circle" which="second">Intersection entre s1 et c1</Intersection>
<Circle name="c2" n="11" hidden="super" through="M1" midpoint="I3" acute="true">Cercle de centre I3 passant par M1</Circle>
<Circle name="c3" n="12" hidden="super" through="M1" midpoint="I2" acute="true">Cercle de centre I2 passant par M1</Circle>
<Intersection name="I4" n="13" hidden="super" first="c3" second="c1" shape="circle" which="second">Intersection entre c3 et c1</Intersection>
<Point name="P4" n="14" hidden="super" x="2*x(I2)-x(I4)" actx="-2.7339717223702897" y="2*y(I2)-y(I4)" acty="2.4183853718433292" shape="circle" fixed="true">Point à &quot;2*x(I2)-x(I4)&quot;, &quot;2*y(I2)-y(I4)&quot;</Point>
<Intersection name="I5" n="15" hidden="super" first="c2" second="c1" shape="circle" which="second">Intersection entre c2 et c1</Intersection>
<Point name="P5" n="16" hidden="super" x="2*x(I3)-x(I5)" actx="-2.9302618542720578" y="2*y(I3)-y(I5)" acty="2.4867241172077756" shape="circle" fixed="true">Point à &quot;2*x(I3)-x(I5)&quot;, &quot;2*y(I3)-y(I5)&quot;</Point>
<Segment name="s3" n="17" target="true" from="P5" to="I5">Segment de P5 à I5</Segment>
<Segment name="s4" n="18" target="true" from="I4" to="P4">Segment de I4 à P4</Segment>
</Objects>
</Macro>
<Macro Name="Segments barrés (codage)/Segment croix" showduplicates="true">
<Parameter name="P1">P1</Parameter>
<Parameter name="P2">P2</Parameter>
<Objects>
<Point name="P1" n="0" mainparameter="true" x="-4.175182481751826" y="2.8175182481751815">Point à -4.17518, 2.81752</Point>
<Point name="P2" n="1" mainparameter="true" x="1.705681499059306" y="-0.9869954741747665">Point à 1.70568, -0.987</Point>
<Segment name="s1" n="2" color="3" target="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Midpoint name="M1" n="3" color="3" hidden="super" first="P1" second="P2" shape="circle">Milieu de P1 et P2</Midpoint>
<Circle name="c1" n="4" color="3" hidden="super" fixed="6/pixel" midpoint="M1" acute="true">Cercle de centre M1 de rayon 6/pixel</Circle>
<Angle name="a1" n="5" color="3" hidden="super" unit="∞" first="P2" root="M1" fixed="60" acute="true">Angle P2 - M1 de mesure 60</Angle>
<Intersection name="I1" n="6" color="3" hidden="super" first="a1" second="c1" shape="circle" which="first">Intersection entre a1 et c1</Intersection>
<Point name="P3" n="7" color="3" hidden="super" x="2*x(M1)-x(I1)" actx="-1.2881632995755412" y="2*y(M1)-y(I1)" acty="0.8879287706405898" shape="circle" fixed="true">Point à &quot;2*x(M1)-x(I1)&quot;, &quot;2*y(M1)-y(I1)&quot;</Point>
<Segment name="s2" n="8" color="3" target="true" from="I1" to="P3">Segment de I1 à P3</Segment>
<Plumb name="perp1" n="9" color="3" hidden="super" point="I1" line="s1" valid="true">Perpendiculaire passant par I1 à s1</Plumb>
<Intersection name="I2" n="10" color="3" hidden="super" first="perp1" second="s1" shape="circle">Intersection entre perp1 et s1</Intersection>
<Point name="P4" n="11" color="3" hidden="super" x="2*x(I2)-x(I1)" actx="-1.2377861553415777" y="2*y(I2)-y(I1)" acty="0.8553382300063748" shape="circle" fixed="true">Point à &quot;2*x(I2)-x(I1)&quot;, &quot;2*y(I2)-y(I1)&quot;</Point>
<Point name="P5" n="12" color="3" hidden="super" x="2*x(M1)-x(P4)" actx="-1.2317148273509426" y="2*y(M1)-y(P4)" acty="0.9751845439940403" shape="circle" fixed="true">Point à &quot;2*x(M1)-x(P4)&quot;, &quot;2*y(M1)-y(P4)&quot;</Point>
<Segment name="s3" n="13" color="3" target="true" from="P5" to="P4">Segment de P5 à P4</Segment>
</Objects>
</Macro>
<Macro Name="Segments barrés (codage)/Segment cercle" showduplicates="true">
<Parameter name="P1">P1</Parameter>
<Parameter name="P2">P2</Parameter>
<Objects>
<Point name="P1" n="0" x="-4.175182481751826" y="2.8175182481751815">Point à -4.17518, 2.81752</Point>
<Point name="P2" n="1" x="1.705681499059306" y="-0.9869954741747665">Point à 1.70568, -0.987</Point>
<Segment name="s1" n="2" color="3" target="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Midpoint name="M1" n="3" color="3" hidden="super" first="P1" second="P2" shape="circle">Milieu de P1 et P2</Midpoint>
<Circle name="c1" n="4" color="3" target="true" fixed="6/pixel" midpoint="M1" acute="true">Cercle de centre M1 de rayon 6/pixel</Circle>
</Objects>
</Macro>
<Macro Name="Segments barrés (codage)/Segment Z" showduplicates="true">
<Parameter name="P1">P1</Parameter>
<Parameter name="P2">P2</Parameter>
<Objects>
<Point name="P1" n="0" mainparameter="true" x="-4.175182481751826" y="2.8175182481751815">Point à -4.17518, 2.81752</Point>
<Point name="P2" n="1" mainparameter="true" x="-0.46849235957923524" y="-0.48227154662568333">Point à -0.46849, -0.48227</Point>
<Segment name="s1" n="2" color="3" target="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Midpoint name="M1" n="3" color="3" hidden="super" first="P1" second="P2" shape="circle">Milieu de P1 et P2</Midpoint>
<Plumb name="perp1" n="4" color="3" hidden="super" point="M1" line="s1" valid="true">Perpendiculaire passant par M1 à s1</Plumb>
<Circle name="c1" n="5" color="3" hidden="super" fixed="6/pixel" midpoint="M1" acute="true">Cercle de centre M1 de rayon 6/pixel</Circle>
<Intersection name="I1" n="6" color="3" hidden="super" first="perp1" second="c1" shape="circle" which="first">Intersection entre perp1 et c1</Intersection>
<Parallel name="par1" n="7" color="3" hidden="super" point="I1" line="s1">Parallèle passant par I1 à s1</Parallel>
<Intersection name="I2" n="8" color="3" hidden="super" first="perp1" second="c1" shape="circle" which="second">Intersection entre perp1 et c1</Intersection>
<Parallel name="par2" n="9" color="3" hidden="super" point="I2" line="s1">Parallèle passant par I2 à s1</Parallel>
<Circle name="c2" n="10" color="3" hidden="super" fixed="12/pixel" midpoint="I2" acute="true">Cercle de centre I2 de rayon 12/pixel</Circle>
<Circle name="c3" n="11" color="3" hidden="super" fixed="c2" midpoint="I1" acute="true">Cercle de centre I1 de rayon c2</Circle>
<Intersection name="I3" n="12" color="3" hidden="super" first="par1" second="c3" shape="circle" which="second">Intersection entre par1 et c3</Intersection>
<Midpoint name="M2" n="13" color="3" hidden="super" first="I3" second="I1" shape="circle">Milieu de I3 et I1</Midpoint>
<Intersection name="I4" n="14" color="3" hidden="super" first="par2" second="c2" shape="circle" which="first">Intersection entre par2 et c2</Intersection>
<Midpoint name="M3" n="15" color="3" hidden="super" first="I2" second="I4" shape="circle">Milieu de I2 et I4</Midpoint>
<Intersection name="I5" n="16" color="3" hidden="super" first="par2" second="c2" shape="circle" which="second">Intersection entre par2 et c2</Intersection>
<Intersection name="I6" n="17" color="3" hidden="super" first="par1" second="c3" shape="circle" which="first">Intersection entre par1 et c3</Intersection>
<Function name="f1" n="18" color="3" type="thick" target="true" large="true" x="(t==0)*x(I5)+(t==1)*x(M2)+(t==2)*x(M3)+(t==3)*x(I6)" y="(t==0)*y(I5)+(t==1)*y(M2)+(t==2)*y(M3)+(t==3)*y(I6)" var="t" min="0" max="3" d="1" shape="cross">Fonction ( (t==0)*x(I5)+(t==1)*x(M2)+(t==2)*x(M3)+(t==3)*x(I6) , (t==0)*y(I5)+(t==1)*y(M2)+(t==2)*y(M3)+(t==3)*y(I6) )</Function>
</Objects>
</Macro>
<Macro Name="Polygones/Régulier convexe avec dialogue">
<Parameter name="P1">O</Parameter>
<Parameter name="P2">M</Parameter>
<Comment>
<P>Montrer le centre du polygone, ainsi qu&apos;un sommet. Le
nombre de côté vous sera ensuite demandé   </P>
</Comment>
<Objects>
<Point name="P1" n="0" mainparameter="true" x="0.07611923821108714" y="0.08235852003166821">Point à 0.07612, 0.08236</Point>
<Point name="P2" n="1" mainparameter="true" x="0.5153646783799839" y="0.5465610874828883">Point à 0.51536, 0.54656</Point>
<Expression name="E1" n="2" color="4" type="thick" showname="true" showvalue="true" x="x(P1)+windoww/32" y="y(P1)-windoww/16" value="3" prompt="Nombre de côtés" fixed="true">Expression &quot;3&quot; à 0.32612, -0.41764</Expression>
<Function name="f1" n="3" color="4" type="thick" showname="true" large="true" x="x(P1)+d(P1,P2)*sin(t-a(P1,P1,P2))" y="y(P1)+d(P1,P2)*cos(t-a(P1,P1,P2))" var="t" min="0" max="360" d="360/E1" shape="cross">Fonction ( x(P1)+d(P1,P2)*sin(t-a(P1,P1,P2)) , y(P1)+d(P1,P2)*cos(t-a(P1,P1,P2)) )</Function>
<Point name="P3" n="4" hidden="super" showname="true" large="true" x="x(P1)+d(P1,P2)*sin(360/E1-a(P1,P1,P2))" actx="0.2585077340413549" y="y(P1)+d(P1,P2)*cos(360/E1-a(P1,P1,P2))" acty="-0.530140473376684" shape="circle" fixed="true">Point à &quot;x(P1)+d(P1,P2)*sin(360/E1-a(P1,P1,P2))&quot;, &quot;y(P1)+d(P1,P2)*cos(360/E1-a(P1,P1,P2))&quot;</Point>
<Polygon name="poly2" n="5" hidden="super" background="true" large="true" point1="P3" point2="P1" point3="P2">Polygon P3, P1, P2</Polygon>
<Expression name="E2" n="6" color="3" type="thick" showname="true" showvalue="true" x="x(P1)+windoww/32" y="y(P1)-windoww/8" value="d(P2,P3)*E1" prompt="Périmètre" fixed="true">Expression &quot;d(P2,P3)*E1&quot; à 0.32612, -0.91764</Expression>
<Expression name="E3" n="7" color="3" type="thick" showname="true" showvalue="true" x="x(P1)+windoww/32" y="y(P1)-3*windoww/16" value="poly2*E1" prompt="Aire" fixed="true">Expression &quot;poly2*E1&quot; à 0.32612, -1.41764</Expression>
</Objects>
<PromptFor object0="E1" prompt0="nombre de côtés du polygone"/>
</Macro>
<Macro Name="Polygones/Régulier convexe curseur" showduplicates="true">
<Parameter name="O">O</Parameter>
<Parameter name="M">M</Parameter>
<Parameter name="P9">P9</Parameter>
<Parameter name="P6">P6</Parameter>
<Comment>
<P>Montrer le centre du polygone, ainsi qu&apos;un sommet.   </P>
<P>Ensuite, montrer deux points qui définissent le curseur.   </P>
</Comment>
<Objects>
<Point name="P9" n="0" color="4" mainparameter="true" large="true" x="(windoww/(windoww-d(windoww)))*(x(P9)-windowcx)+windowcx+d(windowcx)" actx="-5.2038834951456305" y="(windoww/(windoww-d(windoww)))*(y(P9)-windowcy)+windowcy+d(windowcy)" acty="5.24271844660194" shape="circle" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(P9)-windowcx)+windowcx+d(windowcx)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(P9)-windowcy)+windowcy+d(windowcy)&quot;</Point>
<Point name="P6" n="1" color="4" mainparameter="true" large="true" x="if(P6,(x(@P9)/x(@P9))*(windoww/(windoww-d(windoww)))*(x(P6)-windowcx)+windowcx+d(windowcx),invalid)" actx="-2.8548050580933095" y="(windoww/(windoww-d(windoww)))*(y(P6)-windowcy)+windowcy+d(windowcy)" acty="5.2275542870632865" shape="circle" fixed="true">Point à &quot;if(P6,(x(@P9)/x(@P9))*(windoww/(windoww-d(windoww)))*(x(P6)-windowcx)+windowcx+d(windowcx),invalid)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(P6)-windowcy)+windowcy+d(windowcy)&quot;</Point>
<Expression name="E1" n="2" color="3" type="thick" showname="true" showvalue="true" bold="true" large="true" x="x(P9)" y="y(P9)+windoww/24" value="14" prompt="Nombre de divisions" fixed="true">Expression &quot;14&quot; à -5.20388, 5.57605</Expression>
<Point name="P3" n="3" color="5" hidden="super" showname="true" large="true" x="x(P9)+windoww/8" actx="-4.2038834951456305" y="y(P9)-windoww/12" acty="4.576051779935273" shape="circle" fixed="true">Point à &quot;x(P9)+windoww/8&quot;, &quot;y(P9)-windoww/12&quot;</Point>
<Point name="P4" n="4" color="5" hidden="super" large="true" x="x(P3)+3*windoww" actx="19.796116504854368" y="y(P3)" acty="4.576051779935273" shape="circle" fixed="true">Point à &quot;x(P3)+3*windoww&quot;, &quot;y(P3)&quot;</Point>
<Point name="P5" n="5" color="5" hidden="super" showname="true" large="true" x="x(P3)" actx="-4.2038834951456305" y="y(P9)" acty="5.24271844660194" shape="circle" fixed="true">Point à &quot;x(P3)&quot;, &quot;y(P9)&quot;</Point>
<Segment name="s1" n="6" color="5" hidden="super" large="true" from="P3" to="P4">Segment de P3 à P4</Segment>
<PointOn name="po6" n="7" color="5" large="true" on="s1" alpha="0.18217761557177614" x="0.16837927857699686" y="4.576051779935273" shape="diamond">Point sur s1</PointOn>
<Point name="P7" n="8" color="5" hidden="super" showname="true" large="true" x="x(po6)" actx="0.16837927857699686" y="y(P9)" acty="5.24271844660194" shape="circle" fixed="true">Point à &quot;x(po6)&quot;, &quot;y(P9)&quot;</Point>
<Segment name="s2" n="9" color="2" large="true" ctag0="z" cexpr0="1" from="P5" to="P7">Segment de P5 à P7</Segment>
<Expression name="E2" n="10" color="4" type="thick" hidden="super" showname="true" showvalue="true" x="-6.552381352872471" y="0.30289615780976226" value="(x(P7)-x(P5))/(E1-1)" prompt="p">Expression &quot;(x(P7)-x(P5))/(E1-1)&quot; à -6.55238, 0.3029</Expression>
<Point name="P8" n="11" color="5" hidden="super" showname="true" large="true" x="if(x(P6)&lt;x(P5),x(P5),if(x(P6)&gt;x(P7),x(P7),x(P5)+E2*round((x(P6)-x(P5))/E2)))" actx="-2.858571872461745" y="y(P9)" acty="5.24271844660194" shape="circle" fixed="true">Point à &quot;if(x(P6)&lt;x(P5),x(P5),if(x(P6)&gt;x(P7),x(P7),x(P5)+E2*round((x(P6)-x(P5))/E2)))&quot;, &quot;y(P9)&quot;</Point>
<Function name="f1" n="12" color="2" type="thin" showname="true" large="true" ctag0="z" cexpr0="2" x="x" y="y(P9)" var="x" min="x(P5)" max="x(P7)" d="E2" special="true" shape="cross">Fonction ( x , y(P9) )</Function>
<Circle name="c1" n="13" color="5" hidden="super" large="true" through="P6" fixed="windoww/512" midpoint="P8" acute="true">Cercle de centre P8 passant par P6 de rayon &quot;windoww/512&quot;</Circle>
<Expression name="E3" n="14" color="2" type="thick" showname="true" showvalue="true" bold="true" large="true" x="x(P9)-windoww/16" y="y(P9)-windoww/128" value="round((x(P8)-x(P5))/E2)+3" prompt="Valeur" fixed="true">Expression &quot;round((x(P8)-x(P5))/E2)+3&quot; à -5.70388, 5.18022</Expression>
<Point name="O" n="15" mainparameter="true" x="-0.5825242718446599" y="-0.07766990291262132">Point à -0.58252, -0.07767</Point>
<Point name="M" n="16" mainparameter="true" x="2.368932038834952" y="2.446601941747573">Point à 2.36893, 2.4466</Point>
<Function name="f2" n="17" color="4" type="thick" showname="true" large="true" x="x(O)+d(O,M)*sin(t-a(O,O,M))" y="y(O)+d(O,M)*cos(t-a(O,O,M))" var="t" min="0" max="360" d="360/E3" shape="cross">Fonction ( x(O)+d(O,M)*sin(t-a(O,O,M)) , y(O)+d(O,M)*cos(t-a(O,O,M)) )</Function>
<Point name="P12" n="18" color="4" hidden="super" showname="true" large="true" x="x(O)+d(O,M)*sin(360/E3-a(O,O,M))" actx="3.231233836958668" y="y(O)+d(O,M)*cos(360/E3-a(O,O,M))" acty="-0.8113536134661219" shape="circle" fixed="true">Point à &quot;x(O)+d(O,M)*sin(360/E3-a(O,O,M))&quot;, &quot;y(O)+d(O,M)*cos(360/E3-a(O,O,M))&quot;</Point>
<Polygon name="poly3" n="19" color="4" hidden="super" background="true" large="true" point1="P12" point2="O" point3="M">Polygone P12, P16, P17</Polygon>
<Expression name="E4" n="20" color="3" type="thick" showname="true" showvalue="true" x="x(O)+windoww/32" y="y(O)-windoww/8" value="d(M,P12)*E3" prompt="Périmètre" fixed="true">Expression &quot;d(M,P12)*E3&quot; à -0.33252, -1.07767</Expression>
<Expression name="E5" n="21" color="3" type="thick" showname="true" showvalue="true" x="x(O)+windoww/32" y="y(O)-3*windoww/16" value="poly3*E3" prompt="Aire" fixed="true">Expression &quot;poly3*E3&quot; à -0.33252, -1.57767</Expression>
</Objects>
</Macro>
<Macro Name="Polygones/Régulier convexe sans dialogue" showduplicates="true">
<Parameter name="O">O</Parameter>
<Parameter name="M">M</Parameter>
<Parameter name="E1">E1</Parameter>
<Comment>
<P>Montrer le centre du polygone, ainsi qu&apos;un sommet. Montrer
ensuite une expression qui représente le nombre de côtés.  </P>
</Comment>
<Objects>
<Point name="O" n="0" mainparameter="true" x="-0.08121827411167537" y="-0.15228426395939113">Point à -0.08122, -0.15228</Point>
<Point name="M" n="1" mainparameter="true" x="2.781725888324873" y="2.203045685279188">Point à 2.78173, 2.20305</Point>
<Expression name="E1" n="2" color="1" type="thick" showname="true" mainparameter="true" x="0.0" y="0.0" value="15" prompt="Valeur">Expression &quot;15&quot; à 0.0, 0.0</Expression>
<Function name="f1" n="3" color="4" type="thick" showname="true" large="true" x="x(O)+d(O,M)*sin(t-a(O,O,M))" y="y(O)+d(O,M)*cos(t-a(O,O,M))" var="t" min="0" max="360" d="360/E1" shape="cross">Fonction ( x(O)+d(O,M)*sin(t-a(O,O,M)) , y(O)+d(O,M)*cos(t-a(O,O,M)) )</Function>
<Point name="P3" n="4" hidden="super" showname="true" large="true" x="x(O)+d(O,M)*sin(360/E1-a(O,O,M))" actx="3.4922103578558295" y="y(O)+d(O,M)*cos(360/E1-a(O,O,M))" acty="0.8349524144741883" shape="circle" fixed="true">Point à &quot;x(O)+d(O,M)*sin(360/E1-a(O,O,M))&quot;, &quot;y(O)+d(O,M)*cos(360/E1-a(O,O,M))&quot;</Point>
<Polygon name="poly2" n="5" hidden="super" background="true" large="true" point1="P3" point2="O" point3="M">Polygone P3, P1, P2</Polygon>
<Expression name="E2" n="6" color="3" type="thick" showname="true" showvalue="true" x="x(O)+windoww/32" y="y(O)-windoww/8" value="d(M,P3)*E1" prompt="Périmètre" fixed="true">Expression &quot;d(M,P3)*E1&quot; à 0.16878, -1.15228</Expression>
<Expression name="E3" n="7" color="3" type="thick" showname="true" showvalue="true" x="x(O)+windoww/32" y="y(O)-3*windoww/16" value="poly2*E1" prompt="Aire" fixed="true">Expression &quot;poly2*E1&quot; à 0.16878, -1.65228</Expression>
</Objects>
</Macro>
<Macro Name="Polygones/Lignes polygonales/3" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="C">C</Parameter>
<Parameter name="D">D</Parameter>
<Comment>
<P>Montrer les 4 points qui définissent la ligne polygonale à
3 côtés.   </P>
</Comment>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-0.9927007299270073" y="6.759124087591244">Point à -0.9927, 6.75912</Point>
<Point name="B" n="1" mainparameter="true" x="1.897810218978104" y="6.437956204379561">Point à 1.89781, 6.43796</Point>
<Point name="C" n="2" mainparameter="true" x="3.7664233576642343" y="3.5474452554744538">Point à 3.76642, 3.54745</Point>
<Point name="D" n="3" mainparameter="true" x="1.3430656934306568" y="-0.6861313868613141">Point à 1.34307, -0.68613</Point>
<Function name="f1" n="4" color="5" type="thick" showname="true" target="true" large="true" x="(t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)" y="(t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)" var="t" min="0" max="3" d="1" shape="cross">Fonction ( (t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D) , (t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D) )</Function>
</Objects>
</Macro>
<Macro Name="Polygones/Lignes polygonales/4" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="C">C</Parameter>
<Parameter name="D">D</Parameter>
<Parameter name="E">E</Parameter>
<Objects>
<Point name="A" n="0" parameter="true" mainparameter="true" x="-0.9927007299270073" y="6.759124087591244">Point à -0.9927, 6.75912</Point>
<Point name="B" n="1" parameter="true" mainparameter="true" x="1.897810218978104" y="6.437956204379561">Point à 1.89781, 6.43796</Point>
<Point name="C" n="2" parameter="true" mainparameter="true" x="3.7664233576642343" y="3.5474452554744538">Point à 3.76642, 3.54745</Point>
<Point name="D" n="3" parameter="true" mainparameter="true" x="1.3430656934306568" y="-0.6861313868613141">Point à 1.34307, -0.68613</Point>
<Point name="E" n="4" parameter="true" mainparameter="true" x="-0.554744525547445" y="-3.109489051094893">Point à -0.55474, -3.10949</Point>
<Function name="f1" n="5" color="5" type="thick" showname="true" target="true" large="true" x="(t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)" y="(t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)" var="t" min="0" max="4" d="1" shape="cross">Fonction ( (t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E) , (t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E) )</Function>
</Objects>
</Macro>
<Macro Name="Polygones/Lignes polygonales/5" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="C">C</Parameter>
<Parameter name="D">D</Parameter>
<Parameter name="E">E</Parameter>
<Parameter name="F">F</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-0.9927007299270073" y="6.759124087591244">Point à -0.9927, 6.75912</Point>
<Point name="B" n="1" mainparameter="true" x="1.897810218978104" y="6.437956204379561">Point à 1.89781, 6.43796</Point>
<Point name="C" n="2" mainparameter="true" x="3.7664233576642343" y="3.5474452554744538">Point à 3.76642, 3.54745</Point>
<Point name="D" n="3" mainparameter="true" x="1.3430656934306568" y="-0.6861313868613141">Point à 1.34307, -0.68613</Point>
<Point name="E" n="4" mainparameter="true" x="-0.554744525547445" y="-3.109489051094893">Point à -0.55474, -3.10949</Point>
<Point name="F" n="5" mainparameter="true" x="-4.934306569343066" y="-1.9416058394160602">Point à -4.93431, -1.94161</Point>
<Function name="f1" n="6" color="5" type="thick" showname="true" target="true" large="true" x="(t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)" y="(t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)" var="t" min="0" max="5" d="1" shape="cross">Fonction ( (t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F) , (t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F) )</Function>
</Objects>
</Macro>
<Macro Name="Polygones/Lignes polygonales/6" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="C">C</Parameter>
<Parameter name="D">D</Parameter>
<Parameter name="E">E</Parameter>
<Parameter name="F">F</Parameter>
<Parameter name="G">G</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-0.9927007299270073" y="6.759124087591244">Point à -0.9927, 6.75912</Point>
<Point name="B" n="1" mainparameter="true" x="1.897810218978104" y="6.437956204379561">Point à 1.89781, 6.43796</Point>
<Point name="C" n="2" mainparameter="true" x="3.7664233576642343" y="3.5474452554744538">Point à 3.76642, 3.54745</Point>
<Point name="D" n="3" mainparameter="true" x="1.3430656934306568" y="-0.6861313868613141">Point à 1.34307, -0.68613</Point>
<Point name="E" n="4" mainparameter="true" x="-0.554744525547445" y="-3.109489051094893">Point à -0.55474, -3.10949</Point>
<Point name="F" n="5" mainparameter="true" x="-4.934306569343066" y="-1.9416058394160602">Point à -4.93431, -1.94161</Point>
<Point name="G" n="6" mainparameter="true" x="-6.189781021897812" y="-0.3649635036496353">Point à -6.18978, -0.36496</Point>
<Function name="f1" n="7" color="5" type="thick" showname="true" target="true" large="true" x="(t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)+(t==6)*x(G)" y="(t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)+(t==6)*y(G)" var="t" min="0" max="6" d="1" shape="cross">Fonction ( (t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)+(t==6)*x(G) , (t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)+(t==6)*y(G) )</Function>
</Objects>
</Macro>
<Macro Name="Polygones/Lignes polygonales/7" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="C">C</Parameter>
<Parameter name="D">D</Parameter>
<Parameter name="E">E</Parameter>
<Parameter name="F">F</Parameter>
<Parameter name="G">G</Parameter>
<Parameter name="H">H</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-0.9927007299270073" y="6.759124087591244">Point à -0.9927, 6.75912</Point>
<Point name="B" n="1" mainparameter="true" x="1.897810218978104" y="6.437956204379561">Point à 1.89781, 6.43796</Point>
<Point name="C" n="2" mainparameter="true" x="3.7664233576642343" y="3.5474452554744538">Point à 3.76642, 3.54745</Point>
<Point name="D" n="3" mainparameter="true" x="1.3430656934306568" y="-0.6861313868613141">Point à 1.34307, -0.68613</Point>
<Point name="E" n="4" mainparameter="true" x="-0.554744525547445" y="-3.109489051094893">Point à -0.55474, -3.10949</Point>
<Point name="F" n="5" mainparameter="true" x="-4.934306569343066" y="-1.9416058394160602">Point à -4.93431, -1.94161</Point>
<Point name="G" n="6" mainparameter="true" x="-6.189781021897812" y="-0.3649635036496353">Point à -6.18978, -0.36496</Point>
<Point name="H" n="7" mainparameter="true" x="-6.540145985401464" y="1.5036496350364965">Point à -6.54015, 1.50365</Point>
<Function name="f1" n="8" color="5" type="thick" showname="true" target="true" large="true" x="(t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)+(t==6)*x(G)+(t==7)*x(H)" y="(t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)+(t==6)*y(G)+(t==7)*y(H)" var="t" min="0" max="7" d="1" shape="cross">Fonction ( (t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)+(t==6)*x(G)+(t==7)*x(H) , (t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)+(t==6)*y(G)+(t==7)*y(H) )</Function>
</Objects>
</Macro>
<Macro Name="Polygones/Lignes polygonales/8" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="C">C</Parameter>
<Parameter name="D">D</Parameter>
<Parameter name="E">E</Parameter>
<Parameter name="F">F</Parameter>
<Parameter name="G">G</Parameter>
<Parameter name="H">H</Parameter>
<Parameter name="I">I</Parameter>
<Objects>
<Point name="A" n="0" x="-0.9927007299270073" y="6.759124087591244">Point à -0.9927, 6.75912</Point>
<Point name="B" n="1" x="1.897810218978104" y="6.437956204379561">Point à 1.89781, 6.43796</Point>
<Point name="C" n="2" x="3.7664233576642343" y="3.5474452554744538">Point à 3.76642, 3.54745</Point>
<Point name="D" n="3" x="1.3430656934306568" y="-0.6861313868613141">Point à 1.34307, -0.68613</Point>
<Point name="E" n="4" x="-0.554744525547445" y="-3.109489051094893">Point à -0.55474, -3.10949</Point>
<Point name="F" n="5" x="-4.934306569343066" y="-1.9416058394160602">Point à -4.93431, -1.94161</Point>
<Point name="G" n="6" x="-6.189781021897812" y="-0.3649635036496353">Point à -6.18978, -0.36496</Point>
<Point name="H" n="7" x="-6.540145985401464" y="1.5036496350364965">Point à -6.54015, 1.50365</Point>
<Point name="I" n="8" x="-7.036496350364963" y="2.700729927007301">Point à -7.0365, 2.70073</Point>
<Function name="f1" n="9" color="5" type="thick" showname="true" target="true" large="true" x="(t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)+(t==6)*x(G)+(t==7)*x(H)+(t==8)*x(I)" y="(t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)+(t==6)*y(G)+(t==7)*y(H)+(t==8)*y(I)" var="t" min="0" max="8" d="1" shape="cross">Fonction ( (t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)+(t==6)*x(G)+(t==7)*x(H)+(t==8)*x(I) , (t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)+(t==6)*y(G)+(t==7)*y(H)+(t==8)*y(I) )</Function>
</Objects>
</Macro>
<Macro Name="Polygones/Lignes polygonales/9" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="C">C</Parameter>
<Parameter name="D">D</Parameter>
<Parameter name="E">E</Parameter>
<Parameter name="F">F</Parameter>
<Parameter name="G">G</Parameter>
<Parameter name="H">H</Parameter>
<Parameter name="I">I</Parameter>
<Parameter name="J">J</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-0.9927007299270073" y="6.759124087591244">Point à -0.9927, 6.75912</Point>
<Point name="B" n="1" mainparameter="true" x="1.897810218978104" y="6.437956204379561">Point à 1.89781, 6.43796</Point>
<Point name="C" n="2" mainparameter="true" x="3.7664233576642343" y="3.5474452554744538">Point à 3.76642, 3.54745</Point>
<Point name="D" n="3" mainparameter="true" x="1.3430656934306568" y="-0.6861313868613141">Point à 1.34307, -0.68613</Point>
<Point name="E" n="4" mainparameter="true" x="-0.554744525547445" y="-3.109489051094893">Point à -0.55474, -3.10949</Point>
<Point name="F" n="5" mainparameter="true" x="-4.934306569343066" y="-1.9416058394160602">Point à -4.93431, -1.94161</Point>
<Point name="G" n="6" mainparameter="true" x="-6.189781021897812" y="-0.3649635036496353">Point à -6.18978, -0.36496</Point>
<Point name="H" n="7" mainparameter="true" x="-6.540145985401464" y="1.5036496350364965">Point à -6.54015, 1.50365</Point>
<Point name="I" n="8" mainparameter="true" x="-7.036496350364963" y="2.700729927007301">Point à -7.0365, 2.70073</Point>
<Point name="J" n="9" mainparameter="true" x="-7.007299270072998" y="4.306569343065696">Point à -7.0073, 4.30657</Point>
<Function name="f1" n="10" color="5" type="thick" showname="true" target="true" large="true" x="(t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)+(t==6)*x(G)+(t==7)*x(H)+(t==8)*x(I)+(t==9)*x(J)" y="(t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)+(t==6)*y(G)+(t==7)*y(H)+(t==8)*y(I)+(t==9)*y(J)" var="t" min="0" max="9" d="1" shape="cross">Fonction ( (t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)+(t==6)*x(G)+(t==7)*x(H)+(t==8)*x(I)+(t==9)*x(J) , (t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)+(t==6)*y(G)+(t==7)*y(H)+(t==8)*y(I)+(t==9)*y(J) )</Function>
</Objects>
</Macro>
<Macro Name="Polygones/Lignes polygonales/10" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="C">C</Parameter>
<Parameter name="D">D</Parameter>
<Parameter name="E">E</Parameter>
<Parameter name="F">F</Parameter>
<Parameter name="G">G</Parameter>
<Parameter name="H">H</Parameter>
<Parameter name="I">I</Parameter>
<Parameter name="J">J</Parameter>
<Parameter name="K">K</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-0.9927007299270073" y="6.759124087591244">Point à -0.9927, 6.75912</Point>
<Point name="B" n="1" mainparameter="true" x="1.897810218978104" y="6.437956204379561">Point à 1.89781, 6.43796</Point>
<Point name="C" n="2" mainparameter="true" x="3.7664233576642343" y="3.5474452554744538">Point à 3.76642, 3.54745</Point>
<Point name="D" n="3" mainparameter="true" x="1.3430656934306568" y="-0.6861313868613141">Point à 1.34307, -0.68613</Point>
<Point name="E" n="4" mainparameter="true" x="-0.554744525547445" y="-3.109489051094893">Point à -0.55474, -3.10949</Point>
<Point name="F" n="5" mainparameter="true" x="-4.934306569343066" y="-1.9416058394160602">Point à -4.93431, -1.94161</Point>
<Point name="G" n="6" mainparameter="true" x="-6.189781021897812" y="-0.3649635036496353">Point à -6.18978, -0.36496</Point>
<Point name="H" n="7" mainparameter="true" x="-6.540145985401464" y="1.5036496350364965">Point à -6.54015, 1.50365</Point>
<Point name="I" n="8" mainparameter="true" x="-7.036496350364963" y="2.700729927007301">Point à -7.0365, 2.70073</Point>
<Point name="J" n="9" mainparameter="true" x="-7.007299270072998" y="4.306569343065696">Point à -7.0073, 4.30657</Point>
<Point name="K" n="10" mainparameter="true" x="-5.839416058394162" y="5.153284671532846">Point à -5.83942, 5.15328</Point>
<Function name="f1" n="11" color="5" type="thick" showname="true" target="true" large="true" x="(t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)+(t==6)*x(G)+(t==7)*x(H)+(t==8)*x(I)+(t==9)*x(J)+(t==10)*x(K)" y="(t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)+(t==6)*y(G)+(t==7)*y(H)+(t==8)*y(I)+(t==9)*y(J)+(t==10)*y(K)" var="t" min="0" max="10" d="1" shape="cross">Fonction ( (t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)+(t==6)*x(G)+(t==7)*x(H)+(t==8)*x(I)+(t==9)*x(J)+(t==10)*x(K) , (t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)+(t==6)*y(G)+(t==7)*y(H)+(t==8)*y(I)+(t==9)*y(J)+(t==10)*y(K) )</Function>
</Objects>
</Macro>
<Macro Name="Polygones/Lignes polygonales/11" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="C">C</Parameter>
<Parameter name="D">D</Parameter>
<Parameter name="E">E</Parameter>
<Parameter name="F">F</Parameter>
<Parameter name="G">G</Parameter>
<Parameter name="H">H</Parameter>
<Parameter name="I">I</Parameter>
<Parameter name="J">J</Parameter>
<Parameter name="K">K</Parameter>
<Parameter name="L">L</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-0.9927007299270073" y="6.759124087591244">Point à -0.9927, 6.75912</Point>
<Point name="B" n="1" mainparameter="true" x="1.897810218978104" y="6.437956204379561">Point à 1.89781, 6.43796</Point>
<Point name="C" n="2" mainparameter="true" x="3.7664233576642343" y="3.5474452554744538">Point à 3.76642, 3.54745</Point>
<Point name="D" n="3" mainparameter="true" x="1.3430656934306568" y="-0.6861313868613141">Point à 1.34307, -0.68613</Point>
<Point name="E" n="4" mainparameter="true" x="-0.554744525547445" y="-3.109489051094893">Point à -0.55474, -3.10949</Point>
<Point name="F" n="5" mainparameter="true" x="-4.934306569343066" y="-1.9416058394160602">Point à -4.93431, -1.94161</Point>
<Point name="G" n="6" mainparameter="true" x="-6.189781021897812" y="-0.3649635036496353">Point à -6.18978, -0.36496</Point>
<Point name="H" n="7" mainparameter="true" x="-6.540145985401464" y="1.5036496350364965">Point à -6.54015, 1.50365</Point>
<Point name="I" n="8" mainparameter="true" x="-7.036496350364963" y="2.700729927007301">Point à -7.0365, 2.70073</Point>
<Point name="J" n="9" mainparameter="true" x="-7.007299270072998" y="4.306569343065696">Point à -7.0073, 4.30657</Point>
<Point name="K" n="10" mainparameter="true" x="-5.839416058394162" y="5.153284671532846">Point à -5.83942, 5.15328</Point>
<Point name="L" n="11" mainparameter="true" x="-4.64233576642336" y="6.379562043795621">Point à -4.64234, 6.37956</Point>
<Function name="f1" n="12" color="5" type="thick" showname="true" target="true" large="true" x="(t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)+(t==6)*x(G)+(t==7)*x(H)+(t==8)*x(I)+(t==9)*x(J)+(t==10)*x(K)+(t==11)*x(L)" y="(t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)+(t==6)*y(G)+(t==7)*y(H)+(t==8)*y(I)+(t==9)*y(J)+(t==10)*y(K)+(t==11)*y(L)" var="t" min="0" max="11" d="1" shape="cross">Fonction ( (t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)+(t==6)*x(G)+(t==7)*x(H)+(t==8)*x(I)+(t==9)*x(J)+(t==10)*x(K)+(t==11)*x(L) , (t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)+(t==6)*y(G)+(t==7)*y(H)+(t==8)*y(I)+(t==9)*y(J)+(t==10)*y(K)+(t==11)*y(L) )</Function>
</Objects>
</Macro>
<Macro Name="Polygones/Lignes polygonales/12" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="C">C</Parameter>
<Parameter name="D">D</Parameter>
<Parameter name="E">E</Parameter>
<Parameter name="F">F</Parameter>
<Parameter name="G">G</Parameter>
<Parameter name="H">H</Parameter>
<Parameter name="I">I</Parameter>
<Parameter name="J">J</Parameter>
<Parameter name="K">K</Parameter>
<Parameter name="L">L</Parameter>
<Parameter name="M">M</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-0.9927007299270073" y="6.759124087591244">Point à -0.9927, 6.75912</Point>
<Point name="B" n="1" mainparameter="true" x="1.897810218978104" y="6.437956204379561">Point à 1.89781, 6.43796</Point>
<Point name="C" n="2" mainparameter="true" x="3.7664233576642343" y="3.5474452554744538">Point à 3.76642, 3.54745</Point>
<Point name="D" n="3" mainparameter="true" x="1.3430656934306568" y="-0.6861313868613141">Point à 1.34307, -0.68613</Point>
<Point name="E" n="4" mainparameter="true" x="-0.554744525547445" y="-3.109489051094893">Point à -0.55474, -3.10949</Point>
<Point name="F" n="5" mainparameter="true" x="-4.934306569343066" y="-1.9416058394160602">Point à -4.93431, -1.94161</Point>
<Point name="G" n="6" mainparameter="true" x="-6.189781021897812" y="-0.3649635036496353">Point à -6.18978, -0.36496</Point>
<Point name="H" n="7" mainparameter="true" x="-6.540145985401464" y="1.5036496350364965">Point à -6.54015, 1.50365</Point>
<Point name="I" n="8" mainparameter="true" x="-7.036496350364963" y="2.700729927007301">Point à -7.0365, 2.70073</Point>
<Point name="J" n="9" mainparameter="true" x="-7.007299270072998" y="4.306569343065696">Point à -7.0073, 4.30657</Point>
<Point name="K" n="10" mainparameter="true" x="-5.839416058394162" y="5.153284671532846">Point à -5.83942, 5.15328</Point>
<Point name="L" n="11" mainparameter="true" x="-4.64233576642336" y="6.379562043795621">Point à -4.64234, 6.37956</Point>
<Point name="M" n="12" mainparameter="true" x="-0.9635036496350369" y="3.5766423357664214">Point à -0.9635, 3.57664</Point>
<Function name="f1" n="13" color="5" type="thick" showname="true" target="true" large="true" x="(t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)+(t==6)*x(G)+(t==7)*x(H)+(t==8)*x(I)+(t==9)*x(J)+(t==10)*x(K)+(t==11)*x(L)+(t==12)*x(M)" y="(t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)+(t==6)*y(G)+(t==7)*y(H)+(t==8)*y(I)+(t==9)*y(J)+(t==10)*y(K)+(t==11)*y(L)+(t==12)*y(M)" var="t" min="0" max="12" d="1" shape="cross">Fonction ( (t==0)*x(A)+(t==1)*x(B)+(t==2)*x(C)+(t==3)*x(D)+(t==4)*x(E)+(t==5)*x(F)+(t==6)*x(G)+(t==7)*x(H)+(t==8)*x(I)+(t==9)*x(J)+(t==10)*x(K)+(t==11)*x(L)+(t==12)*x(M) , (t==0)*y(A)+(t==1)*y(B)+(t==2)*y(C)+(t==3)*y(D)+(t==4)*y(E)+(t==5)*y(F)+(t==6)*y(G)+(t==7)*y(H)+(t==8)*y(I)+(t==9)*y(J)+(t==10)*y(K)+(t==11)*y(L)+(t==12)*y(M) )</Function>
</Objects>
</Macro>
<Macro Name="Polygones/Triangles/Isocèle" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-2.4958481613285888" y="0.6927639383155394">Point à -2.49585, 0.69276</Point>
<Point name="B" n="1" mainparameter="true" x="-0.6927639383155394" y="-0.12336892052194505">Point à -0.69276, -0.12337</Point>
<Segment name="s1" n="2" color="1" target="true" from="A" to="B">Segment de A à B</Segment>
<Circle name="c1" n="3" color="2" hidden="super" bold="true" through="B" midpoint="A" acute="true">Cercle de centre A passant par B</Circle>
<Circle name="c2" n="4" color="2" hidden="super" bold="true" through="A" midpoint="B" acute="true">Cercle de centre B passant par A</Circle>
<Intersection name="P3" n="5" color="2" hidden="super" bold="true" first="c2" second="c1" shape="circle" which="second">Intersection entre c2 et c1</Intersection>
<Intersection name="P1" n="6" color="2" hidden="super" bold="true" first="c1" second="c2" shape="circle" which="second">Intersection entre c1 et c2</Intersection>
<Line name="l1" n="7" color="3" hidden="super" from="P3" to="P1">Droite passant par P3 et P1</Line>
<PointOn name="D" n="8" color="2" showname="true" xcoffset="0.30693262210324423" ycoffset="0.10761501259072404" keepclose="true" target="true" on="l1" alpha="4.749270811445276" x="-0.34270088103191676" y="3.0498717190145683" shape="circle">Point sur l1</PointOn>
<Segment name="s2" n="9" color="1" target="true" from="A" to="D">Segment de A à D</Segment>
<Segment name="s3" n="10" color="1" target="true" from="D" to="B">Segment de D à B</Segment>
</Objects>
</Macro>
<Macro Name="Polygones/Triangles/Rectangle" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Objects>
<Point name="A" n="0" x="-2.991974317817015" y="-0.9630818619582665">Point à -2.99197, -0.96308</Point>
<Point name="B" n="1" x="3.1974317817014453" y="-2.426966292134831">Point à 3.19743, -2.42697</Point>
<Segment name="s1" n="2" color="1" target="true" from="A" to="B">Segment de A à B</Segment>
<Segment name="s2" n="3" hidden="super" large="true" from="A" to="B">Segment de A à B</Segment>
<Circle name="c1" n="4" hidden="super" large="true" fixed="windoww/24" midpoint="A" acute="true">Cercle de centre A de rayon windoww/24</Circle>
<Plumb name="perp1" n="5" color="3" hidden="super" point="A" line="s1" valid="true">Perpendiculaire passant par A à s1</Plumb>
<Intersection name="P4" n="6" hidden="super" large="true" first="s2" second="c1" shape="circle" which="first">Intersection entre s2 et c1</Intersection>
<PointOn name="D" n="7" color="2" showname="true" xcoffset="-0.566010714018947" ycoffset="0.7104263848642307" keepclose="true" target="true" on="perp1" alpha="3.2347347630397962" x="-2.2474531512416456" y="2.1848058774218053" shape="circle">Point sur perp1</PointOn>
<Segment name="s3" n="8" color="1" target="true" from="D" to="A">Segment de D à A</Segment>
<Segment name="s4" n="9" color="1" target="true" from="D" to="B">Segment de D à B</Segment>
<Segment name="s5" n="10" hidden="super" large="true" from="D" to="A">Segment de D à A</Segment>
<Intersection name="P1" n="11" hidden="super" large="true" first="s5" second="c1" shape="circle" which="second">Intersection entre s5 et c1</Intersection>
<Parallel name="par1" n="12" hidden="super" large="true" point="P4" line="s5">Parallèle passant par P4 à s5</Parallel>
<Parallel name="par2" n="13" hidden="super" large="true" point="P1" line="s2">Parallèle passant par P1 à s2</Parallel>
<Intersection name="P2" n="14" hidden="super" large="true" first="par1" second="par2" shape="circle">Intersection entre par1 et par2</Intersection>
<Segment name="s6" n="15" color="1" target="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Segment name="s7" n="16" color="1" target="true" from="P2" to="P4">Segment de P2 à P4</Segment>
</Objects>
</Macro>
<Macro Name="Polygones/Triangles/Isocèle-rectangle" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-3.09470304975923" y="-0.5521669341894055">Point à -3.0947, -0.55217</Point>
<Point name="B" n="1" mainparameter="true" x="2.375601926163723" y="-1.3739967897271272">Point à 2.3756, -1.374</Point>
<Segment name="s1" n="2" color="1" target="true" from="A" to="B">Segment de A à B</Segment>
<Circle name="c1" n="3" color="4" hidden="super" through="B" midpoint="A" acute="true">Cercle de centre A passant par B</Circle>
<Segment name="s2" n="4" hidden="super" large="true" from="A" to="B">Segment de A à B</Segment>
<Circle name="c2" n="5" hidden="super" large="true" fixed="windoww/24" midpoint="A" acute="true">Cercle de centre A de rayon windoww/24</Circle>
<Plumb name="perp1" n="6" color="3" hidden="super" point="A" line="s1" valid="true">Perpendiculaire passant par A à s1</Plumb>
<Intersection name="P4" n="7" hidden="super" large="true" first="s2" second="c2" shape="circle" which="first">Intersection entre s2 et c2</Intersection>
<Intersection name="D" n="8" color="2" showname="true" xcoffset="-0.2568218298555385" ycoffset="0.3595505617977519" keepclose="true" target="true" first="perp1" second="c1" shape="circle" which="first">Intersection entre perp1 et c1</Intersection>
<Segment name="s3" n="9" color="1" target="true" from="D" to="A">Segment de D à A</Segment>
<Segment name="s4" n="10" color="1" target="true" from="D" to="B">Segment de D à B</Segment>
<Segment name="s5" n="11" hidden="super" large="true" from="D" to="A">Segment de D à A</Segment>
<Intersection name="P1" n="12" hidden="super" large="true" first="s5" second="c2" shape="circle" which="second">Intersection entre s5 et c2</Intersection>
<Parallel name="par1" n="13" hidden="super" large="true" point="P4" line="s5">Parallèle passant par P4 à s5</Parallel>
<Parallel name="par2" n="14" hidden="super" large="true" point="P1" line="s2">Parallèle passant par P1 à s2</Parallel>
<Intersection name="P2" n="15" hidden="super" large="true" first="par1" second="par2" shape="circle">Intersection entre par1 et par2</Intersection>
<Segment name="s6" n="16" color="1" target="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Segment name="s7" n="17" color="1" target="true" from="P2" to="P4">Segment de P2 à P4</Segment>
</Objects>
</Macro>
<Macro Name="Polygones/Triangles/Equilatéral" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-2.5040128410914932" y="0.295345104333868">Point à -2.50401, 0.29535</Point>
<Point name="B" n="1" mainparameter="true" x="1.7335473515248803" y="-1.2969502407704658">Point à 1.73355, -1.29695</Point>
<Circle name="c1" n="2" color="4" hidden="super" through="B" midpoint="A" acute="true">Cercle de centre A passant par B</Circle>
<Circle name="c2" n="3" color="4" hidden="super" through="A" midpoint="B" acute="true">Cercle de centre B passant par A</Circle>
<Segment name="s1" n="4" color="1" target="true" from="B" to="A">Segment de B à A</Segment>
<Intersection name="D" n="5" color="2" showname="true" target="true" first="c1" second="c2" shape="circle" which="second">Intersection entre c1 et c2</Intersection>
<Segment name="s2" n="6" color="1" target="true" from="A" to="D">Segment de A à D</Segment>
<Segment name="s3" n="7" color="1" target="true" from="D" to="B">Segment de D à B</Segment>
</Objects>
</Macro>
<Macro Name="Polygones/Quadrilatères/Trapèze" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-4.4301765650080265" y="-0.3210272873194224">Point à -4.43018, -0.32103</Point>
<Point name="B" n="1" mainparameter="true" x="2.914927768860354" y="-1.0401284109149274">Point à 2.91493, -1.04013</Point>
<Segment name="s1" n="2" color="1" target="true" from="A" to="B">Segment de A à B</Segment>
<Plumb name="perp1" n="3" color="3" hidden="super" point="A" line="s1" valid="true">Perpendiculaire passant par A à s1</Plumb>
<PointOn name="F" n="4" color="2" showname="true" xcoffset="-0.47857308074625404" ycoffset="0.9855999552163426" keepclose="true" target="true" on="perp1" alpha="2.448217219151552" x="-4.191631442947294" y="2.11554074515806" shape="circle">Point sur perp1</PointOn>
<Plumb name="perp2" n="5" color="3" hidden="super" point="F" line="perp1" valid="true">Perpendiculaire passant par F à perp1</Plumb>
<PointOn name="P1" n="6" color="2" showname="true" xcoffset="-0.13683397431237498" ycoffset="0.7156154623348823" keepclose="true" target="true" on="perp2" alpha="-1.365403470621208" x="-2.8327248794592697" y="1.9825009417396522" shape="circle">Point sur perp2</PointOn>
<PointOn name="P2" n="7" color="2" showname="true" xcoffset="0.2344834932185016" ycoffset="0.5221164787139598" keepclose="true" target="true" on="perp2" alpha="-5.629451800918311" x="1.411034117604526" y="1.567028032936204" shape="circle">Point sur perp2</PointOn>
<Segment name="s2" n="8" color="1" target="true" from="A" to="P1">Segment de A à P1</Segment>
<Segment name="s3" n="9" color="1" target="true" from="P1" to="P2">Segment de P1 à P2</Segment>
<Segment name="s4" n="10" color="1" target="true" from="P2" to="B">Segment de P2 à B</Segment>
</Objects>
</Macro>
<Macro Name="Polygones/Quadrilatères/Cerf-volant" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-3.4285714285714293" y="1.1942215088282513">Point à -3.42857, 1.19422</Point>
<Point name="B" n="1" mainparameter="true" x="3.4285714285714293" y="-1.1942215088282506">Point à 3.42857, -1.19422</Point>
<Segment name="s1" n="2" color="1" target="true" from="A" to="B">Segment de A à B</Segment>
<PointOn name="G" n="3" color="2" showname="true" target="true" on="s1" alpha="0.3153193725136982" x="-1.2663814456203557" y="0.44109915521607934" shape="circle">Point sur s1</PointOn>
<Plumb name="perp1" n="4" color="3" hidden="super" point="G" line="s1" valid="true">Perpendiculaire passant par G à s1</Plumb>
<Circle name="c1" n="5" color="4" hidden="super" fixed="1" midpoint="G" acute="true">Cercle de centre G de rayon 1</Circle>
<Intersection name="P1" n="6" color="2" hidden="super" showname="true" first="perp1" second="c1" shape="circle" which="first">Intersection entre perp1 et c1</Intersection>
<Ray name="r1" n="7" color="3" hidden="super" from="G" to="P1">Demi-droite d&apos;origine G vers P1</Ray>
<PointOn name="P2" n="8" color="2" showname="true" xcoffset="0.4573926815754116" ycoffset="0.21379651091554175" keepclose="true" target="true" on="r1" alpha="2.420400981744265" x="-0.4702337730681885" y="2.7268134409303655" shape="circle">Point sur r1</PointOn>
<Point name="P3" n="9" color="2" showname="true" target="true" x="2*x(G)-x(P2)" actx="-2.062529118172523" y="2*y(G)-y(P2)" acty="-1.8446151304982068" shape="circle" fixed="true">Point à &quot;2*x(G)-x(P2)&quot;, &quot;2*y(G)-y(P2)&quot;</Point>
<Segment name="s2" n="10" color="1" target="true" from="A" to="P2">Segment de A à P2</Segment>
<Segment name="s3" n="11" color="1" target="true" from="P2" to="B">Segment de P2 à B</Segment>
<Segment name="s4" n="12" color="1" target="true" from="B" to="P3">Segment de B à P3</Segment>
<Segment name="s5" n="13" color="1" target="true" from="P3" to="A">Segment de P3 à A</Segment>
<Segment name="s6" n="14" color="1" target="true" from="P2" to="P3">Segment de P2 à P3</Segment>
</Objects>
</Macro>
<Macro Name="Polygones/Quadrilatères/Parallélogramme" showduplicates="true">
<Parameter name="C">C</Parameter>
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-2.426966292134831" y="0.7062600321027289">Point à -2.42697, 0.70626</Point>
<Point name="B" n="1" mainparameter="true" x="3.300160513643659" y="-0.808988764044944">Point à 3.30016, -0.80899</Point>
<Point name="C" n="2" mainparameter="true" x="0.21829855537720638" y="3.146067415730338">Point à 0.2183, 3.14607</Point>
<Segment name="s1" n="3" color="1" target="true" from="A" to="B">Segment de A à B</Segment>
<Point name="E" n="4" color="2" showname="true" target="true" x="x(C)+x(B)-x(A)" actx="5.945425361155697" y="y(C)+y(B)-y(A)" acty="1.6308186195826653" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Segment name="s2" n="5" color="1" target="true" from="A" to="C">Segment de A à C</Segment>
<Segment name="s3" n="6" color="1" target="true" from="C" to="E">Segment de C à E</Segment>
<Segment name="s4" n="7" color="1" target="true" from="E" to="B">Segment de E à B</Segment>
</Objects>
</Macro>
<Macro Name="Polygones/Quadrilatères/Losange" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-3.274478330658106" y="1.4767255216693425">Point à -3.27448, 1.47673</Point>
<Point name="B" n="1" mainparameter="true" x="4.404494382022472" y="-1.1685393258426968">Point à 4.40449, -1.16854</Point>
<Segment name="s1" n="2" color="1" target="true" from="A" to="B">Segment de A à B</Segment>
<Circle name="c1" n="3" color="2" hidden="super" bold="true" through="B" midpoint="A" acute="true">Cercle de centre A passant par B</Circle>
<Circle name="c2" n="4" color="2" hidden="super" bold="true" through="A" midpoint="B" acute="true">Cercle de centre B passant par A</Circle>
<Intersection name="P5" n="5" color="2" hidden="super" bold="true" first="c2" second="c1" shape="circle" which="second">Intersection entre c2 et c1</Intersection>
<Intersection name="P1" n="6" color="2" hidden="super" bold="true" first="c1" second="c2" shape="circle" which="second">Intersection entre c1 et c2</Intersection>
<PointOn name="P2" n="7" color="2" hidden="super" on="s1" alpha="9.884663042646986" x="72.62957944787027" y="-24.670826154546127" shape="circle">Point sur s1</PointOn>
<Line name="l1" n="8" color="3" hidden="super" from="P5" to="P1">Droite passant par P5 et P1</Line>
<PointOn name="E" n="9" color="2" showname="true" xcoffset="0.5900731275187079" ycoffset="0.12063538429463705" keepclose="true" target="true" on="l1" alpha="8.483795269455184" x="1.0372992877277274" y="1.5251133634630119" shape="circle">Point sur l1</PointOn>
<Point name="P3" n="10" color="2" hidden="super" x="x(E)" actx="1.0372992877277274" y="y(E)" acty="1.5251133634630119" shape="circle" fixed="true">Point à &quot;x(E)&quot;, &quot;y(E)&quot;</Point>
<Segment name="s2" n="11" color="1" target="true" from="A" to="E">Segment de A à E</Segment>
<Segment name="s3" n="12" color="1" target="true" from="E" to="B">Segment de E à B</Segment>
<Plumb name="perp1" n="13" color="3" hidden="super" point="P3" line="s1" valid="true">Perpendiculaire passant par P3 à s1</Plumb>
<Circle name="c3" n="14" color="4" hidden="super" through="P3" midpoint="P2" acute="true">Cercle de centre P2 passant par P3</Circle>
<Intersection name="P4" n="15" color="2" showname="true" xcoffset="0.18039236204783715" ycoffset="-0.6085968610203816" keepclose="true" target="true" first="perp1" second="c3" awayfrom="P3" shape="circle" which="second">Intersection entre perp1 et c3</Intersection>
<Segment name="s4" n="16" color="1" target="true" from="B" to="P4">Segment de B à P4</Segment>
<Segment name="s5" n="17" color="1" target="true" from="P4" to="A">Segment de P4 à A</Segment>
<Segment name="s6" n="18" color="1" target="true" from="E" to="P4">Segment de E à P4</Segment>
</Objects>
</Macro>
<Macro Name="Polygones/Quadrilatères/Rectangle" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-2.287069988137604" y="0.44602609727164927">Point à -2.28707, 0.44603</Point>
<Point name="B" n="1" mainparameter="true" x="2.7615658362989333" y="-0.23724792408066442">Point à 2.76157, -0.23725</Point>
<Segment name="s1" n="2" color="1" target="true" from="A" to="B">Segment de A à B</Segment>
<Segment name="s2" n="3" hidden="super" large="true" from="A" to="B">Segment de A à B</Segment>
<Circle name="c1" n="4" hidden="super" large="true" fixed="windoww/24" midpoint="A" acute="true">Cercle de centre A de rayon windoww/24</Circle>
<Segment name="s3" n="5" hidden="super" large="true" from="A" to="B">Segment de A à B</Segment>
<Circle name="c2" n="6" hidden="super" large="true" fixed="windoww/24" midpoint="B" acute="true">Cercle de centre B de rayon windoww/24</Circle>
<Plumb name="perp1" n="7" color="3" hidden="super" point="A" line="s1" valid="true">Perpendiculaire passant par A à s1</Plumb>
<Intersection name="P13" n="8" hidden="super" large="true" first="s2" second="c1" shape="circle" which="first">Intersection entre s2 et c1</Intersection>
<Intersection name="P1" n="9" hidden="super" large="true" first="s3" second="c2" shape="circle" which="second">Intersection entre s3 et c2</Intersection>
<PointOn name="E" n="10" color="2" showname="true" xcoffset="-0.37135292584267465" ycoffset="0.6773674973875408" keepclose="true" target="true" on="perp1" alpha="2.300277076568955" x="-1.9785668173354962" y="2.7255217481983327" shape="circle">Point sur perp1</PointOn>
<Point name="P2" n="11" color="2" showname="true" xcoffset="0.4098667874414952" ycoffset="0.2563076503610433" keepclose="true" target="true" x="x(E)+x(B)-x(A)" actx="3.070069007101041" y="y(E)+y(B)-y(A)" acty="2.042247726846019" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Segment name="s4" n="12" color="1" target="true" from="A" to="E">Segment de A à E</Segment>
<Segment name="s5" n="13" hidden="super" large="true" from="E" to="A">Segment de E à A</Segment>
<Segment name="s6" n="14" hidden="super" large="true" from="A" to="E">Segment de A à E</Segment>
<Circle name="c3" n="15" hidden="super" large="true" fixed="windoww/24" midpoint="E" acute="true">Cercle de centre E de rayon windoww/24</Circle>
<Segment name="s7" n="16" color="1" target="true" from="E" to="P2">Segment de E à P2</Segment>
<Segment name="s8" n="17" color="1" target="true" from="P2" to="B">Segment de P2 à B</Segment>
<Intersection name="P3" n="18" hidden="super" large="true" first="s5" second="c1" shape="circle" which="second">Intersection entre s5 et c1</Intersection>
<Parallel name="par1" n="19" hidden="super" large="true" point="P13" line="s5">Parallèle passant par P13 à s5</Parallel>
<Segment name="s9" n="20" hidden="super" large="true" from="B" to="P2">Segment de B à P2</Segment>
<Segment name="s10" n="21" hidden="super" large="true" from="E" to="P2">Segment de E à P2</Segment>
<Intersection name="P4" n="22" hidden="super" large="true" first="s6" second="c3" shape="circle" which="second">Intersection entre s6 et c3</Intersection>
<Segment name="s11" n="23" hidden="super" large="true" from="E" to="P2">Segment de E à P2</Segment>
<Segment name="s12" n="24" hidden="super" large="true" from="P2" to="B">Segment de P2 à B</Segment>
<Circle name="c4" n="25" hidden="super" large="true" fixed="windoww/24" midpoint="P2" acute="true">Cercle de centre P2 de rayon windoww/24</Circle>
<Parallel name="par2" n="26" hidden="super" large="true" point="P3" line="s2">Parallèle passant par P3 à s2</Parallel>
<Parallel name="par3" n="27" hidden="super" large="true" point="P1" line="s9">Parallèle passant par P1 à s9</Parallel>
<Intersection name="P5" n="28" hidden="super" large="true" first="s9" second="c2" shape="circle" which="first">Intersection entre s9 et c2</Intersection>
<Parallel name="par4" n="29" hidden="super" large="true" point="P4" line="s10">Parallèle passant par P4 à s10</Parallel>
<Intersection name="P6" n="30" hidden="super" large="true" first="s10" second="c3" shape="circle" which="first">Intersection entre s10 et c3</Intersection>
<Intersection name="P7" n="31" hidden="super" large="true" first="s11" second="c4" shape="circle" which="second">Intersection entre s11 et c4</Intersection>
<Intersection name="P8" n="32" hidden="super" large="true" first="s12" second="c4" shape="circle" which="first">Intersection entre s12 et c4</Intersection>
<Intersection name="P9" n="33" hidden="super" large="true" first="par1" second="par2" shape="circle">Intersection entre par1 et par2</Intersection>
<Parallel name="par5" n="34" hidden="super" large="true" point="P5" line="s3">Parallèle passant par P5 à s3</Parallel>
<Parallel name="par6" n="35" hidden="super" large="true" point="P6" line="s6">Parallèle passant par P6 à s6</Parallel>
<Parallel name="par7" n="36" hidden="super" large="true" point="P7" line="s12">Parallèle passant par P7 à s12</Parallel>
<Parallel name="par8" n="37" hidden="super" large="true" point="P8" line="s11">Parallèle passant par P8 à s11</Parallel>
<Segment name="s13" n="38" color="1" target="true" from="P3" to="P9">Segment de P3 à P9</Segment>
<Segment name="s14" n="39" color="1" target="true" from="P9" to="P13">Segment de P9 à P13</Segment>
<Intersection name="P10" n="40" hidden="super" large="true" first="par5" second="par3" shape="circle">Intersection entre par5 et par3</Intersection>
<Intersection name="P11" n="41" hidden="super" large="true" first="par6" second="par4" shape="circle">Intersection entre par6 et par4</Intersection>
<Intersection name="P12" n="42" hidden="super" large="true" first="par8" second="par7" shape="circle">Intersection entre par8 et par7</Intersection>
<Segment name="s15" n="43" color="1" target="true" from="P1" to="P10">Segment de P1 à P10</Segment>
<Segment name="s16" n="44" color="1" target="true" from="P10" to="P5">Segment de P10 à P5</Segment>
<Segment name="s17" n="45" color="1" target="true" from="P4" to="P11">Segment de P4 à P11</Segment>
<Segment name="s18" n="46" color="1" target="true" from="P11" to="P6">Segment de P11 à P6</Segment>
<Segment name="s19" n="47" color="1" target="true" from="P7" to="P12">Segment de P7 à P12</Segment>
<Segment name="s20" n="48" color="1" target="true" from="P12" to="P8">Segment de P12 à P8</Segment>
</Objects>
</Macro>
<Macro Name="Polygones/Quadrilatères/Carré" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-3.09470304975923" y="0.21829855537720722">Point à -3.0947, 0.2183</Point>
<Point name="B" n="1" mainparameter="true" x="2.426966292134832" y="-0.3980738362760841">Point à 2.42697, -0.39807</Point>
<Segment name="s1" n="2" color="1" target="true" from="A" to="B">Segment de A à B</Segment>
<Circle name="c1" n="3" color="4" hidden="super" through="B" midpoint="A" acute="true">Cercle de centre A passant par B</Circle>
<Segment name="s2" n="4" hidden="super" large="true" from="A" to="B">Segment de A à B</Segment>
<Circle name="c2" n="5" hidden="super" large="true" fixed="windoww/24" midpoint="A" acute="true">Cercle de centre A de rayon windoww/24</Circle>
<Segment name="s3" n="6" hidden="super" large="true" from="A" to="B">Segment de A à B</Segment>
<Circle name="c3" n="7" hidden="super" large="true" fixed="windoww/24" midpoint="B" acute="true">Cercle de centre B de rayon windoww/24</Circle>
<Plumb name="perp1" n="8" color="3" hidden="super" point="A" line="s1" valid="true">Perpendiculaire passant par A à s1</Plumb>
<Intersection name="P13" n="9" hidden="super" large="true" first="s2" second="c2" shape="circle" which="first">Intersection entre s2 et c2</Intersection>
<Intersection name="P1" n="10" hidden="super" large="true" first="s3" second="c3" shape="circle" which="second">Intersection entre s3 et c3</Intersection>
<Intersection name="E" n="11" color="2" showname="true" xcoffset="-0.38523274478330727" ycoffset="0.10272873194221432" keepclose="true" target="true" first="perp1" second="c1" shape="circle" which="first">Intersection entre perp1 et c1</Intersection>
<Point name="P2" n="12" color="2" showname="true" target="true" x="x(E)+x(B)-x(A)" actx="3.0433386837881233" y="y(E)+y(B)-y(A)" acty="5.1235955056179785" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Segment name="s4" n="13" color="1" target="true" from="A" to="E">Segment de A à E</Segment>
<Segment name="s5" n="14" hidden="super" large="true" from="E" to="A">Segment de E à A</Segment>
<Segment name="s6" n="15" hidden="super" large="true" from="E" to="A">Segment de E à A</Segment>
<Circle name="c4" n="16" hidden="super" large="true" fixed="windoww/24" midpoint="E" acute="true">Cercle de centre E de rayon windoww/24</Circle>
<Segment name="s7" n="17" color="1" target="true" from="E" to="P2">Segment de E à P2</Segment>
<Segment name="s8" n="18" color="1" target="true" from="P2" to="B">Segment de P2 à B</Segment>
<Intersection name="P3" n="19" hidden="super" large="true" first="s5" second="c2" shape="circle" which="second">Intersection entre s5 et c2</Intersection>
<Parallel name="par1" n="20" hidden="super" large="true" point="P13" line="s5">Parallèle passant par P13 à s5</Parallel>
<Segment name="s9" n="21" hidden="super" large="true" from="B" to="P2">Segment de B à P2</Segment>
<Segment name="s10" n="22" hidden="super" large="true" from="B" to="P2">Segment de B à P2</Segment>
<Segment name="s11" n="23" hidden="super" large="true" from="P2" to="E">Segment de P2 à E</Segment>
<Circle name="c5" n="24" hidden="super" large="true" fixed="windoww/24" midpoint="P2" acute="true">Cercle de centre P2 de rayon windoww/24</Circle>
<Segment name="s12" n="25" hidden="super" large="true" from="P2" to="E">Segment de P2 à E</Segment>
<Intersection name="P4" n="26" hidden="super" large="true" first="s6" second="c4" shape="circle" which="first">Intersection entre s6 et c4</Intersection>
<Parallel name="par2" n="27" hidden="super" large="true" point="P3" line="s2">Parallèle passant par P3 à s2</Parallel>
<Parallel name="par3" n="28" hidden="super" large="true" point="P1" line="s9">Parallèle passant par P1 à s9</Parallel>
<Intersection name="P5" n="29" hidden="super" large="true" first="s9" second="c3" shape="circle" which="first">Intersection entre s9 et c3</Intersection>
<Intersection name="P6" n="30" hidden="super" large="true" first="s10" second="c5" shape="circle" which="second">Intersection entre s10 et c5</Intersection>
<Intersection name="P7" n="31" hidden="super" large="true" first="s11" second="c5" shape="circle" which="first">Intersection entre s11 et c5</Intersection>
<Intersection name="P8" n="32" hidden="super" large="true" first="s12" second="c4" shape="circle" which="second">Intersection entre s12 et c4</Intersection>
<Parallel name="par4" n="33" hidden="super" large="true" point="P4" line="s12">Parallèle passant par P4 à s12</Parallel>
<Intersection name="P9" n="34" hidden="super" large="true" first="par1" second="par2" shape="circle">Intersection entre par1 et par2</Intersection>
<Parallel name="par5" n="35" hidden="super" large="true" point="P5" line="s3">Parallèle passant par P5 à s3</Parallel>
<Parallel name="par6" n="36" hidden="super" large="true" point="P6" line="s11">Parallèle passant par P6 à s11</Parallel>
<Parallel name="par7" n="37" hidden="super" large="true" point="P7" line="s10">Parallèle passant par P7 à s10</Parallel>
<Parallel name="par8" n="38" hidden="super" large="true" point="P8" line="s6">Parallèle passant par P8 à s6</Parallel>
<Segment name="s13" n="39" color="1" target="true" from="P3" to="P9">Segment de P3 à P9</Segment>
<Segment name="s14" n="40" color="1" target="true" from="P9" to="P13">Segment de P9 à P13</Segment>
<Intersection name="P10" n="41" hidden="super" large="true" first="par5" second="par3" shape="circle">Intersection entre par5 et par3</Intersection>
<Intersection name="P11" n="42" hidden="super" large="true" first="par7" second="par6" shape="circle">Intersection entre par7 et par6</Intersection>
<Intersection name="P12" n="43" hidden="super" large="true" first="par4" second="par8" shape="circle">Intersection entre par4 et par8</Intersection>
<Segment name="s15" n="44" color="1" target="true" from="P1" to="P10">Segment de P1 à P10</Segment>
<Segment name="s16" n="45" color="1" target="true" from="P10" to="P5">Segment de P10 à P5</Segment>
<Segment name="s17" n="46" color="1" target="true" from="P6" to="P11">Segment de P6 à P11</Segment>
<Segment name="s18" n="47" color="1" target="true" from="P11" to="P7">Segment de P11 à P7</Segment>
<Segment name="s19" n="48" color="1" target="true" from="P8" to="P12">Segment de P8 à P12</Segment>
<Segment name="s20" n="49" color="1" target="true" from="P12" to="P4">Segment de P12 à P4</Segment>
</Objects>
</Macro>
<Macro Name="Alignement/report de mes. algébrique" showduplicates="true">
<Parameter name="O">origine ?</Parameter>
<Parameter name="I">point d&apos;abscisse 1 ?</Parameter>
<Parameter name="k">nombre à reporter ?</Parameter>
<Objects>
<Point name="O" n="0" mainparameter="true" x="-3.7499085670158823" y="0.6770668246000908">Point à -3.74991, 0.67707</Point>
<Point name="I" n="1" mainparameter="true" x="-1.0069198929950058" y="0.7465095758411254">Point à -1.00692, 0.74651</Point>
<Expression name="k" n="2" color="1" type="thick" showname="true" mainparameter="true" x="0.0" y="0.0" value="3" prompt="Valeur">Expression &quot;3&quot; à 0.0, 0.0</Expression>
<Point name="P3" n="3" color="2" target="true" x="x(O)+k*(x(I)-x(O))" actx="4.479057455046746" y="y(O)+k*(y(I)-y(O))" acty="0.8853950783231946" shape="circle" fixed="true">Point à &quot;x(O)+k*(x(I)-x(O))&quot;, &quot;y(O)+k*(y(I)-y(O))&quot;</Point>
</Objects>
</Macro>
<Macro Name="Alignement/report de mes. alg. (avec dialogue)" showduplicates="true">
<Parameter name="O">origine ?</Parameter>
<Parameter name="I">point d&apos;abscisse 1 ?</Parameter>
<Objects>
<Point name="O" n="0" mainparameter="true" x="-3.749908567015882" y="0.6770668246000908">Point à -3.74991, 0.67707</Point>
<Point name="I" n="1" mainparameter="true" x="-1.0069198929950058" y="0.7465095758411254">Point à -1.00692, 0.74651</Point>
<Expression name="k" n="2" color="1" type="thin" hidden="true" showname="true" large="true" x="0.0" y="0.0" value="3" prompt="Valeur">Expression &quot;3&quot; à 0.0, 0.0</Expression>
<Point name="P3" n="3" color="2" target="true" x="x(O)+k*(x(I)-x(O))" actx="4.479057455046747" y="y(O)+k*(y(I)-y(O))" acty="0.8853950783231946" shape="circle" fixed="true">Point à &quot;x(O)+k*(x(I)-x(O))&quot;, &quot;y(O)+k*(y(I)-y(O))&quot;</Point>
</Objects>
<PromptFor object0="k" prompt0="nombre à reporter ?"/>
</Macro>
<Macro Name="Alignement/coeff. d&apos;alignement" showduplicates="true">
<Parameter name="a">Premier point ?</Parameter>
<Parameter name="b">Deuxième point ?</Parameter>
<Parameter name="c">Point aligné avec les deux autres ?</Parameter>
<Parameter name="P4">Point d&apos;ancrage ?</Parameter>
<Objects>
<Point name="a" n="0" mainparameter="true" x="-2.0" y="1.0">Point à -2.0, 1.0</Point>
<Point name="b" n="1" mainparameter="true" x="-1.0" y="1.0">Point à -1.0, 1.0</Point>
<Point name="c" n="2" mainparameter="true" x="2.0" y="1.5">Point à 2.0, 1.5</Point>
<Point name="P4" n="3" color="1" showname="true" xcoffset="-0.5729927007299267" ycoffset="0.31751824817518326" keepclose="true" mainparameter="true" target="true" x="(windoww/(windoww-d(windoww)))*(x(P4)-windowcx)+windowcx+d(windowcx)" actx="-7.063035370471964" y="(windoww/(windoww-d(windoww)))*(y(P4)-windowcy)+windowcy+d(windowcy)" acty="3.4993627171431103" shape="circle" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(P4)-windowcx)+windowcx+d(windowcx)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(P4)-windowcy)+windowcy+d(windowcy)&quot;</Point>
<Expression name="E1" n="4" color="1" type="thin" showname="true" showvalue="true" large="true" ctag0="black" cexpr0="1" x="if((x(c)-x(a))*(y(b)-y(a))~=(y(c)-y(a))*(x(b)-x(a)),x(P4)+15/pixel,invalid)" y="y(P4)-2/pixel" value="if(x(b)~=x(a),(y(c)-y(a))/(y(b)-y(a)),if((x(c)-x(a))*(y(b)-y(a))~=(y(c)-y(a))*(x(b)-x(a)),(x(c)-x(a))/(x(b)-x(a)),invalid))" prompt="coeff. d&apos;alignement " fixed="true">Expression &quot;if(x(b)~=x(a),(y(c)-y(a))/(y(b)-y(a)),if((x(c)-x(a))*(y(b)-y(a))~=(y(c)-y(a))*(x(b)-x(a)),(x(c)-x(a))/(x(b)-x(a)),invalid))&quot; à 0.0, 0.0</Expression>
<Text name="Text1" n="5" color="1" target="true" ctag0="black" cexpr0="1" x="if(!((x(c)-x(a))*(y(b)-y(a))~=(y(c)-y(a))*(x(b)-x(a))),x(P4)+15/pixel,invalid)" y="y(P4)+8/pixel" fixed="true">Les points ne sont pas alignés</Text>
</Objects>
</Macro>
<Macro Name="Transformations/Inverse d&apos;un point" showduplicates="true">
<Parameter name="c">Par rapport à quel cercle ?</Parameter>
<Parameter name="M">inverse de quel point ?</Parameter>
<Comment>
<P>Image d&apos;un point M par l&apos;inversion de cercle c.   </P>
</Comment>
<Objects>
<Point name="P1" n="0" parameter="true" x="-0.23529411764705893" y="0.38914027149321306">Point à -0.23529, 0.38914 </Point>
<Circle name="c" n="1" mainparameter="true" midpoint="P1">???</Circle>
<Point name="M" n="2" mainparameter="true" x="1.7375565610859733" y="0.5882352941176474">Point à 1.73756, 0.58824 </Point>
<Point name="P3" n="3" showname="true" target="true" bold="true" x="x(P1)+c^2/d(P1,M)^2*(x(M)-x(P1))" actx="3.189027388354114" y="y(P1)+c^2/d(P1,M)^2*(y(M)-y(P1))" acty="0.7347140014566338" fixed="true">Point à &quot;x(P1)+c1^2/d(P1,A)^2*(x(A)-x(P1))&quot;, &quot;y(P1)+c1^2/d(P1,A)^2*(y(A)-y(P1))&quot; </Point>
</Objects>
</Macro>
<Macro Name="Transformations/Rotation avec angle" showduplicates="true">
<Parameter name="O">Centre de la rotation</Parameter>
<Parameter name="M">Image du point ?</Parameter>
<Comment>
<P>Image d&apos;un point M par une rotation de centre O et d&apos;angle
a donnés.   </P>
</Comment>
<Objects>
<Expression name="a" n="0" color="1" type="thick" hidden="true" showname="true" x="0.0" y="0.0" value="45" prompt="Valeur">Expression &quot;&quot; à 0.0, 0.0 </Expression>
<Point name="M" n="1" mainparameter="true" x="2.911564625850341" y="-0.013605442176871213">Point à 2.91156, -0.01361 </Point>
<Point name="O" n="2" mainparameter="true" x="0.13605442176870677" y="-0.6938775510204088">Point à 0.13605, -0.69388 </Point>
<Angle name="a1" n="3" color="3" hidden="super" unit="°" large="true" first="M" root="O" fixed="a">Angle P1 - P2 de mesure 25.629060000000003 </Angle>
<Circle name="c1" n="4" color="3" hidden="super" large="true" through="M" midpoint="O">Cercle de centre P2 passant par P1 </Circle>
<Intersection name="I1" n="5" color="3" target="true" large="true" first="a1" second="c1" shape="circle" which="first">Intersection entre a1 et c1 </Intersection>
</Objects>
<PromptFor object0="a" prompt0="Angle de rotation :"/>
</Macro>
<Macro Name="Transformations/Rotation">
<Parameter name="A">A</Parameter>
<Parameter name="O">O</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="O&apos;">O&apos;</Parameter>
<Parameter name="M">M</Parameter>
<Comment>
<P>Image de M par la rotation de centre O&apos; et d&apos;angle AOB   </P>
</Comment>
<Objects>
<Point name="A" n="0" mainparameter="true" x="2.852610000000001" y="-3.271">Point à 2.85261, -3.271 </Point>
<Point name="O" n="1" mainparameter="true" x="-3.689381933438986" y="-3.3217115689381935">Point à -3.68938, -3.32171 </Point>
<Point name="B" n="2" mainparameter="true" x="2.3454800000000002" y="-0.9128400000000001">Point à 2.34548, -0.91284 </Point>
<Point name="O&apos;" n="3" mainparameter="true" x="-3.942947702060222" y="0.6085578446909674">Point à -3.94295, 0.60856 </Point>
<Point name="M" n="4" mainparameter="true" x="1.2044400000000006" y="2.71315">Point à 1.20444, 2.71315 </Point>
<Angle name="w1" n="5" color="1" hidden="super" first="M" root="O&apos;" fixed="a(A,O,B)">Angle P5 - P4 de mesure a(P1,P2,P3) </Angle>
<Circle name="k1" n="6" color="1" hidden="super" through="M" midpoint="O&apos;" acute="true">Circle P4 through P5</Circle>
<Intersection name="S1" n="7" color="1" target="true" first="w1" second="k1" which="first">Intersection w1 and k1</Intersection>
</Objects>
</Macro>
<Macro Name="Transformations/Homothétie avec dialogue" showduplicates="true">
<Parameter name="O">centre ?</Parameter>
<Parameter name="M">point à transformer ?</Parameter>
<Objects>
<Point name="O" n="0" parameter="true" mainparameter="true" x="-2.9569620253164555" y="-1.5696202531645573">Point à -2.95696, -1.56962</Point>
<Point name="M" n="1" parameter="true" mainparameter="true" x="-0.8911392405063294" y="-0.7189873417721522">Point à -0.89114, -0.71899</Point>
<Expression name="k" n="2" color="1" type="thick" hidden="true" showname="true" x="0.0" y="0.0" value="1.5" prompt="Valeur">Expression &quot;1.5&quot; à 0.0, 0.0</Expression>
<Point name="P1" n="3" color="1" showname="true" target="true" x="x(O)+k*(x(M)-x(O))" actx="0.1417721518987336" y="y(O)+k*(y(M)-y(O))" acty="-0.2936708860759496" shape="circle" fixed="true">Point à &quot;x(O)+k*(x(M)-x(O))&quot;, &quot;y(O)+k*(y(M)-y(O))&quot;</Point>
</Objects>
<PromptFor object0="k" prompt0="rapport ?"/>
</Macro>
<Macro Name="Transformations/Homothétie sans dialogue" showduplicates="true">
<Parameter name="k">rapport ?</Parameter>
<Parameter name="O">centre ?</Parameter>
<Parameter name="M">point à transformer ?</Parameter>
<Objects>
<Point name="O" n="0" mainparameter="true" x="-2.9569620253164555" y="-1.5696202531645573">Point à -2.95696, -1.56962</Point>
<Point name="M" n="1" mainparameter="true" x="-0.8911392405063294" y="-0.7189873417721522">Point à -0.89114, -0.71899</Point>
<Expression name="k" n="2" color="1" type="thick" showname="true" mainparameter="true" x="0.0" y="0.0" value="1.5" prompt="Valeur">Expression &quot;1.5&quot; à 0.0, 0.0</Expression>
<Point name="P1" n="3" color="1" showname="true" target="true" x="x(O)+k*(x(M)-x(O))" actx="0.1417721518987336" y="y(O)+k*(y(M)-y(O))" acty="-0.2936708860759496" shape="circle" fixed="true">Point à &quot;x(O)+k*(x(M)-x(O))&quot;, &quot;y(O)+k*(y(M)-y(O))&quot;</Point>
</Objects>
</Macro>
<Macro Name="Vecteurs/Vecteur" showduplicates="true">
<Parameter name="P1">P1</Parameter>
<Parameter name="P2">P2</Parameter>
<Parameter name="P3">P3</Parameter>
<Objects>
<Point name="P1" n="0" mainparameter="true" x="-2.8354430379746853" y="1.286075949367089">Point à -2.83544, 1.28608</Point>
<Point name="P2" n="1" mainparameter="true" x="0.263291139240506" y="2.622784810126582">Point à 0.26329, 2.62278</Point>
<Point name="P3" n="2" mainparameter="true" x="0.5670886075949365" y="0.7189873417721526">Point à 0.56709, 0.71899</Point>
<Point name="P4" n="3" color="2" target="true" bold="true" x="x(P3)+x(P2)-x(P1)" actx="3.665822784810128" y="y(P3)+y(P2)-y(P1)" acty="2.055696202531646" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Segment name="s1" n="4" color="2" target="true" from="P3" to="P4" arrow="true">Segment de P3 à P4</Segment>
</Objects>
</Macro>
<Macro Name="Vecteurs/Vect. défini par coordonnées" showduplicates="true">
<Parameter name="P7">Origine du vecteur</Parameter>
<Objects>
<Point name="P7" n="0" mainparameter="true" x="-1.6842105263157905" y="-0.6315789473684212">Point à -1.68421, -0.63158</Point>
<Expression name="E1" n="1" color="2" type="thin" hidden="true" showname="true" x="0.0" y="0.0" value="1" prompt="Valeur">Expression &quot;1&quot; à 0.0, 0.0</Expression>
<Expression name="E2" n="2" color="2" type="thin" hidden="true" showname="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0</Expression>
<Point name="P2" n="3" color="2" target="true" x="x(P7)+E1" actx="-0.6842105263157905" y="y(P7)+E2" acty="-0.6315789473684212" shape="circle" fixed="true">Point à &quot;x(P7)+E1&quot;, &quot;y(P7)+E2&quot;</Point>
<Segment name="s1" n="4" color="2" target="true" from="P7" to="P2" arrow="true">Segment de P7 à P2</Segment>
</Objects>
<PromptFor object0="E1" prompt0="Abscisse du vecteur" object1="E2" prompt1="Ordonnée du vecteur"/>
</Macro>
<Macro Name="Vecteurs/Vect. mult. par un réel (dlog)" showduplicates="true">
<Parameter name="s3">Vecteur</Parameter>
<Parameter name="E">Origine</Parameter>
<Objects>
<Point name="P1" n="0" parameter="true" x="0.701754385964913" y="1.3894736842105266">Point à 0.70175, 1.38947</Point>
<Point name="E" n="1" mainparameter="true" x="-0.7578947368421057" y="-2.624561403508772">Point à -0.75789, -2.62456</Point>
<Point name="P3" n="2" parameter="true" x="2.701754385964913" y="2.389473684210526">Point à 2.70175, 2.38947</Point>
<Expression name="E1" n="3" color="2" type="thin" hidden="true" showname="true" x="0.0" y="0.0" value="1" prompt="Valeur">Expression &quot;1&quot; à 0.0, 0.0</Expression>
<Segment name="s3" n="4" color="2" mainparameter="true" from="P1" to="P3" arrow="true">Segment de P1 à P3</Segment>
<Point name="P4" n="5" color="2" showname="true" target="true" x="x(E)+E1*(x(P3)-x(P1))" actx="1.2421052631578942" y="y(E)+E1*(y(P3)-y(P1))" acty="-1.6245614035087725" shape="circle" fixed="true">Point à &quot;x(E)+E1*(x(P3)-x(P1))&quot;, &quot;y(E)+E1*(y(P3)-y(P1))&quot;</Point>
<Segment name="s2" n="6" color="2" target="true" from="E" to="P4" arrow="true">Segment de E à P4</Segment>
</Objects>
<PromptFor object0="E1" prompt0="Coefficient multiplicateur"/>
</Macro>
<Macro Name="Vecteurs/Vect. mult. par un réel" showduplicates="true">
<Parameter name="s1">multiplier quel vecteur ?</Parameter>
<Parameter name="E1">par quel expression ?</Parameter>
<Parameter name="P6">Origine du vecteur résultant ?</Parameter>
<Objects>
<Point name="P1" n="0" parameter="true" x="-0.6736842105263162" y="1.4175438596491234">Point à -0.67368, 1.41754</Point>
<Point name="P2" n="1" parameter="true" x="2.919298245614036" y="3.1859649122807014">Point à 2.9193, 3.18596</Point>
<Point name="P6" n="2" mainparameter="true" x="-3.4245614035087732" y="-1.5578947368421057">Point à -3.42456, -1.55789</Point>
<Segment name="s1" n="3" color="1" mainparameter="true" from="P1" to="P2" arrow="true">Segment de P1 à P2</Segment>
<Expression name="E1" n="4" color="1" type="thick" showname="true" mainparameter="true" x="0.0" y="0.0" value="3" prompt="Valeur">Expression &quot;3&quot; à 0.0, 0.0</Expression>
<Expression name="E2" n="5" color="2" type="thin" hidden="super" showname="true" x="0.0" y="0.0" value="E1" prompt="Valeur">Expression &quot;E1&quot; à 0.0, 0.0</Expression>
<Point name="P4" n="6" color="5" type="thick" target="true" x="x(P6)+E2*(x(P2)-x(P1))" actx="7.354385964912284" y="y(P6)+E2*(y(P2)-y(P1))" acty="3.747368421052628" shape="circle" fixed="true">Point à &quot;x(P6)+E2*(x(P2)-x(P1))&quot;, &quot;y(P6)+E2*(y(P2)-y(P1))&quot;</Point>
<Segment name="s2" n="7" color="1" target="true" from="P6" to="P4" arrow="true">Segment de P6 à P4</Segment>
</Objects>
</Macro>
<Macro Name="Vecteurs/Somme de deux vecteurs" showduplicates="true">
<Parameter name="s4">s4</Parameter>
<Parameter name="s5">s5</Parameter>
<Parameter name="E">E</Parameter>
<Objects>
<Point name="P1" n="0" parameter="true" x="-1.431578947368421" y="3.1298245614035105">Point à -1.43158, 3.12982</Point>
<Point name="P2" n="1" parameter="true" x="0.5333333333333332" y="3.831578947368421">Point à 0.53333, 3.83158</Point>
<Point name="P3" n="2" parameter="true" x="-2.8350877192982455" y="-0.6596491228070178">Point à -2.83509, -0.65965</Point>
<Point name="P4" n="3" parameter="true" x="-0.6175438596491236" y="-2.3157894736842106">Point à -0.61754, -2.31579</Point>
<Point name="E" n="4" mainparameter="true" x="0.28070175438596584" y="1.3894736842105266">Point à 0.2807, 1.38947</Point>
<Point name="P6" n="5" color="2" target="true" x="x(E)+x(P2)-x(P1)+x(P4)-x(P3)" actx="4.463157894736842" y="y(E)+y(P2)-y(P1)+y(P4)-y(P3)" acty="0.4350877192982443" shape="circle" fixed="true">Point à &quot;x(E)+x(P2)-x(P1)+x(P4)-x(P3)&quot;, &quot;y(E)+y(P2)-y(P1)+y(P4)-y(P3)&quot;</Point>
<Segment name="s4" n="6" color="2" mainparameter="true" from="P1" to="P2" arrow="true">Segment de P1 à P2</Segment>
<Segment name="s5" n="7" color="2" mainparameter="true" from="P3" to="P4" arrow="true">Segment de P3 à P4</Segment>
<Segment name="s3" n="8" color="2" target="true" from="E" to="P6" arrow="true">Segment de E à P6</Segment>
</Objects>
</Macro>
<Macro Name="Barycentres/Point pondéré">
<Parameter name="A">Quel point ?</Parameter>
<Comment>
<P>Crée un point pondéré à partir d&apos;une masse demandée par
voix numérique.</P>
</Comment>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-2.696832579185521" y="1.7013574660633481">Point à -2.69683, 1.70136 </Point>
<Expression name="m" n="1" color="1" type="thick" showname="true" showvalue="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(A)+0.2" y="y(A)" value="2" prompt="" fixed="true">Expression &quot;2&quot; à -2.49683, 1.70136 </Expression>
</Objects>
<PromptFor object0="m" prompt0="masse associée :"/>
</Macro>
<Macro Name="Barycentres/Barycentre 2 pts">
<Parameter name="A">Premier point ?</Parameter>
<Parameter name="a">Premier coefficient ?</Parameter>
<Parameter name="C">Deuxième point ?</Parameter>
<Parameter name="c">Deuxième coefficient ?</Parameter>
<Comment>
<P>Désigner deux points pondérés (A,a) et (B,b) pour créer le
barycentre G=Bar{(A,a),(B,b)}</P>
</Comment>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-2.7330316742081453" y="0.9230769230769229">Point à -2.73303, 0.92308 </Point>
<Expression name="a" n="1" color="1" type="thick" showname="true" showvalue="true" mainparameter="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(A)+0.2" y="y(A)" value="2" prompt="" fixed="true">Expression &quot;2&quot; à -2.53303, 0.92308 </Expression>
<Point name="C" n="2" mainparameter="true" x="0.14479638009049722" y="0.16289592760180982">Point à 0.1448, 0.1629 </Point>
<Expression name="c" n="3" color="1" type="thick" showname="true" showvalue="true" mainparameter="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(C)+0.2" y="y(C)" value="-1" prompt="" fixed="true">Expression &quot;-1&quot; à 0.3448, 0.1629 </Expression>
<Point name="P3" n="4" type="thick" target="true" bold="true" large="true" x="x(A)+c/(a+c)*(x(C)-x(A))" actx="-5.610859728506788" y="y(A)+c/(a+c)*(y(C)-y(A))" acty="1.683257918552036" shape="circle" fixed="true">Point à &quot;x(A)+c/(a+c)*(x(C)-x(A))&quot;, &quot;y(A)+c/(a+c)*(y(C)-y(A))&quot; </Point>
</Objects>
</Macro>
<Macro Name="Barycentres/Barycentre 2 pts affecté de la somme des masses">
<Parameter name="A">Premier point ?</Parameter>
<Parameter name="a">Premier coefficient ?</Parameter>
<Parameter name="B">Deuxième point ?</Parameter>
<Parameter name="b">Deuxième coefficient ?</Parameter>
<Comment>
<P>Désigner deux points pondérés (A,a) et (B,b) pour créer le
barycentre G=Bar{(A,a),(B,b)}. La masse a+b sera associée
au point G.</P>
</Comment>
<Objects>
<Point name="A" n="0" parameter="true" mainparameter="true" x="-0.8118811881188119" y="2.1287128712871293">Point à -0.81188, 2.12871 </Point>
<Expression name="a" n="1" color="1" type="thick" showname="true" showvalue="true" parameter="true" mainparameter="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(A)+0.2" y="y(A)" value="2" prompt="" fixed="true">Expression &quot;2&quot; à -0.61188, 2.12871 </Expression>
<Point name="B" n="2" parameter="true" mainparameter="true" x="-2.455445544554457" y="-1.0594059405940597">Point à -2.45545, -1.05941 </Point>
<Expression name="b" n="3" color="1" type="thick" showname="true" showvalue="true" parameter="true" mainparameter="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(B)+0.2" y="y(B)" value="3" prompt="" fixed="true">Expression &quot;3&quot; à -2.25545, -1.05941 </Expression>
<Point name="P3" n="4" type="thick" showname="true" target="true" bold="true" large="true" ctag0="brown" cexpr0="1" ctag1="thick" cexpr1="1" x="x(A)+b/(a+b)*(x(B)-x(A))" actx="-1.7980198019801992" y="y(A)+b/(a+b)*(y(B)-y(A))" acty="0.2158415841584158" shape="circle" fixed="true">Point à &quot;x(A)+b/(a+b)*(x(B)-x(A))&quot;, &quot;y(A)+b/(a+b)*(y(B)-y(A))&quot; </Point>
<Expression name="E3" n="5" color="1" type="thick" showname="true" showvalue="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(P3)+0.2" y="y(P3)" value="a+b" prompt="Valeur" fixed="true">Expression &quot;a+b&quot; à -1.59802, 0.21584 </Expression>
</Objects>
</Macro>
<Macro Name="Barycentres/Barycentre 3 pts">
<Parameter name="A">Premier point ?</Parameter>
<Parameter name="a">Premier coefficient ?</Parameter>
<Parameter name="B">Deuxième point ?</Parameter>
<Parameter name="b">Deuxième coefficient ?</Parameter>
<Parameter name="C">Troisième point ?</Parameter>
<Parameter name="c">Troisième coefficient ?</Parameter>
<Comment>
<P>Désigner trois points pondérés (A,a) , (B,b) et (C,c) pour
créer le barycentre G=Bar{(A,a),(B,b),(C,c)}</P>
</Comment>
<Objects>
<Point name="A" n="0" parameter="true" mainparameter="true" x="-1.3465346534653468" y="3.514851485148516">Point à -1.34653, 3.51485 </Point>
<Expression name="a" n="1" color="1" type="thick" showname="true" showvalue="true" parameter="true" mainparameter="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(A)+0.2" y="y(A)" value="2" prompt="" fixed="true">Expression &quot;2&quot; à -1.14653, 3.51485 </Expression>
<Point name="B" n="2" parameter="true" mainparameter="true" x="-2.7326732673267333" y="1.3762376237623757">Point à -2.73267, 1.37624 </Point>
<Expression name="b" n="3" color="1" type="thick" showname="true" showvalue="true" parameter="true" mainparameter="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(B)+0.2" y="y(B)" value="3" prompt="" fixed="true">Expression &quot;3&quot; à -2.53267, 1.37624 </Expression>
<Point name="C" n="4" parameter="true" mainparameter="true" x="1.663366336633663" y="1.435643564356436">Point à 1.66337, 1.43564 </Point>
<Expression name="c" n="5" color="1" type="thick" showname="true" showvalue="true" parameter="true" mainparameter="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(C)+0.2" y="y(C)" value="5" prompt="" fixed="true">Expression &quot;5&quot; à 1.86337, 1.43564 </Expression>
<Point name="P4" n="6" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="brown" cexpr0="1" ctag1="thick" cexpr1="1" x="x(A)+b/(a+b)*(x(B)-x(A))" actx="-2.178217821782179" y="y(A)+b/(a+b)*(y(B)-y(A))" acty="2.231683168316832" shape="circle" fixed="true">Point à &quot;x(P1)+E2/(E1+E2)*(x(P2)-x(P1))&quot;, &quot;y(P1)+E2/(E1+E2)*(y(P2)-y(P1))&quot; </Point>
<Expression name="E4" n="7" color="1" type="thick" hidden="super" showname="true" showvalue="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(P4)+0.2" y="y(P4)" value="a+b" prompt="Valeur" fixed="true">Expression &quot;E1+E2&quot; à -1.97822, 2.23168 </Expression>
<Point name="P5" n="8" color="2" type="thick" showname="true" target="true" bold="true" large="true" ctag0="brown" cexpr0="1" ctag1="thick" cexpr1="1" x="x(P4)+c/(E4+c)*(x(C)-x(P4))" actx="-0.2574257425742579" y="y(P4)+c/(E4+c)*(y(C)-y(P4))" acty="1.833663366336634" shape="circle" fixed="true">Point à &quot;x(P4)+E3/(E4+E3)*(x(P3)-x(P4))&quot;, &quot;y(P4)+E3/(E4+E3)*(y(P3)-y(P4))&quot; </Point>
</Objects>
</Macro>
<Macro Name="Barycentres/Barycentre 3 pts affecté de la somme des masses">
<Parameter name="A">Premier point ?</Parameter>
<Parameter name="a">Premier coefficient ?</Parameter>
<Parameter name="B">Deuxième point ?</Parameter>
<Parameter name="b">Deuxième coefficient ?</Parameter>
<Parameter name="C">Troisième point ?</Parameter>
<Parameter name="c">Troisième coefficient ?</Parameter>
<Comment>
<P>Désigner trois points pondérés (A,a) , (B,b) et (C,c) pour
créer le barycentre G=Bar{(A,a),(B,b),(C,c)}. La masse
a+b+c sera associée au point G.</P>
</Comment>
<Objects>
<Point name="A" n="0" parameter="true" mainparameter="true" x="-1.3465346534653468" y="3.514851485148516">Point à -1.34653, 3.51485 </Point>
<Expression name="a" n="1" color="1" type="thick" showname="true" showvalue="true" parameter="true" mainparameter="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(A)+0.2" y="y(A)" value="2" prompt="" fixed="true">Expression &quot;2&quot; à -1.14653, 3.51485 </Expression>
<Point name="B" n="2" parameter="true" mainparameter="true" x="-2.7326732673267333" y="1.3762376237623757">Point à -2.73267, 1.37624 </Point>
<Expression name="b" n="3" color="1" type="thick" showname="true" showvalue="true" parameter="true" mainparameter="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(B)+0.2" y="y(B)" value="3" prompt="" fixed="true">Expression &quot;3&quot; à -2.53267, 1.37624 </Expression>
<Point name="C" n="4" parameter="true" mainparameter="true" x="1.663366336633663" y="1.435643564356436">Point à 1.66337, 1.43564 </Point>
<Expression name="c" n="5" color="1" type="thick" showname="true" showvalue="true" parameter="true" mainparameter="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(C)+0.2" y="y(C)" value="5" prompt="" fixed="true">Expression &quot;5&quot; à 1.86337, 1.43564 </Expression>
<Point name="P4" n="6" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="brown" cexpr0="1" ctag1="thick" cexpr1="1" x="x(A)+b/(a+b)*(x(B)-x(A))" actx="-2.178217821782179" y="y(A)+b/(a+b)*(y(B)-y(A))" acty="2.231683168316832" shape="circle" fixed="true">Point à &quot;x(P1)+E2/(E1+E2)*(x(P2)-x(P1))&quot;, &quot;y(P1)+E2/(E1+E2)*(y(P2)-y(P1))&quot; </Point>
<Expression name="E4" n="7" color="1" type="thick" hidden="super" showname="true" showvalue="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(P4)+0.2" y="y(P4)" value="a+b" prompt="Valeur" fixed="true">Expression &quot;E1+E2&quot; à -1.97822, 2.23168 </Expression>
<Point name="P5" n="8" color="2" type="thick" showname="true" target="true" bold="true" large="true" ctag0="brown" cexpr0="1" ctag1="thick" cexpr1="1" x="x(P4)+c/(E4+c)*(x(C)-x(P4))" actx="-0.2574257425742579" y="y(P4)+c/(E4+c)*(y(C)-y(P4))" acty="1.833663366336634" shape="circle" fixed="true">Point à &quot;x(P4)+E3/(E4+E3)*(x(P3)-x(P4))&quot;, &quot;y(P4)+E3/(E4+E3)*(y(P3)-y(P4))&quot; </Point>
<Expression name="E5" n="9" color="1" type="thick" showname="true" showvalue="true" bold="true" large="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" x="x(P5)+0.2" y="y(P5)" value="E4+c" prompt="Valeur" fixed="true">Expression &quot;E4+E3&quot; à -0.05743, 1.83366 </Expression>
</Objects>
</Macro>
<Macro Name="Barycentres/Report barycentrique 2 points">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="C">C</Parameter>
<Parameter name="D">D</Parameter>
<Parameter name="G">G</Parameter>
<Comment>
<P>On désigne 5 points A, B, C, D, et G, où A, B et G sont
trois points alignés.</P>
<P>La macro crée le point G&apos; qui sera à C et D ce que G est à
A et B (d&apos;un point de vue barycentrique).</P>
</Comment>
<Objects>
<Point name="A" n="0" parameter="true" mainparameter="true" x="-5.194570135746607" y="2.108597285067875">Point à -5.19457, 2.1086 </Point>
<Point name="B" n="1" parameter="true" mainparameter="true" x="-0.8506787330316747" y="2.108597285067875">Point à -0.85068, 2.1086 </Point>
<Point name="C" n="2" parameter="true" mainparameter="true" x="-5.375565610859733" y="-0.09954751131221773">Point à -5.37557, -0.09955 </Point>
<Point name="D" n="3" parameter="true" mainparameter="true" x="-1.0135746606334841" y="0.3529411764705888">Point à -1.01357, 0.35294 </Point>
<Point name="G" n="4" parameter="true" mainparameter="true" x="0.9230769230769237" y="2.108597285067875">Point à 0.92308, 2.1086 </Point>
<Expression name="E1" n="5" color="5" type="thick" hidden="super" showname="true" showvalue="true" bold="true" large="true" x="-3.113120000000001" y="4.15385" value="sign(angle180(a(G,A,B)+1))*d(A,G)/d(A,B)" prompt="k=sign(angle180(a(M,A,B)+1))*d(A,M)/d(A,B)">Expression &quot;sign(angle180(a(M,A,B)+1))*d(A,M)/d(A,B)&quot; à -3.11312, 4.15385 </Expression>
<Point name="P6" n="6" color="1" type="thick" showname="true" target="true" bold="true" large="true" x="x(C)+E1*(x(D)-x(C))" actx="0.7675716440422349" y="y(C)+E1*(y(D)-y(C))" acty="0.5377073906485682" shape="dcross" fixed="true">Point à &quot;x(C)+E1*(x(D)-x(C))&quot;, &quot;y(C)+E1*(y(D)-y(C))&quot; </Point>
</Objects>
</Macro>
<Macro Name="Barycentres/Report barycentrique 3 points">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="C">C</Parameter>
<Parameter name="A&apos;">A&apos;</Parameter>
<Parameter name="B&apos;">B&apos;</Parameter>
<Parameter name="C&apos;">C&apos;</Parameter>
<Parameter name="G">G</Parameter>
<Comment>
<P>On désigne 7 points A, B, C, A&apos;, B&apos;, C&apos;, et G.</P>
<P>La macro crée le point G&apos; qui sera à A&apos;, B&apos;, C&apos; ce que G
est à A, B et C (d&apos;un point de vue barycentrique).</P>
</Comment>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-4.343891402714935" y="4.09954751131222">Point à -4.34389, 4.09955 </Point>
<Point name="B" n="1" mainparameter="true" x="-5.990950226244348" y="1.6018099547511317">Point à -5.99095, 1.60181 </Point>
<Point name="C" n="2" mainparameter="true" x="-2.515837104072398" y="1.6018099547511317">Point à -2.51584, 1.60181 </Point>
<Point name="A&apos;" n="3" mainparameter="true" x="0.07239819004524861" y="0.13574660633484117">Point à 0.0724, 0.13575 </Point>
<Point name="B&apos;" n="4" mainparameter="true" x="-1.447963800904978" y="-2.651583710407241">Point à -1.44796, -2.65158 </Point>
<Point name="C&apos;" n="5" mainparameter="true" x="3.656108597285068" y="-2.4162895927601813">Point à 3.65611, -2.41629 </Point>
<Point name="G" n="6" mainparameter="true" x="-3.1131221719457014" y="2.9230769230769242">Point à -3.11312, 2.92308 </Point>
<Line name="l1" n="7" color="3" hidden="super" bold="true" large="true" from="B" to="C">Droite passant par B et C</Line>
<Line name="l2" n="8" color="3" hidden="super" bold="true" large="true" from="B" to="A">Droite passant par B et A</Line>
<Parallel name="par1" n="9" color="3" type="thin" hidden="super" bold="true" large="true" point="G" line="l1">Parallèle passant par M à l1</Parallel>
<Parallel name="par2" n="10" color="3" type="thin" hidden="super" bold="true" large="true" point="G" line="l2">Parallèle passant par M à l2</Parallel>
<Intersection name="I1" n="11" color="3" hidden="super" showname="true" bold="true" large="true" first="l2" second="par1">Intersection entre l2 et par1</Intersection>
<Intersection name="I2" n="12" color="3" hidden="super" showname="true" bold="true" large="true" first="l1" second="par2">Intersection entre l1 et par2</Intersection>
<Expression name="E1" n="13" color="5" type="thick" hidden="super" showname="true" showvalue="true" bold="true" large="true" x="-3.113120000000001" y="4.15385" value="sign(angle180(a(I1,B,A)+1))*d(B,I1)/d(B,A)" prompt="k=sign(angle180(a(M,A,B)+1))*d(A,M)/d(A,B)">Expression &quot;sign(angle180(a(I1,B,A)+1))*d(B,I1)/d(B,A)&quot; à -3.11312, 4.15385 </Expression>
<Point name="P8" n="14" hidden="super" showname="true" bold="true" large="true" x="x(&quot;B&apos;&quot;)+E1*(x(&quot;A&apos;&quot;)-x(&quot;B&apos;&quot;))" actx="-0.6437143419240616" y="y(&quot;B&apos;&quot;)+E1*(y(&quot;A&apos;&quot;)-y(&quot;B&apos;&quot;))" acty="-1.1771263689422278" shape="dcross" fixed="true">Point à &quot;x(E)+E1*(x(D)-x(E))&quot;, &quot;y(E)+E1*(y(D)-y(E))&quot; </Point>
<Expression name="E2" n="15" color="5" type="thick" hidden="super" showname="true" showvalue="true" bold="true" large="true" x="-3.113120000000001" y="4.15385" value="sign(angle180(a(I2,B,C)+1))*d(B,I2)/d(B,C)" prompt="k=sign(angle180(a(M,A,B)+1))*d(A,M)/d(A,B)">Expression &quot;sign(angle180(a(I2,B,C)+1))*d(B,I2)/d(B,C)&quot; à -3.11312, 4.15385 </Expression>
<Point name="P9" n="16" hidden="super" showname="true" bold="true" large="true" x="x(&quot;B&apos;&quot;)+E2*(x(&quot;C&apos;&quot;)-x(&quot;B&apos;&quot;))" actx="1.4991679782280825" y="y(&quot;B&apos;&quot;)+E2*(y(&quot;C&apos;&quot;)-y(&quot;B&apos;&quot;))" acty="-2.515723025553589" shape="dcross" fixed="true">Point à &quot;x(E)+E2*(x(F)-x(E))&quot;, &quot;y(E)+E2*(y(F)-y(E))&quot; </Point>
<Point name="P10" n="17" color="1" type="thick" showname="true" target="true" bold="true" large="true" x="x(P8)+x(P9)-x(&quot;B&apos;&quot;)" actx="2.303417437208999" y="y(P8)+y(P9)-y(&quot;B&apos;&quot;)" acty="-1.0412656840885757" shape="dcross" fixed="true">Point à &quot;x(P8)+x(P9)-x(E)&quot;, &quot;y(P8)+y(P9)-y(E)&quot; </Point>
</Objects>
</Macro>
<Macro Name="Fonctions/minimum" showduplicates="true">
<Parameter name="f">fonction ?</Parameter>
<Parameter name="P5">point d&apos;ancrage ?</Parameter>
<Objects>
<Function name="f" n="0" color="5" type="thin" mainparameter="true" x="0" y="0" var="x" min="-10" max="10" d="0.1" shape="cross">Fonction ( 0 , 0 )</Function>
<Expression name="a" n="1" color="5" type="thin" hidden="true" showname="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0</Expression>
<Expression name="b" n="2" color="5" type="thin" hidden="true" showname="true" x="0.0" y="0.0" value="3" prompt="Valeur">Expression &quot;3&quot; à 0.0, 0.0</Expression>
<Point name="P5" n="3" color="1" mainparameter="true" target="true" x="(windoww/(windoww-d(windoww)))*(x(P5)-windowcx)+windowcx+d(windowcx)" actx="-6.5" y="(windoww/(windoww-d(windoww)))*(y(P5)-windowcy)+windowcy+d(windowcy)" acty="2.5" shape="dcross" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(P5)-windowcx)+windowcx+d(windowcx)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(P5)-windowcy)+windowcy+d(windowcy)&quot;</Point>
<Expression name="E3" n="4" color="1" type="thin" showname="true" showvalue="true" x="x(P5)+15/pixel" y="y(P5)-5/pixel" value="min(f,a,b)" prompt="&lt;abs du minimum&gt;" fixed="true">Expression &quot;min(f,a,b)&quot; à -6.35, 2.45</Expression>
</Objects>
<PromptFor object0="a" prompt0="extrémité gauche ?" object1="b" prompt1="extrémité droite ?"/>
</Macro>
<Macro Name="Fonctions/maximum" showduplicates="true">
<Parameter name="f">fonction ?</Parameter>
<Parameter name="P5">Point d&apos;ancrage ?</Parameter>
<Objects>
<Function name="f" n="0" color="5" type="thin" mainparameter="true" x="0" y="0" var="x" min="-10" max="10" d="0.1" shape="cross">Fonction ( 0 , 0 )</Function>
<Expression name="a" n="1" color="5" type="thin" hidden="true" showname="true" x="0.0" y="0.0" value="-1" prompt="Valeur">Expression &quot;-1&quot; à 0.0, 0.0</Expression>
<Expression name="b" n="2" color="5" type="thin" hidden="true" showname="true" x="0.0" y="0.0" value="1" prompt="Valeur">Expression &quot;1&quot; à 0.0, 0.0</Expression>
<Point name="P5" n="3" color="1" mainparameter="true" target="true" x="(windoww/(windoww-d(windoww)))*(x(P5)-windowcx)+windowcx+d(windowcx)" actx="-7.551094890510949" y="(windoww/(windoww-d(windoww)))*(y(P5)-windowcy)+windowcy+d(windowcy)" acty="2.733576642335767" shape="dcross" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(P5)-windowcx)+windowcx+d(windowcx)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(P5)-windowcy)+windowcy+d(windowcy)&quot;</Point>
<Expression name="E3" n="4" color="1" type="thin" showname="true" showvalue="true" x="x(P5)+15/pixel" y="y(P5)-5/pixel" value="max(f,a,b)" prompt="&lt;abs du maximum&gt;" fixed="true">Expression &quot;max(f,a,b)&quot; à -7.40109, 2.68358</Expression>
</Objects>
<PromptFor object0="a" prompt0="extrémité gauche ?" object1="b" prompt1="extrémité droite ?"/>
</Macro>
<Macro Name="Fonctions/Tangente" showduplicates="true">
<Parameter name="f1">f1</Parameter>
<Parameter name="A">A</Parameter>
<Objects>
<Function name="f1" n="0" color="1" mainparameter="true" x="0" y="0" var="x" min="-10" max="10" d="0.1" shape="cross">Fonction ( 0 , 0 )</Function>
<Point name="A" n="1" mainparameter="true" x="0.2672172586692527" y="-0.5919659942982682">Point à 0.26722, -0.59197</Point>
<Point name="P3" n="2" showname="true" target="true" x="x(A)" actx="0.2672172586692527" y="f1(x(A))" acty="0.0" shape="circle" fixed="true">Point à &quot;x(A)&quot;, &quot;f1(x(A))&quot;</Point>
<Point name="P1" n="3" hidden="super" x="x(P3)+1" actx="1.2672172586692527" y="y(P3)+diff(f1,x(P3))" acty="0.0" shape="circle" fixed="true">Point à &quot;x(P3)+1&quot;, &quot;y(P3)+diff(f1,x(P3))&quot;</Point>
<Line name="l2" n="4" target="true" from="P1" to="P3">Droite passant par P1 et P3</Line>
</Objects>
</Macro>
<Macro Name="Fonctions/Tangente (avec dialogue)" showduplicates="true">
<Parameter name="f">fonction ?</Parameter>
<Objects>
<Function name="f" n="0" color="5" type="thin" mainparameter="true" x="0" y="0" var="x" min="-10" max="10" d="0.1" shape="cross">Fonction ( 0 , 0 )</Function>
<Expression name="a" n="1" color="5" type="thin" hidden="true" showname="true" x="0.0" y="0.0" value="-1" prompt="Valeur">Expression &quot;-1&quot; à 0.0, 0.0</Expression>
<Point name="P1" n="2" color="1" showname="true" target="true" x="a" actx="-1.0" y="f(a)" acty="0.0" shape="dcross" fixed="true">Point à &quot;a&quot;, &quot;f(a)&quot;</Point>
<Point name="P2" n="3" color="1" hidden="super" showname="true" x="a+1" actx="0.0" y="f(a)+diff(f,a)" acty="0.0" shape="dcross" fixed="true">Point à &quot;a+1&quot;, &quot;f(a)+diff(f,a)&quot;</Point>
<Line name="l2" n="4" color="1" target="true" from="P1" to="P2">Droite passant par P1 et P2</Line>
</Objects>
<PromptFor object0="a" prompt0="abscisse du pt de tangence ?"/>
</Macro>
<Macro Name="Fonctions/nombre dérivé" showduplicates="true">
<Parameter name="f">fonction ?</Parameter>
<Parameter name="P4">Point d&apos;ancrage ?</Parameter>
<Objects>
<Function name="f" n="0" color="1" type="thin" mainparameter="true" x="0" y="0" var="x" min="-10" max="10" d="0.1" shape="cross">Fonction ( 0 , 0 )</Function>
<Expression name="a" n="1" color="1" type="thin" hidden="true" showname="true" x="0.0" y="0.0" value="1.5" prompt="Valeur">Expression &quot;1.5&quot; à 0.0, 0.0</Expression>
<Point name="P4" n="2" color="1" mainparameter="true" target="true" x="(windoww/(windoww-d(windoww)))*(x(P4)-windowcx)+windowcx+d(windowcx)" actx="-10.97146611832793" y="(windoww/(windoww-d(windoww)))*(y(P4)-windowcy)+windowcy+d(windowcy)" acty="3.846167596001895" shape="dcross" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(P4)-windowcx)+windowcx+d(windowcx)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(P4)-windowcy)+windowcy+d(windowcy)&quot;</Point>
<Expression name="E2" n="3" color="1" type="thin" showname="true" showvalue="true" ctag0="showvalue" cexpr0="1" ctag1="showname" cexpr1="1" x="x(P4)+15/pixel" y="y(P4)-5/pixel" value="diff(f,a)" prompt="&lt;nombre dérivé&gt;" fixed="true">Expression &quot;diff(f,a)&quot; à -10.82147, 3.79617</Expression>
</Objects>
<PromptFor object0="a" prompt0="abscisse du pt de tangence ?"/>
</Macro>
<Macro Name="Coordonnées polaires/Pt par coord. (degrés, sans dialogue)" showduplicates="true">
<Parameter name="E2">angle en degrés ?</Parameter>
<Parameter name="E1">rayon ?</Parameter>
<Objects>
<Expression name="E1" n="0" color="1" type="thick" showname="true" mainparameter="true" x="0.0" y="0.0" value="3" prompt="Valeur">Expression &quot;3&quot; à 0.0, 0.0</Expression>
<Expression name="E2" n="1" color="1" type="thick" showname="true" mainparameter="true" x="0.0" y="0.0" value="-25" prompt="Valeur">Expression &quot;-25&quot; à 0.0, 0.0</Expression>
<Point name="P1" n="2" color="5" target="true" x="E1*cos(E2)" actx="2.7189233611099497" y="E1*sin(E2)" acty="-1.2678547852220983" shape="circle" fixed="true">Point à &quot;E1*cos(E2)&quot;, &quot;E1*sin(E2)&quot;</Point>
</Objects>
</Macro>
<Macro Name="Coordonnées polaires/pt par coord. (degrés, avec dialogue)" showduplicates="true">
<Parameter name="O">O</Parameter>
<Parameter name="I">I</Parameter>
<Comment>
<P>Nécessite la création préalable des deux points </P>
<P>fixes O(0,0) et I(1,0).</P>
</Comment>
<Objects>
<Point name="O" n="0" parameter="true" mainparameter="true" x="0.0" y="0.0">Point à 0.0, 0.0</Point>
<Point name="I" n="1" parameter="true" mainparameter="true" x="1.0" y="0.0">Point à 1.0, 0.0</Point>
<Expression name="r" n="2" color="1" type="thick" hidden="true" showname="true" x="0.0" y="0.0" value="-2.5" prompt="Valeur">Expression &quot;-2.5&quot; à 0.0, 0.0</Expression>
<Expression name="a" n="3" color="1" type="thick" hidden="true" showname="true" x="0.0" y="0.0" value="-30" prompt="Valeur">Expression &quot;-30&quot; à 0.0, 0.0</Expression>
<Expression name="E3" n="4" color="2" type="thick" hidden="super" showname="true" x="0.0" y="0.0" value="a" prompt="Valeur">Expression &quot;&quot; à 0.0, 0.0 </Expression>
<Angle name="a1" n="5" color="3" hidden="super" unit="°" large="true" first="I" root="O" fixed="E3">Angle P1 - P2 de mesure 25.629060000000003 </Angle>
<Circle name="c1" n="6" color="3" hidden="super" large="true" through="I" midpoint="O">Cercle de centre P2 passant par P1 </Circle>
<Intersection name="I1" n="7" color="2" hidden="super" showname="true" large="true" first="a1" second="c1" shape="circle" which="first">Intersection entre a1 et c1 </Intersection>
<Point name="P3" n="8" color="2" target="true" x="r*x(I1)" actx="-2.165063509461096" y="r*y(I1)" acty="1.250000000000001" shape="circle" fixed="true">Point à &quot;r*x(I1)&quot;, &quot;r*y(I1)&quot;</Point>
</Objects>
<PromptFor object0="a" prompt0="angle ?" object1="r" prompt1="r"/>
</Macro>
<Macro Name="Coordonnées polaires/Pt par coord. (radians, sans dialogue)" showduplicates="true">
<Parameter name="E2">angle en radians ?</Parameter>
<Parameter name="E1">rayon ?</Parameter>
<Objects>
<Expression name="E1" n="0" color="1" type="thick" showname="true" mainparameter="true" x="0.0" y="0.0" value="3" prompt="Valeur">Expression &quot;3&quot; à 0.0, 0.0</Expression>
<Expression name="E2" n="1" color="1" type="thick" showname="true" mainparameter="true" x="0.0" y="0.0" value="-3" prompt="Valeur">Expression &quot;-3&quot; à 0.0, 0.0</Expression>
<Point name="P1" n="2" color="5" target="true" x="E1*rcos(E2)" actx="-2.9699774898013365" y="E1*rsin(E2)" acty="-0.4233600241796016" shape="circle" fixed="true">Point à &quot;E1*rcos(E2)&quot;, &quot;E1*rsin(E2)&quot;</Point>
</Objects>
</Macro>
<Macro Name="Coordonnées polaires/pt par coord. (radians, avec dialogue)" showduplicates="true">
<Parameter name="O">O</Parameter>
<Parameter name="I">I</Parameter>
<Comment>
<P>Nécessite la création préalable des deux points </P>
<P>fixes O(0,0) et I(1,0).</P>
</Comment>
<Objects>
<Point name="O" n="0" parameter="true" mainparameter="true" x="0.0" y="0.0">Point à 0.0, 0.0</Point>
<Point name="I" n="1" parameter="true" mainparameter="true" x="1.0" y="0.0">Point à 1.0, 0.0</Point>
<Expression name="r" n="2" color="1" type="thick" hidden="true" showname="true" x="0.0" y="0.0" value="-2" prompt="Valeur">Expression &quot;-2&quot; à 0.0, 0.0</Expression>
<Expression name="a" n="3" color="1" type="thick" hidden="true" showname="true" x="0.0" y="0.0" value="2.2" prompt="Valeur">Expression &quot;2.2&quot; à 0.0, 0.0</Expression>
<Expression name="E3" n="4" color="2" type="thick" hidden="super" showname="true" x="0.0" y="0.0" value="a*180/pi" prompt="Valeur">Expression &quot;&quot; à 0.0, 0.0 </Expression>
<Angle name="a1" n="5" color="3" hidden="super" unit="°" large="true" first="I" root="O" fixed="E3">Angle P1 - P2 de mesure 25.629060000000003 </Angle>
<Circle name="c1" n="6" color="3" hidden="super" large="true" through="I" midpoint="O">Cercle de centre P2 passant par P1 </Circle>
<Intersection name="I1" n="7" color="2" hidden="super" showname="true" large="true" first="a1" second="c1" shape="circle" which="first">Intersection entre a1 et c1 </Intersection>
<Point name="P3" n="8" color="2" showname="true" target="true" x="x(I1)*r" actx="1.1770022345106925" y="y(I1)*r" acty="-1.6169928076391797" shape="circle" fixed="true">Point à &quot;x(I1)*r&quot;, &quot;y(I1)*r&quot;</Point>
</Objects>
<PromptFor object0="a" prompt0="angle ?" object1="r" prompt1="r ?"/>
</Macro>
<Macro Name="Coordonnées polaires/Coord polaires (degrés)" showduplicates="true">
<Parameter name="P1">P1</Parameter>
<Parameter name="P5">P5</Parameter>
<Objects>
<Point name="P1" n="0" mainparameter="true" x="-5.417543859649125" y="-0.9964912280701761">Point à -5.41754, -0.99649</Point>
<Point name="P5" n="1" color="1" mainparameter="true" target="true" x="(windoww/(windoww-d(windoww)))*(x(P5)-windowcx)+windowcx+d(windowcx)" actx="0.08421052631578974" y="(windoww/(windoww-d(windoww)))*(y(P5)-windowcy)+windowcy+d(windowcy)" acty="2.3719298245614038" shape="circle" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(P5)-windowcx)+windowcx+d(windowcx)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(P5)-windowcy)+windowcy+d(windowcy)&quot;</Point>
<Expression name="E1" n="2" color="2" type="thick" showname="true" showvalue="true" ctag0="showvalue" cexpr0="0" x="x(P5)+8/pixel" y="y(P5)+8/pixel" value="sqrt(x(P1)^2+y(P1)^2)" prompt="Valeur" fixed="true">Expression &quot;sqrt(x(P1)^2+y(P1)^2)&quot; à 0.16421, 2.45193</Expression>
<Expression name="E2" n="3" color="2" type="thick" showname="true" showvalue="true" unit=" °" ctag0="showvalue" cexpr0="0" x="x(P5)+8/pixel" y="y(P5)-12/pixel" value="arccos(x(P1)/sqrt(x(P1)^2+y(P1)^2))*(2*(y(P1)&gt;=0)-1)" prompt="Valeur" fixed="true">Expression &quot;arccos(x(P1)/sqrt(x(P1)^2+y(P1)^2))*(2*(y(P1)&gt;=0)-1)&quot; à 0.16421, 2.25193</Expression>
<Point name="P3" n="4" color="1" hidden="super" x="x(P5)" actx="0.08421052631578974" y="y(P5)+15/pixel" acty="2.5219298245614037" shape="dcross" fixed="true">Point à &quot;x(P5)&quot;, &quot;y(P5)+15/pixel&quot;</Point>
<Point name="P4" n="5" color="1" hidden="super" x="2*x(P5)-x(P3)" actx="0.08421052631578974" y="2*y(P5)-y(P3)" acty="2.221929824561404" shape="dcross" fixed="true">Point à &quot;2*x(P5)-x(P3)&quot;, &quot;2*y(P5)-y(P3)&quot;</Point>
<Segment name="s1" n="6" type="thin" target="true" ctag0="normal" cexpr0="1" from="P4" to="P3">Segment de P4 à P3</Segment>
</Objects>
</Macro>
<Macro Name="Coordonnées polaires/Coord polaires (radians)" showduplicates="true">
<Parameter name="P8">P8</Parameter>
<Parameter name="P9">P9</Parameter>
<Objects>
<Point name="P8" n="0" mainparameter="true" x="-3.8456140350877224" y="-3.101754385964913">Point à -3.84561, -3.10175</Point>
<Point name="P9" n="1" color="1" mainparameter="true" target="true" x="(windoww/(windoww-d(windoww)))*(x(P9)-windowcx)+windowcx+d(windowcx)" actx="0.5052631578947366" y="(windoww/(windoww-d(windoww)))*(y(P9)-windowcy)+windowcy+d(windowcy)" acty="-1.5578947368421057" shape="circle" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(P9)-windowcx)+windowcx+d(windowcx)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(P9)-windowcy)+windowcy+d(windowcy)&quot;</Point>
<Expression name="E1" n="2" color="2" type="thick" showname="true" showvalue="true" x="x(P9)+8/pixel" y="y(P9)+8/pixel" value="sqrt(x(P8)^2+y(P8)^2)" prompt="Valeur" fixed="true">Expression &quot;sqrt(x(P8)^2+y(P8)^2)&quot; à 0.58526, -1.47789</Expression>
<Expression name="E2" n="3" color="2" type="thick" showname="true" showvalue="true" unit=" rd" x="x(P9)+8/pixel" y="y(P9)-12/pixel" value="rarccos(x(P8)/sqrt(x(P8)^2+y(P8)^2))*(2*(y(P8)&gt;=0)-1)" prompt="Valeur" fixed="true">Expression &quot;rarccos(x(P8)/sqrt(x(P8)^2+y(P8)^2))*(2*(y(P8)&gt;=0)-1)&quot; à 0.58526, -1.67789</Expression>
<Point name="P3" n="4" color="1" hidden="super" x="x(P9)" actx="0.5052631578947366" y="y(P9)+15/pixel" acty="-1.4078947368421058" shape="dcross" fixed="true">Point à &quot;x(P9)&quot;, &quot;y(P9)+15/pixel&quot;</Point>
<Point name="P4" n="5" color="1" hidden="super" x="2*x(P9)-x(P3)" actx="0.5052631578947366" y="2*y(P9)-y(P3)" acty="-1.7078947368421056" shape="dcross" fixed="true">Point à &quot;2*x(P9)-x(P3)&quot;, &quot;2*y(P9)-y(P3)&quot;</Point>
<Segment name="s1" n="6" type="thin" target="true" ctag0="normal" cexpr0="1" from="P4" to="P3">Segment de P4 à P3</Segment>
</Objects>
</Macro>
<Macro Name="Opérations sur les complexes/Somme" showduplicates="true">
<Parameter name="a">a</Parameter>
<Parameter name="b">b</Parameter>
<Objects>
<Point name="a" n="0" mainparameter="true" x="2.0" y="1.5">Point à 2.0, 1.5</Point>
<Point name="b" n="1" mainparameter="true" x="1.0" y="1.5">Point à 1.0, 1.5</Point>
<Point name="P3" n="2" color="1" showname="true" target="true" x="x(a)+x(b)" actx="3.0" y="y(a)+y(b)" acty="3.0" shape="dcross" fixed="true">Point à &quot;x(a)+x(b)&quot;, &quot;y(a)+y(b)&quot;</Point>
</Objects>
</Macro>
<Macro Name="Opérations sur les complexes/Produit" showduplicates="true">
<Parameter name="a">a</Parameter>
<Parameter name="b">b</Parameter>
<Objects>
<Point name="a" n="0" mainparameter="true" x="2.0" y="1.5">Point à 2.0, 1.5</Point>
<Point name="b" n="1" mainparameter="true" x="1.0" y="1.5">Point à 1.0, 1.5</Point>
<Point name="P3" n="2" color="1" showname="true" target="true" x="x(a)*x(b)-y(a)*y(b)" actx="-0.25" y="x(a)*y(b)+x(b)*y(a)" acty="4.5" shape="dcross" fixed="true">Point à &quot;x(a)*x(b)-y(a)*y(b)&quot;, &quot;x(a)*y(b)+x(b)*y(a)&quot;</Point>
</Objects>
</Macro>
<Macro Name="Opérations sur les complexes/Inverse" showduplicates="true">
<Parameter name="P1">P1</Parameter>
<Objects>
<Point name="P1" n="0" x="0.5" y="0.5">Point à 0.5, 0.5</Point>
<Point name="P2" n="1" color="1" target="true" x="x(P1)/((x(P1))^2+(y(P1))^2)" actx="1.0" y="-y(P1)/((x(P1))^2+(y(P1))^2)" acty="-1.0" shape="circle" fixed="true">Point à &quot;x(P1)/((x(P1))^2+(y(P1))^2)&quot;, &quot;-y(P1)/((x(P1))^2+(y(P1))^2)&quot;</Point>
</Objects>
</Macro>
<Macro Name="Opérations sur les complexes/Produit par un réel (avec dialogue)" showduplicates="true">
<Parameter name="m">m</Parameter>
<Objects>
<Point name="m" n="0" mainparameter="true" x="1.5" y="2.0">Point à 1.5, 2.0</Point>
<Expression name="k" n="1" color="1" type="thick" hidden="true" showname="true" x="0.0" y="0.0" value="1.5" prompt="Valeur">Expression &quot;1.5&quot; à 0.0, 0.0</Expression>
<Point name="P2" n="2" color="1" showname="true" target="true" x="k*x(m)" actx="2.25" y="k*y(m)" acty="3.0" shape="dcross" fixed="true">Point à &quot;k*x(m)&quot;, &quot;k*y(m)&quot;</Point>
</Objects>
<PromptFor object0="k" prompt0="nombre réel ?"/>
</Macro>
<Macro Name="Pour &quot;experts&quot;/progress bar" showduplicates="true">
<Parameter name="a">a</Parameter>
<Objects>
<Point name="a" n="0" mainparameter="true" x="-5.221052631578949" y="4.7017543859649145">Point à -5.22105, 4.70175</Point>
<Expression name="E1" n="1" color="2" type="thick" showname="true" showvalue="true" x="x(a)" y="y(a)-50/pixel" value="sum(1,1)" prompt="val" fixed="true">Expression &quot;sum(1,1)&quot; à -5.22105, 4.20175</Expression>
<Expression name="E2" n="2" color="2" type="thick" showname="true" showvalue="true" x="x(a)" y="y(a)-30/pixel" value="1000" prompt="max" fixed="true">Expression &quot;1000&quot; à -5.22105, 4.40175</Expression>
<Expression name="E3" n="3" color="2" type="thick" showname="true" showvalue="true" x="x(a)" y="y(a)-70/pixel" value="5" prompt="d" fixed="true">Expression &quot;5&quot; à -5.22105, 4.00175</Expression>
<Point name="P2" n="4" color="2" hidden="true" x="x(a)+1" actx="-4.221052631578949" y="y(a)" acty="4.7017543859649145" shape="circle" fixed="true">Point à &quot;x(a)+1&quot;, &quot;y(a)&quot;</Point>
<Point name="P3" n="5" color="2" hidden="true" showname="true" xcoffset="-0.2286767699634673" ycoffset="-0.36940093609483293" keepclose="true" ctag0="z" cexpr0="5" x="x(a)" actx="-5.221052631578949" y="y(a)-10/pixel" acty="4.601754385964915" shape="circle" fixed="true">Point à &quot;x(a)&quot;, &quot;y(a)-10/pixel&quot;</Point>
<Polygon name="poly1" n="6" color="2" type="thin" background="true" point1="a">Polygone a</Polygon>
<Ray name="r1" n="7" color="2" hidden="true" from="a" to="P2">Demi-droite d&apos;origine a vers P2</Ray>
<Segment name="s1" n="8" color="2" type="thick" from="P3" to="a">Segment de P3 à a</Segment>
<PointOn name="po4" n="9" color="2" type="thick" xcoffset="0.08163066383408246" ycoffset="0.26385781149630727" keepclose="true" on="r1" alpha="9.375438596491229" x="4.154385964912279" y="4.7017543859649145" shape="dcross">Point sur r1</PointOn>
<Expression name="E4" n="10" color="2" type="thick" hidden="super" showname="true" showvalue="true" x="3.031578947368421" y="1.6701754385964929" value="(x(po4)-x(a))/E3" prompt="d">Expression &quot;(x(po4)-x(a))/E3&quot; à 3.03158, 1.67018</Expression>
<Point name="P5" n="11" color="2" hidden="true" showname="true" xcoffset="-0.0029704641350205563" ycoffset="0.32405063291139236" keepclose="true" x="x(a)+(x(po4)-x(a))*min(1,E1/E2)" actx="-5.211677192982458" y="y(a)" acty="4.7017543859649145" shape="circle" fixed="true">Point à &quot;x(a)+(x(po4)-x(a))*min(1,E1/E2)&quot;, &quot;y(a)&quot;</Point>
<Point name="P6" n="12" color="2" hidden="true" showname="true" x="x(po4)" actx="4.154385964912279" y="y(P3)" acty="4.601754385964915" shape="circle" fixed="true">Point à &quot;x(po4)&quot;, &quot;y(P3)&quot;</Point>
<Segment name="s2" n="13" color="2" type="thick" ctag0="z" cexpr0="5" from="a" to="po4">Segment de a à po4</Segment>
<Function name="f2" n="14" color="2" type="thick" ctag0="z" cexpr0="8" x="x(a)+floor((t+1)/2)*E4" y="if(floor(t/2)==2*floor(floor(t/2)/2),y(a),y(P3))" var="t" min="0" max="2*E3-2" d="1" shape="cross">Fonction ( x(a)+floor((t+1)/2)*E4 , if(floor(t/2)==2*floor(floor(t/2)/2),y(a),y(P3)) )</Function>
<Point name="P7" n="15" color="2" type="thin" hidden="true" showname="true" x="x(P5)" actx="-5.211677192982458" y="y(P3)" acty="4.601754385964915" shape="circle" fixed="true">Point à &quot;x(P5)&quot;, &quot;y(P3)&quot;</Point>
<Segment name="s3" n="16" color="2" type="thick" ctag0="z" cexpr0="5" from="po4" to="P6">Segment de po4 à P6</Segment>
<Segment name="s4" n="17" color="2" type="thick" ctag0="z" cexpr0="5" from="P6" to="P3">Segment de P6 à P3</Segment>
<Polygon name="poly3" n="18" color="2" type="thin" background="true" point1="a" point2="P5" point3="P7" point4="P3">Polygone a, P5, P7, P3</Polygon>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Curseurs/Linéaire discret">
<Parameter name="a">a</Parameter>
<Parameter name="t">t</Parameter>
<Objects>
<Point name="a" n="0" color="3" type="thick" xcoffset="-1.1386861313868617" ycoffset="-0.17518248175182283" keepclose="true" mainparameter="true" large="true" x="(windoww/(windoww-d(windoww)))*(x(a)-windowcx)+windowcx+d(windowcx)" actx="-3.6473223232169234" y="(windoww/(windoww-d(windoww)))*(y(a)-windowcy)+windowcy+d(windowcy)" acty="4.617358861339105" shape="circle" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(a)-windowcx)+windowcx+d(windowcx)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(a)-windowcy)+windowcy+d(windowcy)&quot;</Point>
<Point name="t" n="1" color="2" type="thick" showname="true" xcoffset="0.05839416058394242" ycoffset="0.29197080291970856" keepclose="true" mainparameter="true" large="true" x="if(t,(x(@a)/x(@a))*(windoww/(windoww-d(windoww)))*(x(t)-windowcx)+windowcx+d(windowcx),invalid)" actx="-2.661044980876497" y="(windoww/(windoww-d(windoww)))*(y(t)-windowcy)+windowcy+d(windowcy)" acty="4.624830761482854" shape="circle" fixed="true">Point à &quot;if(t,(x(@a)/x(@a))*(windoww/(windoww-d(windoww)))*(x(t)-windowcx)+windowcx+d(windowcx),invalid)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(t)-windowcy)+windowcy+d(windowcy)&quot;</Point>
<Expression name="E1" n="2" color="3" type="thick" hidden="true" showname="true" showvalue="true" bold="true" large="true" x="x(a)" y="y(a)+windoww/24" value="5" prompt="Nombre de divisions" fixed="true">Expression &quot;5&quot; à -3.64732, 4.95069</Expression>
<Point name="P3" n="3" color="5" hidden="super" showname="true" large="true" x="x(a)+windoww/8" actx="-2.6473223232169234" y="y(a)-windoww/12" acty="3.9506921946724387" shape="circle" fixed="true">Point à &quot;x(a)+windoww/8&quot;, &quot;y(a)-windoww/12&quot;</Point>
<Point name="P4" n="4" color="5" hidden="super" large="true" x="x(P3)+3*windoww" actx="21.352677676783077" y="y(P3)" acty="3.9506921946724387" shape="circle" fixed="true">Point à &quot;x(P3)+3*windoww&quot;, &quot;y(P3)&quot;</Point>
<Point name="P5" n="5" color="5" hidden="super" showname="true" large="true" x="x(P3)" actx="-2.6473223232169234" y="y(a)" acty="4.617358861339105" shape="circle" fixed="true">Point à &quot;x(P3)&quot;, &quot;y(a)&quot;</Point>
<Segment name="s1" n="6" color="5" hidden="super" large="true" from="P3" to="P4">Segment de P3 à P4</Segment>
<PointOn name="po6" n="7" color="5" large="true" on="s1" alpha="0.18217761557177614" x="1.724940450505704" y="3.9506921946724387" shape="diamond">Point sur s1</PointOn>
<Point name="P7" n="8" color="5" hidden="super" showname="true" large="true" x="x(po6)" actx="1.724940450505704" y="y(a)" acty="4.617358861339105" shape="circle" fixed="true">Point à &quot;x(po6)&quot;, &quot;y(a)&quot;</Point>
<Segment name="s2" n="9" color="2" large="true" ctag0="z" cexpr0="1" from="P5" to="P7">Segment de P5 à P7</Segment>
<Expression name="E2" n="10" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-6.552381352872471" y="0.30289615780976226" value="(x(P7)-x(P5))/(E1-1)" prompt="p">Expression &quot;(x(P7)-x(P5))/(E1-1)&quot; à -6.55238, 0.3029</Expression>
<Point name="P8" n="11" color="5" hidden="super" showname="true" large="true" x="if(x(t)&lt;x(P5),x(P5),if(x(t)&gt;x(P7),x(P7),x(P5)+E2*round((x(t)-x(P5))/E2)))" actx="-2.6473223232169234" y="y(a)" acty="4.617358861339105" shape="circle" fixed="true">Point à &quot;if(x(t)&lt;x(P5),x(P5),if(x(t)&gt;x(P7),x(P7),x(P5)+E2*round((x(t)-x(P5))/E2)))&quot;, &quot;y(a)&quot;</Point>
<Function name="f1" n="12" color="2" type="thin" showname="true" large="true" ctag0="z" cexpr0="2" x="x" y="y(a)" var="x" min="x(P5)" max="x(P7)" d="E2" special="true" shape="cross">Fonction ( x , y(a) )</Function>
<Circle name="c1" n="13" color="5" hidden="super" large="true" through="t" fixed="windoww/512" midpoint="P8" acute="true">Cercle de centre P8 passant par t de rayon &quot;windoww/512&quot;</Circle>
<Expression name="E3" n="14" color="2" type="thick" showname="true" showvalue="true" bold="true" large="true" x="x(a)-windoww/16" y="y(a)-windoww/128" value="round((x(P8)-x(P5))/E2)" prompt="Valeur" fixed="true">Expression &quot;round((x(P8)-x(P5))/E2)&quot; à -4.14732, 4.55486</Expression>
</Objects>
<PromptFor object0="E1" prompt0="e"/>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Curseurs/Linéaire continu" showduplicates="true">
<Parameter name="P">Point d&apos;ancrage du curseur</Parameter>
<Comment>
<P>Montrer le point d&apos;ancrage du curseur. Entrez ensuite le
min et le max.   </P>
<P>Le curseur créé est flottant, c&apos;est à dire qu&apos;il ne se
déplace pas avec la figure lorsqu&apos;on utilise les flèches du
clavier ou l&apos;outil de déplacement de la palette.   </P>
</Comment>
<Objects>
<Point name="P" n="0" xcoffset="0.023289245826245697" ycoffset="-0.2561817040887151" keepclose="true" mainparameter="true" bold="true" x="(windoww/(windoww-d(windoww)))*(x(P)-windowcx)+windowcx+d(windowcx)" actx="-2.6759637312032023" y="(windoww/(windoww-d(windoww)))*(y(P)-windowcy)+windowcy+d(windowcy)" acty="-0.600585937900215" shape="dcross" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(A)-windowcx)+windowcx+d(windowcx)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(A)-windowcy)+windowcy+d(windowcy)&quot; </Point>
<Point name="P2" n="1" hidden="true" bold="true" x="x(P)+3*windoww" actx="21.324036268796796" y="y(P)" acty="-0.600585937900215" fixed="true">Point à &quot;x(A)+3*windoww&quot;, &quot;y(A)&quot; </Point>
<Segment name="s1" n="2" hidden="true" bold="true" from="P" to="P2">Segment de A à P2</Segment>
<PointOn name="po3" n="3" xcoffset="0.023289245826246585" ycoffset="-0.3726279332199498" keepclose="true" bold="true" on="s1" alpha="0.02770794927657672" x="-2.010972948565361" y="-0.600585937900215" shape="dcross">Point sur s1 </PointOn>
<Point name="P4" n="4" bold="true" x="x(po3)" actx="-2.010972948565361" y="y(po3)+windoww/20" acty="-0.20058593790021495" shape="cross" fixed="true">Point à &quot;x(po3)&quot;, &quot;y(po3)+windoww/20&quot; </Point>
<Point name="P5" n="5" bold="true" x="x(P4)+2*(x(P)-x(po3))" actx="-3.3409545138410435" y="y(P4)" acty="-0.20058593790021495" shape="cross" fixed="true">Point à &quot;x(P4)+2*(x(A)-x(po3))&quot;, &quot;y(P4)&quot; </Point>
<Expression name="min" n="6" color="1" type="thick" showname="true" showvalue="true" x="x(P4)+windoww/30" y="y(P4)+windoww/25" value="-5" prompt="m" fixed="true">Expression &quot;-5&quot; à -1.74431, 0.11941 </Expression>
<Expression name="max" n="7" color="1" type="thick" showname="true" showvalue="true" x="x(P4)+windoww/30" y="y(P4)-windoww/40" value="5" prompt="M" fixed="true">Expression &quot;5&quot; à -1.74431, -0.40059 </Expression>
<Segment name="s2" n="8" bold="true" from="P5" to="P4">Segment de P5 à P4</Segment>
<PointOn name="po6" n="9" color="2" xcoffset="4.583102756861024E-6" ycoffset="0.14519389194800736" keepclose="true" bold="true" ctag0="green" cexpr0="1" ctag1="thick" cexpr1="1" on="s2" alpha="0.5044455252663747" x="-2.6700512645509566" y="-0.20058593790021495" shape="circle">Point sur s2 </PointOn>
<Expression name="E3" n="10" color="1" type="thick" showname="true" showvalue="true" bold="true" large="true" x="x(po6)-windoww/100" y="y(po6)+windoww/30" value="round((min+d(P5,po6)/d(P5,P4)*(max-min))*10)/10" prompt="" fixed="true">Expression &quot;round((E1+d(P5,po6)/d(P5,P4)*(E2-E1))*10)/10&quot; à -2.75005, 0.06608 </Expression>
</Objects>
<PromptFor object0="min" prompt0="Min" object1="max" prompt1="Max"/>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Curseurs/Circulaire continu" showduplicates="true">
<Parameter name="P">Point d&apos;ancrage du curseur</Parameter>
<Comment>
<P>Montrer le point d&apos;ancrage du curseur. Entrer ensuite le
nombre de décimales souhaitées pour le résultat et la
valeur numérique que doit représenter un tour.   </P>
<P>Le curseur créé est flottant, c&apos;est à dire qu&apos;il ne se
déplace pas avec la figure lorsqu&apos;on utilise les flèches du
clavier ou l&apos;outil de déplacement de la palette.   </P>
</Comment>
<Objects>
<Point name="P" n="0" mainparameter="true" bold="true" large="true" x="(windoww/(windoww-d(windoww)))*(x(P)-windowcx)+windowcx+d(windowcx)" actx="-3.9980148606078925" y="(windoww/(windoww-d(windoww)))*(y(P)-windowcy)+windowcy+d(windowcy)" acty="4.628332816193092" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(O)-windowcx)+windowcx+d(windowcx)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(O)-windowcy)+windowcy+d(windowcy)&quot; </Point>
<Expression name="E1" n="1" color="1" type="thick" hidden="true" showname="true" showvalue="true" x="x(P)-0.02*windoww" y="y(P)-0.11*windoww" value="1" prompt="p" fixed="true">Expression &quot;1&quot; à -4.15801, 3.74833 </Expression>
<Expression name="E2" n="2" color="1" type="thick" hidden="true" showname="true" showvalue="true" x="x(P)-0.02*windoww" y="y(P)-0.07*windoww" value="5" prompt="t" fixed="true">Expression &quot;5&quot; à -4.15801, 4.06833 </Expression>
<Point name="P2" n="3" hidden="true" bold="true" large="true" x="x(P)+3*windoww" actx="20.001985139392108" y="y(P)" acty="4.628332816193092" fixed="true">Point à &quot;x(O)+3*windoww&quot;, &quot;y(O)&quot; </Point>
<Segment name="s1" n="4" hidden="true" bold="true" large="true" from="P" to="P2">Segment de O à P2</Segment>
<PointOn name="po3" n="5" xcoffset="0.061320913273631206" ycoffset="-0.4599068495522296" keepclose="true" bold="true" large="true" on="s1" alpha="0.04480286738351252" x="-2.922746043403592" y="4.628332816193092" shape="cross">Point sur s1 </PointOn>
<Circle name="c1" n="6" color="3" background="true" bold="true" large="true" through="po3" midpoint="P" filled="true" acute="true">Cercle de centre O passant par po3</Circle>
<PointOn name="po4" n="7" color="5" type="thick" xcoffset="0.4284023244079842" ycoffset="0.1498854246755963" keepclose="true" bold="true" large="true" on="c1" alpha="0.3378804811492755" x="-2.9835424094450635" y="4.984771696063985" shape="circle">Point sur c1 </PointOn>
<Expression name="E3" n="8" color="1" type="thick" hidden="true" showname="true" showvalue="true" x="x(po3)+0.03*windoww" y="y(po3)+0.01*windoww" value="sum(-round(d(a(po3,P,po4))/360),0)" prompt="compte-tour" fixed="true">Expression &quot;sum(-round(d(a(po3,O,po4))/360),0)&quot; à -2.68275, 4.70833 </Expression>
<Expression name="E4" n="9" color="5" type="thick" showname="true" showvalue="true" bold="true" large="true" x="x(P)-0.02*windoww" y="y(P)+0.03*windoww" value="round((E3*360+a(po3,P,po4))*E2/360*10^E1)/10^E1" prompt="Valeur" fixed="true">Expression &quot;round((E3*360+a(po3,O,po4))*E2/360*10^E1)/10^E1&quot; à -4.15801, 4.86833 </Expression>
</Objects>
<PromptFor object0="E2" prompt0="valeur du tour" object1="E1" prompt1="nombre de decimales"/>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Curseurs/Phi, theta" showduplicates="true">
<Parameter name="A">Point d&apos;ancrage du curseur</Parameter>
<Comment>
<P>Montrer le point d&apos;ancrage du curseur.   </P>
<P>Le curseur créé est flottant, c&apos;est à dire qu&apos;il ne se
déplace pas avec la figure lorsqu&apos;on utilise les flèches du
clavier ou l&apos;outil de déplacement de la palette.   </P>
</Comment>
<Objects>
<Point name="A" n="0" mainparameter="true" target="true" bold="true" x="(windoww/(windoww-d(windoww)))*(x(A)-windowcx)+windowcx+d(windowcx)" actx="-6.62988" y="(windoww/(windoww-d(windoww)))*(y(A)-windowcy)+windowcy+d(windowcy)" acty="-0.43596" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(A)-windowcx)+windowcx+d(windowcx)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(A)-windowcy)+windowcy+d(windowcy)&quot; </Point>
<Point name="P2" n="1" hidden="super" bold="true" x="x(A)+3*windoww" actx="17.37012" y="y(A)" acty="-0.43596" fixed="true">Point à &quot;x(A)+3*windoww&quot;, &quot;y(A)&quot; </Point>
<Segment name="s1" n="2" hidden="super" bold="true" from="A" to="P2">Segment de A à P2</Segment>
<PointOn name="po3" n="3" target="true" bold="true" on="s1" alpha="0.044825833333333315" x="-5.554060000000001" y="-0.43596">Point sur s1 </PointOn>
<Circle name="c1" n="4" hidden="super" bold="true" through="po3" midpoint="A" acute="true">Cercle de centre A passant par po3</Circle>
<Segment name="s2" n="5" target="true" bold="true" from="A" to="po3">Segment de A à po3</Segment>
<Midpoint name="M1" n="6" hidden="super" bold="true" first="A" second="po3">Milieu de A et po3</Midpoint>
<PointOn name="po4" n="7" target="true" bold="true" on="c1" alpha="1.5707963267948966" x="-6.62988" y="0.6398599999999993" shape="circle">Point sur c1 </PointOn>
<Circle name="c2" n="8" hidden="super" bold="true" through="M1" midpoint="A" acute="true">Cercle de centre A passant par M1</Circle>
<Angle name="a1" alias="phi" n="9" xcoffset="1.3782771535580522" ycoffset="0.4344569288389515" keepclose="true" target="true" unit="°" bold="true" first="po3" root="A" second="po4" display="large">Angle po3 - A - po4</Angle>
<PointOn name="po5" n="10" type="thick" target="true" bold="true" on="c2" alpha="0.47501075626509637" x="-6.151523198255045" y="-0.18994793053116643" shape="circle">Point sur c2 </PointOn>
<Segment name="s3" n="11" target="true" bold="true" from="A" to="po4">Segment de A à po4</Segment>
<Segment name="s4" n="12" target="true" bold="true" from="A" to="po5">Segment de A à po5</Segment>
<Angle name="a2" alias="theta" n="13" target="true" unit="°" bold="true" first="M1" root="A" second="po5" display="large">Angle M1 - A - po5</Angle>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Objets flottants/Point flottant">
<Parameter name="P">P</Parameter>
<Comment>
<P>Place un point flottant dans la fenêtre (qui ne se déplace
pas avec la figure).   </P>
</Comment>
<Objects>
<Point name="P" n="0" mainparameter="true" target="true" bold="true" large="true" x="(windoww/(windoww-d(windoww)))*(x(P)-windowcx)+windowcx+d(windowcx)" actx="-2.465623785327825" y="(windoww/(windoww-d(windoww)))*(y(P)-windowcy)+windowcy+d(windowcy)" acty="2.820883453541457" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(P35)-windowcx)+windowcx+d(windowcx)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(P35)-windowcy)+windowcy+d(windowcy)&quot; </Point>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Objets flottants/Repère flottant" showduplicates="true">
<Parameter name="O">O</Parameter>
<Objects>
<Point name="O" n="0" color="2" showname="true" mainparameter="true" x="(windoww/(windoww-d(windoww)))*(x(O)-windowcx)+windowcx+d(windowcx)" actx="8.91905336252041" y="(windoww/(windoww-d(windoww)))*(y(O)-windowcy)+windowcy+d(windowcy)" acty="2.973017787506802" shape="dcross" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(O)-windowcx)+windowcx+d(windowcx)&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(O)-windowcy)+windowcy+d(windowcy)&quot;</Point>
<Point name="P2" n="1" color="2" showname="true" x="x(O)+windoww*3" actx="32.91905336252041" y="y(O)" acty="2.973017787506802" shape="circle" fixed="true">Point à &quot;x(O)+windoww*3&quot;, &quot;y(O)&quot;</Point>
<Segment name="s1" n="2" color="2" from="O" to="P2">Segment de O à P2</Segment>
<Point name="P3" n="3" color="2" showname="true" x="x(O)" actx="8.91905336252041" y="y(O)+d(O,P2)" acty="26.9730177875068" shape="circle" fixed="true">Point à &quot;x(O)&quot;, &quot;y(O)+d(O,P2)&quot;</Point>
<Segment name="s2" n="4" color="2" from="O" to="P3">Segment de O à P3</Segment>
<PointOn name="po4" n="5" color="2" showname="true" on="s1" alpha="0.06767713574625199" x="10.543304620430458" y="2.973017787506802" shape="circle">Point sur s1</PointOn>
<PointOn name="po5" n="6" color="2" showname="true" xcoffset="-1.5613891289437536" ycoffset="0.46715328467153583" keepclose="true" on="s2" alpha="0.041415492937315154" x="8.91905336252041" y="3.9669896180023656" shape="circle">Point sur s2</PointOn>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Objets flottants/Cadres/Vertical gauche">
<Parameter name="P">P</Parameter>
<Objects>
<Point name="P" n="0" mainparameter="true" target="true" bold="true" large="true" x="(windoww/(windoww-d(windoww)))*(x(P)-windowcx)+windowcx+d(windowcx)" actx="-14.291264795578904" y="windowcy" acty="0.0" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(P37)-windowcx)+windowcx+d(windowcx)&quot;, &quot;windowcy&quot; </Point>
<Point name="P2" n="1" hidden="true" bold="true" large="true" x="x(P)-2*windoww" actx="-30.291264795578904" y="y(P)+windowh" acty="8.0" fixed="true">Point à &quot;x(P37)-2*windoww&quot;, &quot;y(P37)+windowh&quot; </Point>
<Point name="P3" n="2" hidden="true" bold="true" large="true" x="x(P)" actx="-14.291264795578904" y="y(P)+windowh" acty="8.0" fixed="true">Point à &quot;x(P37)&quot;, &quot;y(P37)+windowh&quot; </Point>
<Point name="P4" n="3" hidden="true" bold="true" large="true" x="x(P)" actx="-14.291264795578904" y="y(P)-windowh" acty="-8.0" fixed="true">Point à &quot;x(P37)&quot;, &quot;y(P37)-windowh&quot; </Point>
<Point name="P5" n="4" hidden="true" bold="true" large="true" x="x(P)-2*windoww" actx="-30.291264795578904" y="y(P)-windowh" acty="-8.0" fixed="true">Point à &quot;x(P37)-2*windoww&quot;, &quot;y(P37)-windowh&quot; </Point>
<Polygon name="poly1" n="5" background="true" target="true" bold="true" large="true" point1="P2" point2="P3" point3="P4" point4="P5">Polygone P2, P3, P4, P5</Polygon>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Objets flottants/Cadres/Vertical droit">
<Parameter name="P">P</Parameter>
<Objects>
<Point name="P" n="0" mainparameter="true" target="true" bold="true" large="true" x="(windoww/(windoww-d(windoww)))*(x(P)-windowcx)+windowcx+d(windowcx)" actx="0.0" y="windowcy" acty="0.0" fixed="true">Point à &quot;(windoww/(windoww-d(windoww)))*(x(P42)-windowcx)+windowcx+d(windowcx)&quot;, &quot;windowcy&quot; </Point>
<Point name="P2" n="1" hidden="true" bold="true" large="true" x="x(P)+2*windoww" actx="16.0" y="y(P)+windowh" acty="8.0" fixed="true">Point à &quot;x(P42)+2*windoww&quot;, &quot;y(P42)+windowh&quot; </Point>
<Point name="P3" n="2" hidden="true" bold="true" large="true" x="x(P)" actx="0.0" y="y(P)+windowh" acty="8.0" fixed="true">Point à &quot;x(P42)&quot;, &quot;y(P42)+windowh&quot; </Point>
<Point name="P4" n="3" hidden="true" bold="true" large="true" x="x(P)" actx="0.0" y="y(P)-windowh" acty="-8.0" fixed="true">Point à &quot;x(P42)&quot;, &quot;y(P42)-windowh&quot; </Point>
<Point name="P5" n="4" hidden="true" bold="true" large="true" x="x(P)+2*windoww" actx="16.0" y="y(P)-windowh" acty="-8.0" fixed="true">Point à &quot;x(P42)+2*windoww&quot;, &quot;y(P42)-windowh&quot; </Point>
<Polygon name="poly1" n="5" background="true" target="true" bold="true" large="true" point1="P2" point2="P3" point3="P4" point4="P5">Polygone P2, P3, P4, P5</Polygon>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Objets flottants/Cadres/Horizontal haut">
<Parameter name="P">P</Parameter>
<Objects>
<Point name="P" n="0" mainparameter="true" target="true" bold="true" large="true" x="windowcx" actx="0.0" y="(windoww/(windoww-d(windoww)))*(y(P)-windowcy)+windowcy+d(windowcy)" acty="13.776239194639858" fixed="true">Point à &quot;windowcx&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(P47)-windowcy)+windowcy+d(windowcy)&quot; </Point>
<Point name="P2" n="1" hidden="true" bold="true" large="true" x="x(P)-2*windoww" actx="-16.0" y="y(P)+windowh" acty="21.776239194639857" fixed="true">Point à &quot;x(P47)-2*windoww&quot;, &quot;y(P47)+windowh&quot; </Point>
<Point name="P3" n="2" hidden="true" bold="true" large="true" x="x(P)+2*windoww" actx="16.0" y="y(P)+windowh" acty="21.776239194639857" fixed="true">Point à &quot;x(P47)+2*windoww&quot;, &quot;y(P47)+windowh&quot; </Point>
<Point name="P4" n="3" hidden="true" bold="true" large="true" x="x(P)+2*windoww" actx="16.0" y="y(P)" acty="13.776239194639858" fixed="true">Point à &quot;x(P47)+2*windoww&quot;, &quot;y(P47)&quot; </Point>
<Point name="P5" n="4" hidden="true" bold="true" large="true" x="x(P)-2*windoww" actx="-16.0" y="y(P)" acty="13.776239194639858" fixed="true">Point à &quot;x(P47)-2*windoww&quot;, &quot;y(P47)&quot; </Point>
<Polygon name="poly1" n="5" background="true" target="true" bold="true" large="true" point1="P2" point2="P3" point3="P4" point4="P5">Polygone P2, P3, P4, P5</Polygon>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Objets flottants/Cadres/Horizontal bas">
<Parameter name="P">P</Parameter>
<Objects>
<Point name="P" n="0" mainparameter="true" target="true" bold="true" large="true" x="windowcx" actx="0.0" y="(windoww/(windoww-d(windoww)))*(y(P)-windowcy)+windowcy+d(windowcy)" acty="0.0" fixed="true">Point à &quot;windowcx&quot;, &quot;(windoww/(windoww-d(windoww)))*(y(P52)-windowcy)+windowcy+d(windowcy)&quot; </Point>
<Point name="P2" n="1" hidden="true" bold="true" large="true" x="x(P)-2*windoww" actx="-16.0" y="y(P)-windowh" acty="-8.0" fixed="true">Point à &quot;x(P52)-2*windoww&quot;, &quot;y(P52)-windowh&quot; </Point>
<Point name="P3" n="2" hidden="true" bold="true" large="true" x="x(P)+2*windoww" actx="16.0" y="y(P)-windowh" acty="-8.0" fixed="true">Point à &quot;x(P52)+2*windoww&quot;, &quot;y(P52)-windowh&quot; </Point>
<Point name="P4" n="3" hidden="true" bold="true" large="true" x="x(P)+2*windoww" actx="16.0" y="y(P)" acty="0.0" fixed="true">Point à &quot;x(P52)+2*windoww&quot;, &quot;y(P52)&quot; </Point>
<Point name="P5" n="4" hidden="true" bold="true" large="true" x="x(P)-2*windoww" actx="-16.0" y="y(P)" acty="0.0" fixed="true">Point à &quot;x(P52)-2*windoww&quot;, &quot;y(P52)&quot; </Point>
<Polygon name="poly1" n="5" background="true" target="true" bold="true" large="true" point1="P2" point2="P3" point3="P4" point4="P5">Polygone P2, P3, P4, P5</Polygon>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Objets magnétiques/Point magnétique 5px" showduplicates="true">
<Parameter name="c">c</Parameter>
<Parameter name="d">d</Parameter>
<Objects>
<Point name="d" n="0" mainparameter="true" x="2.1897810218978098" y="3.576642335766424">Point à 2.18978, 3.57664</Point>
<Point name="P2" n="1" target="true" ctag0="superhidden" cexpr0="1" x="x(d)" actx="2.1897810218978098" y="y(d)" acty="3.576642335766424" shape="circle" fixed="true">Point à &quot;x(d)&quot;, &quot;y(d)&quot;</Point>
<Point name="c" n="2" color="2" type="thick" showname="true" mainparameter="true" target="true" x="if(c,if(pixel==0,x(c),if(d(c,@P2)&lt;(5/pixel),x(@P2),x(c))),invalid)" actx="-1.226277372262774" y="if(c,if(pixel==0,y(c),if(d(c,@P2)&lt;(5/pixel),y(@P2),y(c))),invalid)" acty="3.9270072992700733" shape="dcross" fixed="true">Point à &quot;if(c,if(pixel==0,x(c),if(d(c,@P2)&lt;(5/pixel),x(@P2),x(c))),invalid)&quot;, &quot;if(c,if(pixel==0,y(c),if(d(c,@P2)&lt;(5/pixel),y(@P2),y(c))),invalid)&quot;</Point>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Objets magnétiques/Point magnétique 10px" showduplicates="true">
<Parameter name="c">c</Parameter>
<Parameter name="d">d</Parameter>
<Objects>
<Point name="d" n="0" x="2.1897810218978098" y="3.576642335766424">Point à 2.18978, 3.57664</Point>
<Point name="P2" n="1" target="true" ctag0="superhidden" cexpr0="1" x="x(d)" actx="2.1897810218978098" y="y(d)" acty="3.576642335766424" shape="circle" fixed="true">Point à &quot;x(d)&quot;, &quot;y(d)&quot;</Point>
<Point name="c" n="2" color="2" type="thick" showname="true" mainparameter="true" target="true" x="if(c,if(pixel==0,x(c),if(d(c,@P2)&lt;(10/pixel),x(@P2),x(c))),invalid)" actx="-1.226277372262774" y="if(c,if(pixel==0,y(c),if(d(c,@P2)&lt;(10/pixel),y(@P2),y(c))),invalid)" acty="3.9270072992700733" shape="dcross" fixed="true">Point à &quot;if(c,if(pixel==0,x(c),if(d(c,@P2)&lt;(10/pixel),x(@P2),x(c))),invalid)&quot;, &quot;if(c,if(pixel==0,y(c),if(d(c,@P2)&lt;(10/pixel),y(@P2),y(c))),invalid)&quot;</Point>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Objets magnétiques/Point magnétique 15px" showduplicates="true">
<Parameter name="c">c</Parameter>
<Parameter name="d">d</Parameter>
<Objects>
<Point name="d" n="0" mainparameter="true" x="2.1897810218978098" y="3.576642335766424">Point à 2.18978, 3.57664</Point>
<Point name="P2" n="1" target="true" ctag0="superhidden" cexpr0="1" x="x(d)" actx="2.1897810218978098" y="y(d)" acty="3.576642335766424" shape="circle" fixed="true">Point à &quot;x(d)&quot;, &quot;y(d)&quot;</Point>
<Point name="c" n="2" color="2" type="thick" showname="true" mainparameter="true" target="true" x="if(c,if(pixel==0,x(c),if(d(c,@P2)&lt;(15/pixel),x(@P2),x(c))),invalid)" actx="-1.226277372262774" y="if(c,if(pixel==0,y(c),if(d(c,@P2)&lt;(15/pixel),y(@P2),y(c))),invalid)" acty="3.9270072992700733" shape="dcross" fixed="true">Point à &quot;if(c,if(pixel==0,x(c),if(d(c,@P2)&lt;(15/pixel),x(@P2),x(c))),invalid)&quot;, &quot;if(c,if(pixel==0,y(c),if(d(c,@P2)&lt;(15/pixel),y(@P2),y(c))),invalid)&quot;</Point>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Objets magnétiques/Point magnétique 20px" showduplicates="true">
<Parameter name="c">c</Parameter>
<Parameter name="d">d</Parameter>
<Objects>
<Point name="d" n="0" x="2.1897810218978098" y="3.576642335766424">Point à 2.18978, 3.57664</Point>
<Point name="P2" n="1" target="true" ctag0="superhidden" cexpr0="1" x="x(d)" actx="2.1897810218978098" y="y(d)" acty="3.576642335766424" shape="circle" fixed="true">Point à &quot;x(d)&quot;, &quot;y(d)&quot;</Point>
<Point name="c" n="2" color="2" type="thick" showname="true" mainparameter="true" target="true" x="if(c,if(pixel==0,x(c),if(d(c,@P2)&lt;(20/pixel),x(@P2),x(c))),invalid)" actx="-1.226277372262774" y="if(c,if(pixel==0,y(c),if(d(c,@P2)&lt;(20/pixel),y(@P2),y(c))),invalid)" acty="3.9270072992700733" shape="dcross" fixed="true">Point à &quot;if(c,if(pixel==0,x(c),if(d(c,@P2)&lt;(20/pixel),x(@P2),x(c))),invalid)&quot;, &quot;if(c,if(pixel==0,y(c),if(d(c,@P2)&lt;(20/pixel),y(@P2),y(c))),invalid)&quot;</Point>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Objets magnétiques/Droite magnétique 15px" showduplicates="true">
<Parameter name="P3">P3</Parameter>
<Parameter name="l1">l1</Parameter>
<Objects>
<Line name="l1" n="0" mainparameter="true">???</Line>
<Point name="P3" n="1" mainparameter="true" target="true" x="if(P3,if(pixel==0,x(P3),if(d(P3,@P2)&lt;(15/pixel),x(@P2),x(P3))),invalid)" actx="-0.43795620437956195" y="if(P3,if(pixel==0,y(P3),if(d(P3,@P2)&lt;(15/pixel),y(@P2),y(P3))),invalid)" acty="1.9708029197080297" shape="circle" fixed="true">Point à &quot;if(P3,if(pixel==0,x(P3),if(d(P3,@P2)&lt;(15/pixel),x(@P2),x(P3))),invalid)&quot;, &quot;if(P3,if(pixel==0,y(P3),if(d(P3,@P2)&lt;(15/pixel),y(@P2),y(P3))),invalid)&quot;</Point>
<Plumb name="perp1" n="2" hidden="super" point="P3" line="l1" valid="true">Perpendiculaire passant par P3 à l1</Plumb>
<Intersection name="I1" n="3" hidden="super" first="perp1" second="l1" shape="circle">Intersection entre perp1 et l1</Intersection>
<Point name="P2" n="4" target="true" ctag0="superhidden" cexpr0="1" x="x(I1)" actx="0.07924275258740543" y="y(I1)" acty="0.2943649212633772" shape="circle" fixed="true">Point à &quot;x(I1)&quot;, &quot;y(I1)&quot;</Point>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Objets magnétiques/Cercle magnétique 15px" showduplicates="true">
<Parameter name="P3">P3</Parameter>
<Parameter name="c1">c1</Parameter>
<Objects>
<Point name="P1" n="0" parameter="true" x="-0.17518248175182466" y="0.2481751824817525">Point à -0.17518, 0.24818</Point>
<Circle name="c1" n="1" mainparameter="true" midpoint="P1">???</Circle>
<Point name="P3" n="2" mainparameter="true" target="true" x="if(P3,if(pixel==0,x(P3),if(d(P3,@P4)&lt;(15/pixel),x(@P4),x(P3))),invalid)" actx="3.8540145985401466" y="if(P3,if(pixel==0,y(P3),if(d(P3,@P4)&lt;(15/pixel),y(@P4),y(P3))),invalid)" acty="1.8248175182481754" shape="circle" fixed="true">Point à &quot;if(P3,if(pixel==0,x(P3),if(d(P3,@P4)&lt;(15/pixel),x(@P4),x(P3))),invalid)&quot;, &quot;if(P3,if(pixel==0,y(P3),if(d(P3,@P4)&lt;(15/pixel),y(@P4),y(P3))),invalid)&quot;</Point>
<Ray name="r1" n="3" hidden="super" from="P1" to="P3">Demi-droite d&apos;origine P1 vers P3</Ray>
<Intersection name="I1" n="4" hidden="super" first="r1" second="c1" shape="circle" which="first">Intersection entre r1 et c1</Intersection>
<Point name="P4" n="5" target="true" ctag0="superhidden" cexpr0="1" x="x(I1)" actx="2.3440746253764755" y="y(I1)" acty="1.2339714417928258" shape="circle" fixed="true">Point à &quot;x(I1)&quot;, &quot;y(I1)&quot;</Point>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Grilles et repères/Petite grille orthonormée" showduplicates="true">
<Parameter name="P11">P11</Parameter>
<Objects>
<Point name="P11" n="0" mainparameter="true" x="-2.890510948905109" y="-0.8029197080291974">Point à -2.89051, -0.80292</Point>
<Point name="P2" n="1" color="5" hidden="super" large="true" x="x(P11)+1" actx="-1.890510948905109" y="y(P11)" acty="-0.8029197080291974" shape="circle" fixed="true">Point à &quot;x(P11)+1&quot;, &quot;y(P11)&quot;</Point>
<Point name="P3" n="2" color="5" hidden="super" large="true" x="x(P11)" actx="-2.890510948905109" y="y(P11)+1" acty="0.19708029197080257" shape="circle" fixed="true">Point à &quot;x(P11)&quot;, &quot;y(P11)+1&quot;</Point>
<Ray name="r1" n="3" color="5" hidden="super" large="true" from="P11" to="P2">Demi-droite d&apos;origine P11 vers P2</Ray>
<Ray name="r2" n="4" color="5" hidden="super" large="true" from="P11" to="P3">Demi-droite d&apos;origine P11 vers P3</Ray>
<PointOn name="po4" n="5" color="5" type="thick" target="true" large="true" on="r1" alpha="7.451776649746192" x="4.561265700841083" y="-0.8029197080291974" shape="circle">Point sur r1</PointOn>
<PointOn name="po5" n="6" color="5" type="thick" xcoffset="-1.3138686131386867" ycoffset="-0.058394160583940646" keepclose="true" target="true" large="true" on="r2" alpha="5.82741116751269" x="-2.890510948905109" y="5.024491459483492" shape="circle">Point sur r2</PointOn>
<Function name="f1" n="7" color="4" type="thick" showname="true" target="true" large="true" ctag0="z" cexpr0="1" x="if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),x(P11),x(po4)),if(floor((t-1)/4)==(t-1)/4,x(P11),x(po4)))" y="if(floor(t/2)==(t/2),y(P11)+t/2,y(P11)+(t+1)/2)" var="t" min="0" max="2*floor(y(po5)-y(P11))" d="1" shape="cross">Fonction ( if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),x(P11),x(po4)),if(floor((t-1)/4)==(t-1)/4,x(P11),x(po4))) , if(floor(t/2)==(t/2),y(P11)+t/2,y(P11)+(t+1)/2) )</Function>
<Function name="f2" n="8" color="4" type="thick" showname="true" target="true" large="true" ctag0="z" cexpr0="1" x="if(floor(t/2)==(t/2),x(P11)+t/2,x(P11)+(t+1)/2)" y="if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),y(P11),y(po5)),if(floor((t-1)/4)==(t-1)/4,y(P11),y(po5)))" var="t" min="0" max="2*floor(x(po4)-x(P11))" d="1" shape="cross">Fonction ( if(floor(t/2)==(t/2),x(P11)+t/2,x(P11)+(t+1)/2) , if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),y(P11),y(po5)),if(floor((t-1)/4)==(t-1)/4,y(P11),y(po5))) )</Function>
<Segment name="s1" n="9" color="5" type="thick" target="true" large="true" ctag0="z" cexpr0="0" from="P11" to="po5">Segment de P11 à po5</Segment>
<Point name="P6" n="10" color="4" hidden="super" showname="true" large="true" x="x(po4)" actx="4.561265700841083" y="y(po5)" acty="5.024491459483492" shape="circle" fixed="true">Point à &quot;x(po4)&quot;, &quot;y(po5)&quot;</Point>
<Segment name="s2" n="11" color="5" type="thick" target="true" large="true" ctag0="z" cexpr0="0" from="po4" to="P11">Segment de po4 à P11</Segment>
<Segment name="s3" n="12" color="5" type="thick" target="true" large="true" ctag0="z" cexpr0="0" from="po5" to="P6">Segment de po5 à P6</Segment>
<Segment name="s4" n="13" color="5" type="thick" target="true" large="true" ctag0="z" cexpr0="0" from="P6" to="po4">Segment de P6 à po4</Segment>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Grilles et repères/Grille orthonormée" showduplicates="true">
<Parameter name="P1">P1</Parameter>
<Objects>
<Point name="P1" n="0" mainparameter="true" x="0.5888324873096452" y="0.45685279187817257">Point à 0.58883, 0.45685</Point>
<Point name="P2" n="1" color="5" hidden="super" large="true" x="x(P1)-2*floor(windoww)" actx="-15.411167512690355" y="y(P1)-2*floor(windoww)" acty="-15.543147208121827" shape="circle" fixed="true">Point à &quot;x(P1)-2*floor(windoww)&quot;, &quot;y(P1)-2*floor(windoww)&quot;</Point>
<Point name="P3" n="2" color="5" hidden="super" large="true" x="x(P1)+2*floor(windoww)" actx="16.588832487309645" y="y(P1)-2*floor(windoww)" acty="-15.543147208121827" shape="circle" fixed="true">Point à &quot;x(P1)+2*floor(windoww)&quot;, &quot;y(P1)-2*floor(windoww)&quot;</Point>
<Point name="P4" n="3" color="5" hidden="super" large="true" x="x(P1)-2*floor(windoww)" actx="-15.411167512690355" y="y(P1)+2*floor(windoww)" acty="16.456852791878173" shape="circle" fixed="true">Point à &quot;x(P1)-2*floor(windoww)&quot;, &quot;y(P1)+2*floor(windoww)&quot;</Point>
<Function name="f1" n="4" color="4" type="thick" showname="true" target="true" large="true" ctag0="z" cexpr0="1" x="if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),x(P2),x(P3)),if(floor((t-1)/4)==(t-1)/4,x(P2),x(P3)))" y="if(floor(t/2)==(t/2),y(P2)+t/2,y(P2)+(t+1)/2)" var="t" min="0" max="2*floor(y(P4)-y(P2))" d="1" shape="cross">Fonction ( if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),x(P2),x(P3)),if(floor((t-1)/4)==(t-1)/4,x(P2),x(P3))) , if(floor(t/2)==(t/2),y(P2)+t/2,y(P2)+(t+1)/2) )</Function>
<Function name="f2" n="5" color="4" type="thick" showname="true" target="true" large="true" ctag0="z" cexpr0="1" x="if(floor(t/2)==(t/2),x(P2)+t/2,x(P2)+(t+1)/2)" y="if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),y(P2),y(P4)),if(floor((t-1)/4)==(t-1)/4,y(P2),y(P4)))" var="t" min="0" max="2*floor(x(P3)-x(P2))" d="1" shape="cross">Fonction ( if(floor(t/2)==(t/2),x(P2)+t/2,x(P2)+(t+1)/2) , if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),y(P2),y(P4)),if(floor((t-1)/4)==(t-1)/4,y(P2),y(P4))) )</Function>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Grilles et repères/Grille oblique 2 pts" showduplicates="true">
<Parameter name="O">O</Parameter>
<Parameter name="P38">P38</Parameter>
<Objects>
<Point name="O" n="0" mainparameter="true" x="-1.313868613138686" y="-0.3649635036496353">Point à -1.31387, -0.36496</Point>
<Point name="P2" n="1" color="2" hidden="super" large="true" x="x(O)+1" actx="-0.3138686131386861" y="y(O)" acty="-0.3649635036496353" shape="circle" fixed="true">Point à &quot;x(O)+1&quot;, &quot;y(O)&quot;</Point>
<Ray name="r1" n="2" color="2" hidden="super" large="true" from="O" to="P2">Demi-droite d&apos;origine O vers P2</Ray>
<PointOn name="po3" n="3" color="2" target="true" large="true" on="r1" alpha="1.7810218978102181" x="0.46715328467153205" y="-0.3649635036496353" shape="circle">Point sur r1</PointOn>
<Point name="P38" n="4" mainparameter="true" x="0.17518248175182552" y="0.715328467153283">Point à 0.17518, 0.71533</Point>
<Point name="P5" n="5" color="2" hidden="super" showname="true" bold="true" x="x(P38)+x(po3)-x(O)" actx="1.9562043795620436" y="y(P38)+y(po3)-y(O)" acty="0.715328467153283" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Circle name="c1" n="6" color="2" hidden="super" large="true" fixed="(1+floor(4*windoww/d(P5,O)))*d(P5,O)" midpoint="O" acute="true">Cercle de centre O de rayon (1+floor(4*windoww/d(P5,O)))*d(P5,O)</Circle>
<Line name="l1" n="7" color="2" hidden="super" large="true" from="O" to="P5">Droite passant par O et P5</Line>
<Angle name="a1" n="8" color="1" hidden="super" unit="∞" large="true" first="po3" root="O" fixed="90" inverse="true">Angle po3 - O de mesure 90</Angle>
<Intersection name="I1" n="9" color="1" hidden="super" large="true" first="a1" second="c1" shape="circle" which="first">Intersection entre a1 et c1</Intersection>
<Point name="P6" n="10" color="1" hidden="super" bold="true" x="x(I1)+x(po3)-x(O)" actx="0.46715328467152584" y="y(I1)+y(po3)-y(O)" acty="-34.803906750752844" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Line name="l2" n="11" color="1" hidden="super" large="true" from="I1" to="P6">Droite passant par I1 et P6</Line>
<Intersection name="I2" n="12" color="1" hidden="super" showname="true" large="true" first="l1" second="l2" shape="circle">Intersection entre l1 et l2</Intersection>
<Angle name="a2" n="13" color="3" hidden="super" unit="∞" large="true" first="P38" root="O" fixed="90" acute="true">Angle P38 - O de mesure 90</Angle>
<Intersection name="I3" n="14" color="3" hidden="super" large="true" first="a2" second="c1" shape="circle" which="first">Intersection entre a2 et c1</Intersection>
<Point name="P7" n="15" color="3" hidden="super" bold="true" x="x(I3)+x(P38)-x(O)" actx="-20.048316030866317" y="y(I3)+y(P38)-y(O)" acty="28.590961552113484" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Line name="l3" n="16" color="3" hidden="super" large="true" from="P7" to="I3">Droite passant par P7 et I3</Line>
<Intersection name="I4" n="17" color="3" hidden="super" showname="true" large="true" first="l1" second="l3" shape="circle">Intersection entre l1 et l3</Intersection>
<Point name="P8" n="18" color="5" type="thick" hidden="super" showname="true" large="true" x="if(d(O,I2)&gt;d(O,I4),x(I2),x(I4))" actx="-108.99299755313955" y="if(d(O,I2)&gt;d(O,I4),y(I2),y(I4))" acty="-35.93753288561417" shape="circle" fixed="true">Point à &quot;if(d(O,I2)&gt;d(O,I4),x(I2),x(I4))&quot;, &quot;if(d(O,I2)&gt;d(O,I4),y(I2),y(I4))&quot;</Point>
<Circle name="c2" n="19" color="1" hidden="super" large="true" fixed="(1+floor(d(O,P8)/d(O,P5)))*d(O,P5)" midpoint="O" acute="true">Cercle de centre O de rayon (1+floor(d(O,P8)/d(O,P5)))*d(O,P5)</Circle>
<Intersection name="I5" n="20" color="2" type="thick" hidden="super" showname="true" large="true" first="l1" second="c2" shape="circle" which="second">Intersection entre l1 et c2</Intersection>
<Point name="P9" n="21" color="1" hidden="super" showname="true" bold="true" x="x(I5)+x(po3)-x(O)" actx="-107.44525547445255" y="y(I5)+y(po3)-y(O)" acty="-36.01459854014594" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Point name="P10" n="22" color="1" hidden="super" showname="true" bold="true" x="x(I5)+x(P38)-x(O)" actx="-107.73722627737224" y="y(I5)+y(P38)-y(O)" acty="-34.93430656934302" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Point name="P11" n="23" color="2" type="thick" hidden="super" x="2*x(O)-x(I5)" actx="106.59854014598538" y="2*y(O)-y(I5)" acty="35.284671532846666" shape="circle" fixed="true">Point à &quot;2*x(O)-x(I5)&quot;, &quot;2*y(O)-y(I5)&quot;</Point>
<Point name="P12" n="24" color="2" type="thick" hidden="super" bold="true" x="x(P11)+x(I5)-x(P9)" actx="104.81751824817518" y="y(P11)+y(I5)-y(P9)" acty="35.284671532846666" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Point name="P13" n="25" color="2" type="thick" hidden="super" bold="true" x="x(P11)+x(I5)-x(P10)" actx="105.10948905109487" y="y(P11)+y(I5)-y(P10)" acty="34.20437956204375" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Line name="l4" n="26" color="1" hidden="super" large="true" from="I5" to="P9">Droite passant par I5 et P9</Line>
<Line name="l5" n="27" color="1" hidden="super" large="true" from="I5" to="P10">Droite passant par I5 et P10</Line>
<Line name="l6" n="28" color="1" hidden="super" large="true" from="P11" to="P12">Droite passant par P11 et P12</Line>
<Line name="l7" n="29" color="1" hidden="super" large="true" from="P11" to="P13">Droite passant par P11 et P13</Line>
<Intersection name="I6" n="30" color="1" hidden="super" showname="true" large="true" first="l7" second="l4" shape="circle">Intersection entre l7 et l4</Intersection>
<Intersection name="I7" n="31" color="1" hidden="super" showname="true" large="true" first="l6" second="l5" shape="circle">Intersection entre l6 et l5</Intersection>
<Function name="f8" n="32" color="1" type="thick" showname="true" target="true" large="true" x="if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),x(I5)+(t/2)*(x(P10)-x(I5)),x(I6)+(t/2)*(x(P10)-x(I5))),if(floor((t-1)/4)==(t-1)/4,x(I5)+((t+1)/2)*(x(P10)-x(I5)),x(I6)+((t+1)/2)*(x(P10)-x(I5))))" y="if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),y(I5)+(t/2)*(y(P10)-y(I5)),y(I6)+(t/2)*(y(P10)-y(I5))),if(floor((t-1)/4)==(t-1)/4,y(I5)+((t+1)/2)*(y(P10)-y(I5)),y(I6)+((t+1)/2)*(y(P10)-y(I5))))" var="t" min="0" max="floor(((y(I7)-y(I5))/(y(P10)-y(I5)))*2)-1" d="1" shape="cross">Fonction ( if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),x(I5)+(t/2)*(x(P10)-x(I5)),x(I6)+(t/2)*(x(P10)-x(I5))),if(floor((t-1)/4)==(t-1)/4,x(I5)+((t+1)/2)*(x(P10)-x(I5)),x(I6)+((t+1)/2)*(x(P10)-x(I5)))) , if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),y(I5)+(t/2)*(y(P10)-y(I5)),y(I6)+(t/2)*(y(P10)-y(I5))),if(floor((t-1)/4)==(t-1)/4,y(I5)+((t+1)/2)*(y(P10)-y(I5)),y(I6)+((t+1)/2)*(y(P10)-y(I5)))) )</Function>
<Function name="f9" n="33" color="1" type="thick" showname="true" target="true" large="true" x="if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),x(I5)+(t/2)*(x(P9)-x(I5)),x(I7)+(t/2)*(x(P9)-x(I5))),if(floor((t-1)/4)==(t-1)/4,x(I5)+((t+1)/2)*(x(P9)-x(I5)),x(I7)+((t+1)/2)*(x(P9)-x(I5))))" y="if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),y(I5)+(t/2)*(y(P9)-y(I5)),y(I7)+(t/2)*(y(P9)-y(I5))),if(floor((t-1)/4)==(t-1)/4,y(I5)+((t+1)/2)*(y(P9)-y(I5)),y(I7)+((t+1)/2)*(y(P9)-y(I5))))" var="t" min="0" max="floor(((x(I6)-x(I5))/(x(P9)-x(I5)))*2)-1" d="1" shape="cross">Fonction ( if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),x(I5)+(t/2)*(x(P9)-x(I5)),x(I7)+(t/2)*(x(P9)-x(I5))),if(floor((t-1)/4)==(t-1)/4,x(I5)+((t+1)/2)*(x(P9)-x(I5)),x(I7)+((t+1)/2)*(x(P9)-x(I5)))) , if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),y(I5)+(t/2)*(y(P9)-y(I5)),y(I7)+(t/2)*(y(P9)-y(I5))),if(floor((t-1)/4)==(t-1)/4,y(I5)+((t+1)/2)*(y(P9)-y(I5)),y(I7)+((t+1)/2)*(y(P9)-y(I5)))) )</Function>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Grilles et repères/Grille oblique 3 pts" showduplicates="true">
<Parameter name="P18">P18</Parameter>
<Parameter name="P19">P19</Parameter>
<Parameter name="P20">P20</Parameter>
<Objects>
<Point name="P18" n="0" mainparameter="true" x="-1.5474452554744529" y="0.45255474452554817">Point à -1.54745, 0.45255</Point>
<Point name="P19" n="1" mainparameter="true" x="1.1094890510948905" y="-0.6569343065693438">Point à 1.10949, -0.65693</Point>
<Point name="P20" n="2" mainparameter="true" x="1.3722627737226285" y="1.124087591240876">Point à 1.37226, 1.12409</Point>
<Point name="P4" n="3" color="2" hidden="super" showname="true" bold="true" x="x(P20)+x(P19)-x(P18)" actx="4.029197080291972" y="y(P20)+y(P19)-y(P18)" acty="0.014598540145984107" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Circle name="c1" n="4" color="2" hidden="super" large="true" fixed="(1+floor(4*windoww/d(P4,P18)))*d(P4,P18)" midpoint="P18" acute="true">Cercle de centre P18 de rayon (1+floor(4*windoww/d(P4,P18)))*d(P4,P18)</Circle>
<Line name="l1" n="5" color="2" hidden="super" large="true" from="P18" to="P4">Droite passant par P18 et P4</Line>
<Angle name="a1" n="6" color="1" hidden="super" unit="∞" large="true" first="P19" root="P18" fixed="90" inverse="true">Angle P19 - P18 de mesure 90</Angle>
<Intersection name="I1" n="7" color="1" hidden="super" large="true" first="a1" second="c1" shape="circle" which="first">Intersection entre a1 et c1</Intersection>
<Point name="P5" n="8" color="1" hidden="super" bold="true" x="x(I1)+x(P19)-x(P18)" actx="-11.82347279375302" y="y(I1)+y(P19)-y(P18)" acty="-31.62797451396824" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Line name="l2" n="9" color="1" hidden="super" large="true" from="I1" to="P5">Droite passant par I1 et P5</Line>
<Intersection name="I2" n="10" color="1" hidden="super" showname="true" large="true" first="l1" second="l2" shape="circle">Intersection entre l1 et l2</Intersection>
<Angle name="a2" n="11" color="3" hidden="super" unit="∞" large="true" first="P20" root="P18" fixed="90" acute="true">Angle P20 - P18 de mesure 90</Angle>
<Intersection name="I3" n="12" color="3" hidden="super" large="true" first="a2" second="c1" shape="circle" which="first">Intersection entre a2 et c1</Intersection>
<Point name="P6" n="13" color="3" hidden="super" bold="true" x="x(I3)+x(P20)-x(P18)" actx="-6.150779077984127" y="y(I3)+y(P20)-y(P18)" acty="33.832965207357255" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Line name="l3" n="14" color="3" hidden="super" large="true" from="P6" to="I3">Droite passant par P6 et I3</Line>
<Intersection name="I4" n="15" color="3" hidden="super" showname="true" large="true" first="l1" second="l3" shape="circle">Intersection entre l1 et l3</Intersection>
<Point name="P7" n="16" color="5" type="thick" hidden="super" showname="true" large="true" x="if(d(P18,I2)&gt;d(P18,I4),x(I2),x(I4))" actx="-113.1694179896292" y="if(d(P18,I2)&gt;d(P18,I4),y(I2),y(I4))" acty="9.218678257679098" shape="circle" fixed="true">Point à &quot;if(d(P18,I2)&gt;d(P18,I4),x(I2),x(I4))&quot;, &quot;if(d(P18,I2)&gt;d(P18,I4),y(I2),y(I4))&quot;</Point>
<Circle name="c2" n="17" color="1" hidden="super" large="true" fixed="(1+floor(d(P18,P7)/d(P18,P4)))*d(P18,P4)" midpoint="P18" acute="true">Cercle de centre P18 de rayon (1+floor(d(P18,P7)/d(P18,P4)))*d(P18,P4)</Circle>
<Intersection name="I5" n="18" color="2" type="thick" hidden="super" showname="true" large="true" first="l1" second="c2" shape="circle" which="second">Intersection entre l1 et c2</Intersection>
<Point name="P8" n="19" color="1" hidden="super" showname="true" bold="true" x="x(I5)+x(P19)-x(P18)" actx="-116.00000000000001" y="y(I5)+y(P19)-y(P18)" acty="8.5401459854015" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Point name="P9" n="20" color="1" hidden="super" showname="true" bold="true" x="x(I5)+x(P20)-x(P18)" actx="-115.73722627737229" y="y(I5)+y(P20)-y(P18)" acty="10.32116788321172" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Point name="P10" n="21" color="2" type="thick" hidden="super" x="2*x(P18)-x(I5)" actx="115.56204379562047" y="2*y(P18)-y(I5)" acty="-8.744525547445296" shape="circle" fixed="true">Point à &quot;2*x(P18)-x(I5)&quot;, &quot;2*y(P18)-y(I5)&quot;</Point>
<Point name="P11" n="22" color="2" type="thick" hidden="super" bold="true" x="x(P10)+x(I5)-x(P8)" actx="112.90510948905111" y="y(P10)+y(I5)-y(P8)" acty="-7.635036496350404" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Point name="P12" n="23" color="2" type="thick" hidden="super" bold="true" x="x(P10)+x(I5)-x(P9)" actx="112.64233576642339" y="y(P10)+y(I5)-y(P9)" acty="-9.416058394160624" shape="circle" fixed="true">Point à &quot;x(C)+x(B)-x(A)&quot;, &quot;y(C)+y(B)-y(A)&quot; </Point>
<Line name="l4" n="24" color="1" hidden="super" large="true" from="I5" to="P8">Droite passant par I5 et P8</Line>
<Line name="l5" n="25" color="1" hidden="super" large="true" from="I5" to="P9">Droite passant par I5 et P9</Line>
<Line name="l6" n="26" color="1" hidden="super" large="true" from="P10" to="P11">Droite passant par P10 et P11</Line>
<Line name="l7" n="27" color="1" hidden="super" large="true" from="P10" to="P12">Droite passant par P10 et P12</Line>
<Intersection name="I6" n="28" color="1" hidden="super" showname="true" large="true" first="l7" second="l4" shape="circle">Intersection entre l7 et l4</Intersection>
<Intersection name="I7" n="29" color="1" hidden="super" showname="true" large="true" first="l6" second="l5" shape="circle">Intersection entre l6 et l5</Intersection>
<Function name="f8" n="30" color="1" type="thick" showname="true" target="true" large="true" x="if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),x(I5)+(t/2)*(x(P9)-x(I5)),x(I6)+(t/2)*(x(P9)-x(I5))),if(floor((t-1)/4)==(t-1)/4,x(I5)+((t+1)/2)*(x(P9)-x(I5)),x(I6)+((t+1)/2)*(x(P9)-x(I5))))" y="if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),y(I5)+(t/2)*(y(P9)-y(I5)),y(I6)+(t/2)*(y(P9)-y(I5))),if(floor((t-1)/4)==(t-1)/4,y(I5)+((t+1)/2)*(y(P9)-y(I5)),y(I6)+((t+1)/2)*(y(P9)-y(I5))))" var="t" min="0" max="floor(((y(I7)-y(I5))/(y(P9)-y(I5)))*2)-1" d="1" shape="cross">Fonction ( if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),x(I5)+(t/2)*(x(P9)-x(I5)),x(I6)+(t/2)*(x(P9)-x(I5))),if(floor((t-1)/4)==(t-1)/4,x(I5)+((t+1)/2)*(x(P9)-x(I5)),x(I6)+((t+1)/2)*(x(P9)-x(I5)))) , if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),y(I5)+(t/2)*(y(P9)-y(I5)),y(I6)+(t/2)*(y(P9)-y(I5))),if(floor((t-1)/4)==(t-1)/4,y(I5)+((t+1)/2)*(y(P9)-y(I5)),y(I6)+((t+1)/2)*(y(P9)-y(I5)))) )</Function>
<Function name="f9" n="31" color="1" type="thick" showname="true" target="true" large="true" x="if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),x(I5)+(t/2)*(x(P8)-x(I5)),x(I7)+(t/2)*(x(P8)-x(I5))),if(floor((t-1)/4)==(t-1)/4,x(I5)+((t+1)/2)*(x(P8)-x(I5)),x(I7)+((t+1)/2)*(x(P8)-x(I5))))" y="if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),y(I5)+(t/2)*(y(P8)-y(I5)),y(I7)+(t/2)*(y(P8)-y(I5))),if(floor((t-1)/4)==(t-1)/4,y(I5)+((t+1)/2)*(y(P8)-y(I5)),y(I7)+((t+1)/2)*(y(P8)-y(I5))))" var="t" min="0" max="floor(((x(I6)-x(I5))/(x(P8)-x(I5)))*2)-1" d="1" shape="cross">Fonction ( if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),x(I5)+(t/2)*(x(P8)-x(I5)),x(I7)+(t/2)*(x(P8)-x(I5))),if(floor((t-1)/4)==(t-1)/4,x(I5)+((t+1)/2)*(x(P8)-x(I5)),x(I7)+((t+1)/2)*(x(P8)-x(I5)))) , if(floor(t/2)==(t/2),if(floor(t/4)==(t/4),y(I5)+(t/2)*(y(P8)-y(I5)),y(I7)+(t/2)*(y(P8)-y(I5))),if(floor((t-1)/4)==(t-1)/4,y(I5)+((t+1)/2)*(y(P8)-y(I5)),y(I7)+((t+1)/2)*(y(P8)-y(I5)))) )</Function>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Fonctions et tangentes/Tangente mobile">
<Parameter name="A">A</Parameter>
<Comment>
<P>Se reporter à l&apos;article de CaRzine &quot;Interpolation
polynomiale de degré 3&quot;.   </P>
</Comment>
<Objects>
<Point name="A" n="0" mainparameter="true" x="-3.382663847780129" y="1.1839323467230445">Point à -3.38266, 1.18393 </Point>
<Circle name="c1" n="1" hidden="super" bold="true" large="true" fixed="2" midpoint="A" acute="true">Cercle de centre A de rayon 2.0</Circle>
<PointOn name="po2" n="2" target="true" bold="true" large="true" ctag0="blue" cexpr0="1" ctag1="thick" cexpr1="1" on="c1" alpha="2.356194490192345" x="-4.796877410153224" y="2.5981459090961394" shape="circle">Point sur c1 </PointOn>
<Point name="P3" n="3" hidden="super" bold="true" large="true" x="2*x(A)-x(po2)" actx="-1.9684502854070347" y="2*y(A)-y(po2)" acty="-0.23028121565005044" fixed="true">Point à &quot;2*x(A)-x(po2)&quot;, &quot;2*y(A)-y(po2)&quot; </Point>
<Segment name="s1" n="4" target="true" bold="true" large="true" ctag0="blue" cexpr0="1" ctag1="thin" cexpr1="1" from="po2" to="P3">Segment de po2 à P3</Segment>
<Expression name="E1" n="5" color="1" type="thick" showname="true" showvalue="true" bold="true" large="true" ctag0="blue" cexpr0="1" ctag1="thin" cexpr1="1" x="x(po2)+0.1" y="y(po2)+0.1" value="round((y(po2)-y(A))/(x(po2)-x(A))*100)/100" prompt="Valeur" fixed="true">Expression &quot;round((y(po2)-y(A))/(x(po2)-x(A))*100)/100&quot; à -5.81684, 3.81811 </Expression>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Fonctions et tangentes/2 pts - 2 tangentes">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="dA">dA</Parameter>
<Parameter name="dB">dB</Parameter>
<Comment>
<P>Se reporter à l&apos;article de CaRzine &quot;Interpolation
polynomiale de degré 3&quot;.   </P>
</Comment>
<Objects>
<Expression name="dA" n="0" color="1" type="thick" showname="true" showvalue="true" parameter="true" mainparameter="true" x="-7.120507399577171" y="4.279069767441865" value="1" prompt="dA">Expression &quot;1&quot; à -7.12051, 4.27907 </Expression>
<Expression name="dB" n="1" color="1" type="thick" showname="true" showvalue="true" parameter="true" mainparameter="true" x="-7.137420718816069" y="3.856236786469345" value="2" prompt="dB">Expression &quot;2&quot; à -7.13742, 3.85624 </Expression>
<Point name="A" n="2" parameter="true" mainparameter="true" x="-2.503171247357295" y="1.8435517970401698">Point à -2.50317, 1.84355 </Point>
<Point name="B" n="3" parameter="true" mainparameter="true" x="0.3890063424947154" y="1.4545454545454541">Point à 0.38901, 1.45455 </Point>
<Expression name="E3" n="4" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(A)-x(B))^3" prompt="sdet">Expression &quot;-(x(A)-x(B))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E4" n="5" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(A)-x(A)*dA-x(A)*dB+x(B)*dA+x(B)*dB+-2*y(B)))/E3" prompt="a">Expression &quot;((2*y(A)-x(A)*dA-x(A)*dB+x(B)*dA+x(B)*dB+-2*y(B)))/E3&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E5" n="6" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((dA*(x(A))^2+dA*x(A)*x(B)+-2*dA*(x(B))^2+2*(x(A))^2*dB-x(A)*dB*x(B)+-3*x(A)*y(A)+3*x(A)*y(B)-dB*(x(B))^2+-3*y(A)*x(B)+3*x(B)*y(B)))/E3" prompt="b">Expression &quot;((dA*(x(A))^2+dA*x(A)*x(B)+-2*dA*(x(B))^2+2*(x(A))^2*dB-x(A)*dB*x(B)+-3*x(A)*y(A)+3*x(A)*y(B)-dB*(x(B))^2+-3*y(A)*x(B)+3*x(B)*y(B)))/E3&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E6" n="7" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(dB*(x(A))^3+dB*(x(A))^2*x(B)+-2*dB*x(A)*(x(B))^2+2*(x(A))^2*x(B)*dA-x(A)*(x(B))^2*dA+-6*x(A)*x(B)*y(A)+6*x(A)*x(B)*y(B)-(x(B))^3*dA))/E3" prompt="c">Expression &quot;((-1)*(dB*(x(A))^3+dB*(x(A))^2*x(B)+-2*dB*x(A)*(x(B))^2+2*(x(A))^2*x(B)*dA-x(A)*(x(B))^2*dA+-6*x(A)*x(B)*y(A)+6*x(A)*x(B)*y(B)-(x(B))^3*dA))/E3&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E7" n="8" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-dB*(x(B))^2*(x(A))^2+dB*x(B)*(x(A))^3-(x(B))^3*x(A)*dA+(x(B))^3*y(A)+(x(B))^2*(x(A))^2*dA-3*(x(B))^2*x(A)*y(A)--3*x(B)*(x(A))^2*y(B)-(x(A))^3*y(B)))/E3" prompt="d">Expression &quot;((-dB*(x(B))^2*(x(A))^2+dB*x(B)*(x(A))^3-(x(B))^3*x(A)*dA+(x(B))^3*y(A)+(x(B))^2*(x(A))^2*dA-3*(x(B))^2*x(A)*y(A)--3*x(B)*(x(A))^2*y(B)-(x(A))^3*y(B)))/E3&quot; à -7.13742, 1.43763 </Expression>
<Function name="f1" n="9" color="2" type="thick" showname="true" target="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E4*x^3+E5*x^2+E6*x+E7" var="x" min="x(A)" max="x(B)" d="0.1" shape="cross">Fonction ( x , E4*x^3+E5*x^2+E6*x+E7 )</Function>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Fonctions et tangentes/3 pts - 3 tangentes" showduplicates="true">
<Parameter name="A">A</Parameter>
<Parameter name="B">B</Parameter>
<Parameter name="C">C</Parameter>
<Parameter name="E1">E1</Parameter>
<Parameter name="E2">E2</Parameter>
<Parameter name="E3">E3</Parameter>
<Comment>
<P>Se reporter à l&apos;article de CaRzine &quot;Interpolation
polynomiale de degré 3&quot;.   </P>
</Comment>
<Objects>
<Point name="A" n="0" parameter="true" mainparameter="true" x="-5.937677053824364" y="1.359773371104816">Point à -5.93768, 1.35977 </Point>
<Expression name="E1" n="1" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Point name="B" n="2" parameter="true" mainparameter="true" x="-2.152974504249292" y="1.4730878186968837">Point à -2.15297, 1.47309 </Point>
<Expression name="E2" n="3" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Point name="C" n="4" parameter="true" mainparameter="true" x="1.269121813031161" y="1.314447592067989">Point à 1.26912, 1.31445 </Point>
<Expression name="E3" n="5" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Expression name="E4" n="6" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(A)-x(B))^3" prompt="sdet">Expression &quot;-(x(A)-x(B))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E5" n="7" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(A)-x(A)*E1-x(A)*E2+x(B)*E1+x(B)*E2+-2*y(B)))/E4" prompt="a">Expression &quot;((2*y(A)-x(A)*E1-x(A)*E2+x(B)*E1+x(B)*E2+-2*y(B)))/E4&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E6" n="8" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((E1*(x(A))^2+E1*x(A)*x(B)+-2*E1*(x(B))^2+2*(x(A))^2*E2-x(A)*E2*x(B)+-3*x(A)*y(A)+3*x(A)*y(B)-E2*(x(B))^2+-3*y(A)*x(B)+3*x(B)*y(B)))/E4" prompt="b">Expression &quot;((E1*(x(A))^2+E1*x(A)*x(B)+-2*E1*(x(B))^2+2*(x(A))^2*E2-x(A)*E2*x(B)+-3*x(A)*y(A)+3*x(A)*y(B)-E2*(x(B))^2+-3*y(A)*x(B)+3*x(B)*y(B)))/E4&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E7" n="9" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(E2*(x(A))^3+E2*(x(A))^2*x(B)+-2*E2*x(A)*(x(B))^2+2*(x(A))^2*x(B)*E1-x(A)*(x(B))^2*E1+-6*x(A)*x(B)*y(A)+6*x(A)*x(B)*y(B)-(x(B))^3*E1))/E4" prompt="c">Expression &quot;((-1)*(E2*(x(A))^3+E2*(x(A))^2*x(B)+-2*E2*x(A)*(x(B))^2+2*(x(A))^2*x(B)*E1-x(A)*(x(B))^2*E1+-6*x(A)*x(B)*y(A)+6*x(A)*x(B)*y(B)-(x(B))^3*E1))/E4&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E8" n="10" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-E2*(x(B))^2*(x(A))^2+E2*x(B)*(x(A))^3-(x(B))^3*x(A)*E1+(x(B))^3*y(A)+(x(B))^2*(x(A))^2*E1-3*(x(B))^2*x(A)*y(A)--3*x(B)*(x(A))^2*y(B)-(x(A))^3*y(B)))/E4" prompt="d">Expression &quot;((-E2*(x(B))^2*(x(A))^2+E2*x(B)*(x(A))^3-(x(B))^3*x(A)*E1+(x(B))^3*y(A)+(x(B))^2*(x(A))^2*E1-3*(x(B))^2*x(A)*y(A)--3*x(B)*(x(A))^2*y(B)-(x(A))^3*y(B)))/E4&quot; à -7.13742, 1.43763 </Expression>
<Function name="f1" n="11" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E5*x^3+E6*x^2+E7*x+E8" var="x" min="x(A)" max="x(B)" d="0.1" shape="cross">Fonction ( x , E5*x^3+E6*x^2+E7*x+E8 )</Function>
<Expression name="E9" n="12" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(B)-x(C))^3" prompt="sdet">Expression &quot;-(x(B)-x(C))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E10" n="13" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(B)-x(B)*E2-x(B)*E3+x(C)*E2+x(C)*E3+-2*y(C)))/E9" prompt="a">Expression &quot;((2*y(B)-x(B)*E2-x(B)*E3+x(C)*E2+x(C)*E3+-2*y(C)))/E9&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E11" n="14" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((E2*(x(B))^2+E2*x(B)*x(C)+-2*E2*(x(C))^2+2*(x(B))^2*E3-x(B)*E3*x(C)+-3*x(B)*y(B)+3*x(B)*y(C)-E3*(x(C))^2+-3*y(B)*x(C)+3*x(C)*y(C)))/E9" prompt="b">Expression &quot;((E2*(x(B))^2+E2*x(B)*x(C)+-2*E2*(x(C))^2+2*(x(B))^2*E3-x(B)*E3*x(C)+-3*x(B)*y(B)+3*x(B)*y(C)-E3*(x(C))^2+-3*y(B)*x(C)+3*x(C)*y(C)))/E9&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E12" n="15" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(E3*(x(B))^3+E3*(x(B))^2*x(C)+-2*E3*x(B)*(x(C))^2+2*(x(B))^2*x(C)*E2-x(B)*(x(C))^2*E2+-6*x(B)*x(C)*y(B)+6*x(B)*x(C)*y(C)-(x(C))^3*E2))/E9" prompt="c">Expression &quot;((-1)*(E3*(x(B))^3+E3*(x(B))^2*x(C)+-2*E3*x(B)*(x(C))^2+2*(x(B))^2*x(C)*E2-x(B)*(x(C))^2*E2+-6*x(B)*x(C)*y(B)+6*x(B)*x(C)*y(C)-(x(C))^3*E2))/E9&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E13" n="16" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-E3*(x(C))^2*(x(B))^2+E3*x(C)*(x(B))^3-(x(C))^3*x(B)*E2+(x(C))^3*y(B)+(x(C))^2*(x(B))^2*E2-3*(x(C))^2*x(B)*y(B)--3*x(C)*(x(B))^2*y(C)-(x(B))^3*y(C)))/E9" prompt="d">Expression &quot;((-E3*(x(C))^2*(x(B))^2+E3*x(C)*(x(B))^3-(x(C))^3*x(B)*E2+(x(C))^3*y(B)+(x(C))^2*(x(B))^2*E2-3*(x(C))^2*x(B)*y(B)--3*x(C)*(x(B))^2*y(C)-(x(B))^3*y(C)))/E9&quot; à -7.13742, 1.43763 </Expression>
<Function name="f2" n="17" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E10*x^3+E11*x^2+E12*x+E13" var="x" min="x(B)" max="x(C)" d="0.1" shape="cross">Fonction ( x , E10*x^3+E11*x^2+E12*x+E13 )</Function>
<Function name="f3" n="18" color="2" type="thick" showname="true" target="true" bold="true" large="true" x="x" y="if(x&lt;x(B),f1(x),f2(x))" var="x" min="x(A)" max="x(C)" d="0.1" shape="cross">Fonction ( x , if(x&lt;x(B),f1(x),f2(x)) )</Function>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Fonctions et tangentes/4 pts - 4 tangentes" showduplicates="true">
<Parameter name="D">D</Parameter>
<Parameter name="E">E</Parameter>
<Parameter name="F">F</Parameter>
<Parameter name="G">G</Parameter>
<Parameter name="E34">E34</Parameter>
<Parameter name="E35">E35</Parameter>
<Parameter name="E36">E36</Parameter>
<Parameter name="E37">E37</Parameter>
<Comment>
<P>Se reporter à l&apos;article de CaRzine &quot;Interpolation
polynomiale de degré 3&quot;.   </P>
</Comment>
<Objects>
<Point name="D" n="0" parameter="true" mainparameter="true" x="-5.756373937677054" y="-3.7235127478753562">Point à -5.75637, -3.72351 </Point>
<Expression name="E34" n="1" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Point name="E" n="2" parameter="true" mainparameter="true" x="-2.810198300283288" y="-3.451558073654393">Point à -2.8102, -3.45156 </Point>
<Expression name="E35" n="3" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Point name="F" n="4" parameter="true" mainparameter="true" x="0.6118980169971666" y="-3.270254957507082">Point à 0.6119, -3.27025 </Point>
<Expression name="E36" n="5" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Point name="G" n="6" parameter="true" mainparameter="true" x="4.215297450424929" y="-3.292917847025495">Point à 4.2153, -3.29292 </Point>
<Expression name="E37" n="7" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Expression name="E5" n="8" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(D)-x(E))^3" prompt="sdet">Expression &quot;-(x(D)-x(E))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E6" n="9" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(D)-x(D)*E34-x(D)*E35+x(E)*E34+x(E)*E35+-2*y(E)))/E5" prompt="a">Expression &quot;((2*y(D)-x(D)*E34-x(D)*E35+x(E)*E34+x(E)*E35+-2*y(E)))/E5&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E7" n="10" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((E34*(x(D))^2+E34*x(D)*x(E)+-2*E34*(x(E))^2+2*(x(D))^2*E35-x(D)*E35*x(E)+-3*x(D)*y(D)+3*x(D)*y(E)-E35*(x(E))^2+-3*y(D)*x(E)+3*x(E)*y(E)))/E5" prompt="b">Expression &quot;((E34*(x(D))^2+E34*x(D)*x(E)+-2*E34*(x(E))^2+2*(x(D))^2*E35-x(D)*E35*x(E)+-3*x(D)*y(D)+3*x(D)*y(E)-E35*(x(E))^2+-3*y(D)*x(E)+3*x(E)*y(E)))/E5&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E8" n="11" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(E35*(x(D))^3+E35*(x(D))^2*x(E)+-2*E35*x(D)*(x(E))^2+2*(x(D))^2*x(E)*E34-x(D)*(x(E))^2*E34+-6*x(D)*x(E)*y(D)+6*x(D)*x(E)*y(E)-(x(E))^3*E34))/E5" prompt="c">Expression &quot;((-1)*(E35*(x(D))^3+E35*(x(D))^2*x(E)+-2*E35*x(D)*(x(E))^2+2*(x(D))^2*x(E)*E34-x(D)*(x(E))^2*E34+-6*x(D)*x(E)*y(D)+6*x(D)*x(E)*y(E)-(x(E))^3*E34))/E5&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E9" n="12" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-E35*(x(E))^2*(x(D))^2+E35*x(E)*(x(D))^3-(x(E))^3*x(D)*E34+(x(E))^3*y(D)+(x(E))^2*(x(D))^2*E34-3*(x(E))^2*x(D)*y(D)--3*x(E)*(x(D))^2*y(E)-(x(D))^3*y(E)))/E5" prompt="d">Expression &quot;((-E35*(x(E))^2*(x(D))^2+E35*x(E)*(x(D))^3-(x(E))^3*x(D)*E34+(x(E))^3*y(D)+(x(E))^2*(x(D))^2*E34-3*(x(E))^2*x(D)*y(D)--3*x(E)*(x(D))^2*y(E)-(x(D))^3*y(E)))/E5&quot; à -7.13742, 1.43763 </Expression>
<Function name="f1" n="13" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E6*x^3+E7*x^2+E8*x+E9" var="x" min="x(D)" max="x(E)" d="0.1" shape="cross">Fonction ( x , E6*x^3+E7*x^2+E8*x+E9 )</Function>
<Expression name="E10" n="14" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(E)-x(F))^3" prompt="sdet">Expression &quot;-(x(E)-x(F))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E11" n="15" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(E)-x(E)*E35-x(E)*E36+x(F)*E35+x(F)*E36+-2*y(F)))/E10" prompt="a">Expression &quot;((2*y(E)-x(E)*E35-x(E)*E36+x(F)*E35+x(F)*E36+-2*y(F)))/E10&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E12" n="16" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((E35*(x(E))^2+E35*x(E)*x(F)+-2*E35*(x(F))^2+2*(x(E))^2*E36-x(E)*E36*x(F)+-3*x(E)*y(E)+3*x(E)*y(F)-E36*(x(F))^2+-3*y(E)*x(F)+3*x(F)*y(F)))/E10" prompt="b">Expression &quot;((E35*(x(E))^2+E35*x(E)*x(F)+-2*E35*(x(F))^2+2*(x(E))^2*E36-x(E)*E36*x(F)+-3*x(E)*y(E)+3*x(E)*y(F)-E36*(x(F))^2+-3*y(E)*x(F)+3*x(F)*y(F)))/E10&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E13" n="17" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(E36*(x(E))^3+E36*(x(E))^2*x(F)+-2*E36*x(E)*(x(F))^2+2*(x(E))^2*x(F)*E35-x(E)*(x(F))^2*E35+-6*x(E)*x(F)*y(E)+6*x(E)*x(F)*y(F)-(x(F))^3*E35))/E10" prompt="c">Expression &quot;((-1)*(E36*(x(E))^3+E36*(x(E))^2*x(F)+-2*E36*x(E)*(x(F))^2+2*(x(E))^2*x(F)*E35-x(E)*(x(F))^2*E35+-6*x(E)*x(F)*y(E)+6*x(E)*x(F)*y(F)-(x(F))^3*E35))/E10&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E14" n="18" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-E36*(x(F))^2*(x(E))^2+E36*x(F)*(x(E))^3-(x(F))^3*x(E)*E35+(x(F))^3*y(E)+(x(F))^2*(x(E))^2*E35-3*(x(F))^2*x(E)*y(E)--3*x(F)*(x(E))^2*y(F)-(x(E))^3*y(F)))/E10" prompt="d">Expression &quot;((-E36*(x(F))^2*(x(E))^2+E36*x(F)*(x(E))^3-(x(F))^3*x(E)*E35+(x(F))^3*y(E)+(x(F))^2*(x(E))^2*E35-3*(x(F))^2*x(E)*y(E)--3*x(F)*(x(E))^2*y(F)-(x(E))^3*y(F)))/E10&quot; à -7.13742, 1.43763 </Expression>
<Function name="f2" n="19" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E11*x^3+E12*x^2+E13*x+E14" var="x" min="x(E)" max="x(F)" d="0.1" shape="cross">Fonction ( x , E11*x^3+E12*x^2+E13*x+E14 )</Function>
<Expression name="E15" n="20" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(F)-x(G))^3" prompt="sdet">Expression &quot;-(x(F)-x(G))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E16" n="21" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(F)-x(F)*E36-x(F)*E37+x(G)*E36+x(G)*E37+-2*y(G)))/E15" prompt="a">Expression &quot;((2*y(F)-x(F)*E36-x(F)*E37+x(G)*E36+x(G)*E37+-2*y(G)))/E15&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E17" n="22" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((E36*(x(F))^2+E36*x(F)*x(G)+-2*E36*(x(G))^2+2*(x(F))^2*E37-x(F)*E37*x(G)+-3*x(F)*y(F)+3*x(F)*y(G)-E37*(x(G))^2+-3*y(F)*x(G)+3*x(G)*y(G)))/E15" prompt="b">Expression &quot;((E36*(x(F))^2+E36*x(F)*x(G)+-2*E36*(x(G))^2+2*(x(F))^2*E37-x(F)*E37*x(G)+-3*x(F)*y(F)+3*x(F)*y(G)-E37*(x(G))^2+-3*y(F)*x(G)+3*x(G)*y(G)))/E15&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E18" n="23" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(E37*(x(F))^3+E37*(x(F))^2*x(G)+-2*E37*x(F)*(x(G))^2+2*(x(F))^2*x(G)*E36-x(F)*(x(G))^2*E36+-6*x(F)*x(G)*y(F)+6*x(F)*x(G)*y(G)-(x(G))^3*E36))/E15" prompt="c">Expression &quot;((-1)*(E37*(x(F))^3+E37*(x(F))^2*x(G)+-2*E37*x(F)*(x(G))^2+2*(x(F))^2*x(G)*E36-x(F)*(x(G))^2*E36+-6*x(F)*x(G)*y(F)+6*x(F)*x(G)*y(G)-(x(G))^3*E36))/E15&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E19" n="24" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-E37*(x(G))^2*(x(F))^2+E37*x(G)*(x(F))^3-(x(G))^3*x(F)*E36+(x(G))^3*y(F)+(x(G))^2*(x(F))^2*E36-3*(x(G))^2*x(F)*y(F)--3*x(G)*(x(F))^2*y(G)-(x(F))^3*y(G)))/E15" prompt="d">Expression &quot;((-E37*(x(G))^2*(x(F))^2+E37*x(G)*(x(F))^3-(x(G))^3*x(F)*E36+(x(G))^3*y(F)+(x(G))^2*(x(F))^2*E36-3*(x(G))^2*x(F)*y(F)--3*x(G)*(x(F))^2*y(G)-(x(F))^3*y(G)))/E15&quot; à -7.13742, 1.43763 </Expression>
<Function name="f3" n="25" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E16*x^3+E17*x^2+E18*x+E19" var="x" min="x(F)" max="x(G)" d="0.1" shape="cross">Fonction ( x , E16*x^3+E17*x^2+E18*x+E19 )</Function>
<Function name="f4" n="26" color="2" type="thick" showname="true" target="true" bold="true" large="true" x="x" y="if(x&lt;x(E),f1(x),if(x&lt;x(F),f2(x),f3(x)))" var="x" min="x(D)" max="x(G)" d="0.1" shape="cross">Fonction ( x , if(x&lt;x(E),f1(x),if(x&lt;x(F),f2(x),f3(x))) )</Function>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Fonctions et tangentes/5 pts - 5 tangentes" showduplicates="true">
<Parameter name="D">D</Parameter>
<Parameter name="E">E</Parameter>
<Parameter name="F">F</Parameter>
<Parameter name="G">G</Parameter>
<Parameter name="H">H</Parameter>
<Parameter name="E34">E34</Parameter>
<Parameter name="E35">E35</Parameter>
<Parameter name="E36">E36</Parameter>
<Parameter name="E37">E37</Parameter>
<Parameter name="E33">E33</Parameter>
<Comment>
<P>Se reporter à l&apos;article de CaRzine &quot;Interpolation
polynomiale de degré 3&quot;.   </P>
</Comment>
<Objects>
<Point name="D" n="0" parameter="true" mainparameter="true" x="-5.756373937677054" y="-3.7235127478753562">Point à -5.75637, -3.72351 </Point>
<Point name="E" n="1" parameter="true" mainparameter="true" x="-4.736540000000002" y="-3.65552">Point à -4.73654, -3.65552 </Point>
<Point name="F" n="2" parameter="true" mainparameter="true" x="-3.6487300000000022" y="-3.7008500000000004">Point à -3.64873, -3.70085 </Point>
<Point name="G" n="3" parameter="true" mainparameter="true" x="-2.6289000000000002" y="-3.7008500000000004">Point à -2.6289, -3.70085 </Point>
<Expression name="E1" n="4" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(D)-x(E))^3" prompt="sdet">Expression &quot;-(x(D)-x(E))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E2" n="5" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(E)-x(F))^3" prompt="sdet">Expression &quot;-(x(E)-x(F))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E3" n="6" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(F)-x(G))^3" prompt="sdet">Expression &quot;-(x(F)-x(G))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E34" n="7" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Expression name="E35" n="8" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Expression name="E36" n="9" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Expression name="E37" n="10" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Expression name="E8" n="11" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(D)-x(D)*E34-x(D)*E35+x(E)*E34+x(E)*E35+-2*y(E)))/E1" prompt="a">Expression &quot;((2*y(D)-x(D)*E34-x(D)*E35+x(E)*E34+x(E)*E35+-2*y(E)))/E1&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E9" n="12" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((E34*(x(D))^2+E34*x(D)*x(E)+-2*E34*(x(E))^2+2*(x(D))^2*E35-x(D)*E35*x(E)+-3*x(D)*y(D)+3*x(D)*y(E)-E35*(x(E))^2+-3*y(D)*x(E)+3*x(E)*y(E)))/E1" prompt="b">Expression &quot;((E34*(x(D))^2+E34*x(D)*x(E)+-2*E34*(x(E))^2+2*(x(D))^2*E35-x(D)*E35*x(E)+-3*x(D)*y(D)+3*x(D)*y(E)-E35*(x(E))^2+-3*y(D)*x(E)+3*x(E)*y(E)))/E1&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E10" n="13" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(E35*(x(D))^3+E35*(x(D))^2*x(E)+-2*E35*x(D)*(x(E))^2+2*(x(D))^2*x(E)*E34-x(D)*(x(E))^2*E34+-6*x(D)*x(E)*y(D)+6*x(D)*x(E)*y(E)-(x(E))^3*E34))/E1" prompt="c">Expression &quot;((-1)*(E35*(x(D))^3+E35*(x(D))^2*x(E)+-2*E35*x(D)*(x(E))^2+2*(x(D))^2*x(E)*E34-x(D)*(x(E))^2*E34+-6*x(D)*x(E)*y(D)+6*x(D)*x(E)*y(E)-(x(E))^3*E34))/E1&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E11" n="14" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-E35*(x(E))^2*(x(D))^2+E35*x(E)*(x(D))^3-(x(E))^3*x(D)*E34+(x(E))^3*y(D)+(x(E))^2*(x(D))^2*E34-3*(x(E))^2*x(D)*y(D)--3*x(E)*(x(D))^2*y(E)-(x(D))^3*y(E)))/E1" prompt="d">Expression &quot;((-E35*(x(E))^2*(x(D))^2+E35*x(E)*(x(D))^3-(x(E))^3*x(D)*E34+(x(E))^3*y(D)+(x(E))^2*(x(D))^2*E34-3*(x(E))^2*x(D)*y(D)--3*x(E)*(x(D))^2*y(E)-(x(D))^3*y(E)))/E1&quot; à -7.13742, 1.43763 </Expression>
<Expression name="E12" n="15" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(E)-x(E)*E35-x(E)*E36+x(F)*E35+x(F)*E36+-2*y(F)))/E2" prompt="a">Expression &quot;((2*y(E)-x(E)*E35-x(E)*E36+x(F)*E35+x(F)*E36+-2*y(F)))/E2&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E13" n="16" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((E35*(x(E))^2+E35*x(E)*x(F)+-2*E35*(x(F))^2+2*(x(E))^2*E36-x(E)*E36*x(F)+-3*x(E)*y(E)+3*x(E)*y(F)-E36*(x(F))^2+-3*y(E)*x(F)+3*x(F)*y(F)))/E2" prompt="b">Expression &quot;((E35*(x(E))^2+E35*x(E)*x(F)+-2*E35*(x(F))^2+2*(x(E))^2*E36-x(E)*E36*x(F)+-3*x(E)*y(E)+3*x(E)*y(F)-E36*(x(F))^2+-3*y(E)*x(F)+3*x(F)*y(F)))/E2&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E14" n="17" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(E36*(x(E))^3+E36*(x(E))^2*x(F)+-2*E36*x(E)*(x(F))^2+2*(x(E))^2*x(F)*E35-x(E)*(x(F))^2*E35+-6*x(E)*x(F)*y(E)+6*x(E)*x(F)*y(F)-(x(F))^3*E35))/E2" prompt="c">Expression &quot;((-1)*(E36*(x(E))^3+E36*(x(E))^2*x(F)+-2*E36*x(E)*(x(F))^2+2*(x(E))^2*x(F)*E35-x(E)*(x(F))^2*E35+-6*x(E)*x(F)*y(E)+6*x(E)*x(F)*y(F)-(x(F))^3*E35))/E2&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E15" n="18" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-E36*(x(F))^2*(x(E))^2+E36*x(F)*(x(E))^3-(x(F))^3*x(E)*E35+(x(F))^3*y(E)+(x(F))^2*(x(E))^2*E35-3*(x(F))^2*x(E)*y(E)--3*x(F)*(x(E))^2*y(F)-(x(E))^3*y(F)))/E2" prompt="d">Expression &quot;((-E36*(x(F))^2*(x(E))^2+E36*x(F)*(x(E))^3-(x(F))^3*x(E)*E35+(x(F))^3*y(E)+(x(F))^2*(x(E))^2*E35-3*(x(F))^2*x(E)*y(E)--3*x(F)*(x(E))^2*y(F)-(x(E))^3*y(F)))/E2&quot; à -7.13742, 1.43763 </Expression>
<Expression name="E16" n="19" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(F)-x(F)*E36-x(F)*E37+x(G)*E36+x(G)*E37+-2*y(G)))/E3" prompt="a">Expression &quot;((2*y(F)-x(F)*E36-x(F)*E37+x(G)*E36+x(G)*E37+-2*y(G)))/E3&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E17" n="20" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((E36*(x(F))^2+E36*x(F)*x(G)+-2*E36*(x(G))^2+2*(x(F))^2*E37-x(F)*E37*x(G)+-3*x(F)*y(F)+3*x(F)*y(G)-E37*(x(G))^2+-3*y(F)*x(G)+3*x(G)*y(G)))/E3" prompt="b">Expression &quot;((E36*(x(F))^2+E36*x(F)*x(G)+-2*E36*(x(G))^2+2*(x(F))^2*E37-x(F)*E37*x(G)+-3*x(F)*y(F)+3*x(F)*y(G)-E37*(x(G))^2+-3*y(F)*x(G)+3*x(G)*y(G)))/E3&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E18" n="21" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(E37*(x(F))^3+E37*(x(F))^2*x(G)+-2*E37*x(F)*(x(G))^2+2*(x(F))^2*x(G)*E36-x(F)*(x(G))^2*E36+-6*x(F)*x(G)*y(F)+6*x(F)*x(G)*y(G)-(x(G))^3*E36))/E3" prompt="c">Expression &quot;((-1)*(E37*(x(F))^3+E37*(x(F))^2*x(G)+-2*E37*x(F)*(x(G))^2+2*(x(F))^2*x(G)*E36-x(F)*(x(G))^2*E36+-6*x(F)*x(G)*y(F)+6*x(F)*x(G)*y(G)-(x(G))^3*E36))/E3&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E19" n="22" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-E37*(x(G))^2*(x(F))^2+E37*x(G)*(x(F))^3-(x(G))^3*x(F)*E36+(x(G))^3*y(F)+(x(G))^2*(x(F))^2*E36-3*(x(G))^2*x(F)*y(F)--3*x(G)*(x(F))^2*y(G)-(x(F))^3*y(G)))/E3" prompt="d">Expression &quot;((-E37*(x(G))^2*(x(F))^2+E37*x(G)*(x(F))^3-(x(G))^3*x(F)*E36+(x(G))^3*y(F)+(x(G))^2*(x(F))^2*E36-3*(x(G))^2*x(F)*y(F)--3*x(G)*(x(F))^2*y(G)-(x(F))^3*y(G)))/E3&quot; à -7.13742, 1.43763 </Expression>
<Function name="f1" n="23" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E8*x^3+E9*x^2+E10*x+E11" var="x" min="x(D)" max="x(E)" d="0.1" shape="cross">Fonction ( x , E8*x^3+E9*x^2+E10*x+E11 )</Function>
<Function name="f2" n="24" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E12*x^3+E13*x^2+E14*x+E15" var="x" min="x(E)" max="x(F)" d="0.1" shape="cross">Fonction ( x , E12*x^3+E13*x^2+E14*x+E15 )</Function>
<Function name="f3" n="25" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E16*x^3+E17*x^2+E18*x+E19" var="x" min="x(F)" max="x(G)" d="0.1" shape="cross">Fonction ( x , E16*x^3+E17*x^2+E18*x+E19 )</Function>
<Point name="H" n="26" parameter="true" mainparameter="true" x="-1.8583569405099165" y="-3.6328611898016994">Point à -1.85836, -3.63286 </Point>
<Expression name="E33" n="27" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Expression name="E21" n="28" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(G)-x(H))^3" prompt="sdet">Expression &quot;-(x(G)-x(H))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E22" n="29" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(G)-x(G)*E37-x(G)*E33+x(H)*E37+x(H)*E33+-2*y(H)))/E21" prompt="a">Expression &quot;((2*y(G)-x(G)*E37-x(G)*E33+x(H)*E37+x(H)*E33+-2*y(H)))/E21&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E23" n="30" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((E37*(x(G))^2+E37*x(G)*x(H)+-2*E37*(x(H))^2+2*(x(G))^2*E33-x(G)*E33*x(H)+-3*x(G)*y(G)+3*x(G)*y(H)-E33*(x(H))^2+-3*y(G)*x(H)+3*x(H)*y(H)))/E21" prompt="b">Expression &quot;((E37*(x(G))^2+E37*x(G)*x(H)+-2*E37*(x(H))^2+2*(x(G))^2*E33-x(G)*E33*x(H)+-3*x(G)*y(G)+3*x(G)*y(H)-E33*(x(H))^2+-3*y(G)*x(H)+3*x(H)*y(H)))/E21&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E24" n="31" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(E33*(x(G))^3+E33*(x(G))^2*x(H)+-2*E33*x(G)*(x(H))^2+2*(x(G))^2*x(H)*E37-x(G)*(x(H))^2*E37+-6*x(G)*x(H)*y(G)+6*x(G)*x(H)*y(H)-(x(H))^3*E37))/E21" prompt="c">Expression &quot;((-1)*(E33*(x(G))^3+E33*(x(G))^2*x(H)+-2*E33*x(G)*(x(H))^2+2*(x(G))^2*x(H)*E37-x(G)*(x(H))^2*E37+-6*x(G)*x(H)*y(G)+6*x(G)*x(H)*y(H)-(x(H))^3*E37))/E21&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E25" n="32" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-E33*(x(H))^2*(x(G))^2+E33*x(H)*(x(G))^3-(x(H))^3*x(G)*E37+(x(H))^3*y(G)+(x(H))^2*(x(G))^2*E37-3*(x(H))^2*x(G)*y(G)--3*x(H)*(x(G))^2*y(H)-(x(G))^3*y(H)))/E21" prompt="d">Expression &quot;((-E33*(x(H))^2*(x(G))^2+E33*x(H)*(x(G))^3-(x(H))^3*x(G)*E37+(x(H))^3*y(G)+(x(H))^2*(x(G))^2*E37-3*(x(H))^2*x(G)*y(G)--3*x(H)*(x(G))^2*y(H)-(x(G))^3*y(H)))/E21&quot; à -7.13742, 1.43763 </Expression>
<Function name="f4" n="33" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E22*x^3+E23*x^2+E24*x+E25" var="x" min="x(G)" max="x(H)" d="0.1" shape="cross">Fonction ( x , E22*x^3+E23*x^2+E24*x+E25 )</Function>
<Function name="f5" n="34" color="2" type="thick" showname="true" target="true" bold="true" large="true" x="x" y="if(x&lt;x(E),f1(x),if(x&lt;x(F),f2(x),if(x&lt;x(G),f3(x),f4(x))))" var="x" min="x(D)" max="x(H)" d="0.1" shape="cross">Fonction ( x , if(x&lt;x(E),f1(x),if(x&lt;x(F),f2(x),if(x&lt;x(G),f3(x),f4(x)))) )</Function>
</Objects>
</Macro>
<Macro Name="Pour &quot;experts&quot;/Fonctions et tangentes/6 pts - 6 tangentes" showduplicates="true">
<Parameter name="D">D</Parameter>
<Parameter name="E">E</Parameter>
<Parameter name="F">F</Parameter>
<Parameter name="G">G</Parameter>
<Parameter name="H">H</Parameter>
<Parameter name="I">I</Parameter>
<Parameter name="E34">E34</Parameter>
<Parameter name="E35">E35</Parameter>
<Parameter name="E36">E36</Parameter>
<Parameter name="E37">E37</Parameter>
<Parameter name="E33">E33</Parameter>
<Parameter name="E58">E58</Parameter>
<Comment>
<P>Se reporter à l&apos;article de CaRzine &quot;Interpolation
polynomiale de degré 3&quot;.   </P>
</Comment>
<Objects>
<Point name="D" n="0" parameter="true" mainparameter="true" x="-5.756373937677054" y="-3.7235127478753562">Point à -5.75637, -3.72351 </Point>
<Point name="E" n="1" parameter="true" mainparameter="true" x="-4.736540000000002" y="-3.65552">Point à -4.73654, -3.65552 </Point>
<Point name="F" n="2" parameter="true" mainparameter="true" x="-3.6487300000000022" y="-3.7008500000000004">Point à -3.64873, -3.70085 </Point>
<Point name="G" n="3" parameter="true" mainparameter="true" x="-2.6289000000000002" y="-3.7008500000000004">Point à -2.6289, -3.70085 </Point>
<Expression name="E1" n="4" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(D)-x(E))^3" prompt="sdet">Expression &quot;-(x(D)-x(E))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E2" n="5" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(E)-x(F))^3" prompt="sdet">Expression &quot;-(x(E)-x(F))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E3" n="6" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(F)-x(G))^3" prompt="sdet">Expression &quot;-(x(F)-x(G))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E34" n="7" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Expression name="E35" n="8" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Expression name="E36" n="9" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Expression name="E37" n="10" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Expression name="E8" n="11" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(D)-x(D)*E34-x(D)*E35+x(E)*E34+x(E)*E35+-2*y(E)))/E1" prompt="a">Expression &quot;((2*y(D)-x(D)*E34-x(D)*E35+x(E)*E34+x(E)*E35+-2*y(E)))/E1&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E9" n="12" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((E34*(x(D))^2+E34*x(D)*x(E)+-2*E34*(x(E))^2+2*(x(D))^2*E35-x(D)*E35*x(E)+-3*x(D)*y(D)+3*x(D)*y(E)-E35*(x(E))^2+-3*y(D)*x(E)+3*x(E)*y(E)))/E1" prompt="b">Expression &quot;((E34*(x(D))^2+E34*x(D)*x(E)+-2*E34*(x(E))^2+2*(x(D))^2*E35-x(D)*E35*x(E)+-3*x(D)*y(D)+3*x(D)*y(E)-E35*(x(E))^2+-3*y(D)*x(E)+3*x(E)*y(E)))/E1&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E10" n="13" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(E35*(x(D))^3+E35*(x(D))^2*x(E)+-2*E35*x(D)*(x(E))^2+2*(x(D))^2*x(E)*E34-x(D)*(x(E))^2*E34+-6*x(D)*x(E)*y(D)+6*x(D)*x(E)*y(E)-(x(E))^3*E34))/E1" prompt="c">Expression &quot;((-1)*(E35*(x(D))^3+E35*(x(D))^2*x(E)+-2*E35*x(D)*(x(E))^2+2*(x(D))^2*x(E)*E34-x(D)*(x(E))^2*E34+-6*x(D)*x(E)*y(D)+6*x(D)*x(E)*y(E)-(x(E))^3*E34))/E1&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E11" n="14" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-E35*(x(E))^2*(x(D))^2+E35*x(E)*(x(D))^3-(x(E))^3*x(D)*E34+(x(E))^3*y(D)+(x(E))^2*(x(D))^2*E34-3*(x(E))^2*x(D)*y(D)--3*x(E)*(x(D))^2*y(E)-(x(D))^3*y(E)))/E1" prompt="d">Expression &quot;((-E35*(x(E))^2*(x(D))^2+E35*x(E)*(x(D))^3-(x(E))^3*x(D)*E34+(x(E))^3*y(D)+(x(E))^2*(x(D))^2*E34-3*(x(E))^2*x(D)*y(D)--3*x(E)*(x(D))^2*y(E)-(x(D))^3*y(E)))/E1&quot; à -7.13742, 1.43763 </Expression>
<Expression name="E12" n="15" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(E)-x(E)*E35-x(E)*E36+x(F)*E35+x(F)*E36+-2*y(F)))/E2" prompt="a">Expression &quot;((2*y(E)-x(E)*E35-x(E)*E36+x(F)*E35+x(F)*E36+-2*y(F)))/E2&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E13" n="16" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((E35*(x(E))^2+E35*x(E)*x(F)+-2*E35*(x(F))^2+2*(x(E))^2*E36-x(E)*E36*x(F)+-3*x(E)*y(E)+3*x(E)*y(F)-E36*(x(F))^2+-3*y(E)*x(F)+3*x(F)*y(F)))/E2" prompt="b">Expression &quot;((E35*(x(E))^2+E35*x(E)*x(F)+-2*E35*(x(F))^2+2*(x(E))^2*E36-x(E)*E36*x(F)+-3*x(E)*y(E)+3*x(E)*y(F)-E36*(x(F))^2+-3*y(E)*x(F)+3*x(F)*y(F)))/E2&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E14" n="17" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(E36*(x(E))^3+E36*(x(E))^2*x(F)+-2*E36*x(E)*(x(F))^2+2*(x(E))^2*x(F)*E35-x(E)*(x(F))^2*E35+-6*x(E)*x(F)*y(E)+6*x(E)*x(F)*y(F)-(x(F))^3*E35))/E2" prompt="c">Expression &quot;((-1)*(E36*(x(E))^3+E36*(x(E))^2*x(F)+-2*E36*x(E)*(x(F))^2+2*(x(E))^2*x(F)*E35-x(E)*(x(F))^2*E35+-6*x(E)*x(F)*y(E)+6*x(E)*x(F)*y(F)-(x(F))^3*E35))/E2&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E15" n="18" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-E36*(x(F))^2*(x(E))^2+E36*x(F)*(x(E))^3-(x(F))^3*x(E)*E35+(x(F))^3*y(E)+(x(F))^2*(x(E))^2*E35-3*(x(F))^2*x(E)*y(E)--3*x(F)*(x(E))^2*y(F)-(x(E))^3*y(F)))/E2" prompt="d">Expression &quot;((-E36*(x(F))^2*(x(E))^2+E36*x(F)*(x(E))^3-(x(F))^3*x(E)*E35+(x(F))^3*y(E)+(x(F))^2*(x(E))^2*E35-3*(x(F))^2*x(E)*y(E)--3*x(F)*(x(E))^2*y(F)-(x(E))^3*y(F)))/E2&quot; à -7.13742, 1.43763 </Expression>
<Expression name="E16" n="19" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(F)-x(F)*E36-x(F)*E37+x(G)*E36+x(G)*E37+-2*y(G)))/E3" prompt="a">Expression &quot;((2*y(F)-x(F)*E36-x(F)*E37+x(G)*E36+x(G)*E37+-2*y(G)))/E3&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E17" n="20" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((E36*(x(F))^2+E36*x(F)*x(G)+-2*E36*(x(G))^2+2*(x(F))^2*E37-x(F)*E37*x(G)+-3*x(F)*y(F)+3*x(F)*y(G)-E37*(x(G))^2+-3*y(F)*x(G)+3*x(G)*y(G)))/E3" prompt="b">Expression &quot;((E36*(x(F))^2+E36*x(F)*x(G)+-2*E36*(x(G))^2+2*(x(F))^2*E37-x(F)*E37*x(G)+-3*x(F)*y(F)+3*x(F)*y(G)-E37*(x(G))^2+-3*y(F)*x(G)+3*x(G)*y(G)))/E3&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E18" n="21" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(E37*(x(F))^3+E37*(x(F))^2*x(G)+-2*E37*x(F)*(x(G))^2+2*(x(F))^2*x(G)*E36-x(F)*(x(G))^2*E36+-6*x(F)*x(G)*y(F)+6*x(F)*x(G)*y(G)-(x(G))^3*E36))/E3" prompt="c">Expression &quot;((-1)*(E37*(x(F))^3+E37*(x(F))^2*x(G)+-2*E37*x(F)*(x(G))^2+2*(x(F))^2*x(G)*E36-x(F)*(x(G))^2*E36+-6*x(F)*x(G)*y(F)+6*x(F)*x(G)*y(G)-(x(G))^3*E36))/E3&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E19" n="22" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-E37*(x(G))^2*(x(F))^2+E37*x(G)*(x(F))^3-(x(G))^3*x(F)*E36+(x(G))^3*y(F)+(x(G))^2*(x(F))^2*E36-3*(x(G))^2*x(F)*y(F)--3*x(G)*(x(F))^2*y(G)-(x(F))^3*y(G)))/E3" prompt="d">Expression &quot;((-E37*(x(G))^2*(x(F))^2+E37*x(G)*(x(F))^3-(x(G))^3*x(F)*E36+(x(G))^3*y(F)+(x(G))^2*(x(F))^2*E36-3*(x(G))^2*x(F)*y(F)--3*x(G)*(x(F))^2*y(G)-(x(F))^3*y(G)))/E3&quot; à -7.13742, 1.43763 </Expression>
<Function name="f1" n="23" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E8*x^3+E9*x^2+E10*x+E11" var="x" min="x(D)" max="x(E)" d="0.1" shape="cross">Fonction ( x , E8*x^3+E9*x^2+E10*x+E11 )</Function>
<Function name="f2" n="24" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E12*x^3+E13*x^2+E14*x+E15" var="x" min="x(E)" max="x(F)" d="0.1" shape="cross">Fonction ( x , E12*x^3+E13*x^2+E14*x+E15 )</Function>
<Function name="f3" n="25" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E16*x^3+E17*x^2+E18*x+E19" var="x" min="x(F)" max="x(G)" d="0.1" shape="cross">Fonction ( x , E16*x^3+E17*x^2+E18*x+E19 )</Function>
<Point name="H" n="26" parameter="true" mainparameter="true" x="-1.8583569405099165" y="-3.6328611898016994">Point à -1.85836, -3.63286 </Point>
<Expression name="E33" n="27" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Expression name="E21" n="28" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(G)-x(H))^3" prompt="sdet">Expression &quot;-(x(G)-x(H))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E22" n="29" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(G)-x(G)*E37-x(G)*E33+x(H)*E37+x(H)*E33+-2*y(H)))/E21" prompt="a">Intersection entre l152 et c588Expression &quot;((2*y(G)-x(G)*E37-x(G)*E33+x(H)*E37+x(H)*E33+-2*y(H)))/E21&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E23" n="30" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((E37*(x(G))^2+E37*x(G)*x(H)+-2*E37*(x(H))^2+2*(x(G))^2*E33-x(G)*E33*x(H)+-3*x(G)*y(G)+3*x(G)*y(H)-E33*(x(H))^2+-3*y(G)*x(H)+3*x(H)*y(H)))/E21" prompt="b">Expression &quot;((E37*(x(G))^2+E37*x(G)*x(H)+-2*E37*(x(H))^2+2*(x(G))^2*E33-x(G)*E33*x(H)+-3*x(G)*y(G)+3*x(G)*y(H)-E33*(x(H))^2+-3*y(G)*x(H)+3*x(H)*y(H)))/E21&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E24" n="31" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(E33*(x(G))^3+E33*(x(G))^2*x(H)+-2*E33*x(G)*(x(H))^2+2*(x(G))^2*x(H)*E37-x(G)*(x(H))^2*E37+-6*x(G)*x(H)*y(G)+6*x(G)*x(H)*y(H)-(x(H))^3*E37))/E21" prompt="c">Expression &quot;((-1)*(E33*(x(G))^3+E33*(x(G))^2*x(H)+-2*E33*x(G)*(x(H))^2+2*(x(G))^2*x(H)*E37-x(G)*(x(H))^2*E37+-6*x(G)*x(H)*y(G)+6*x(G)*x(H)*y(H)-(x(H))^3*E37))/E21&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E25" n="32" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-E33*(x(H))^2*(x(G))^2+E33*x(H)*(x(G))^3-(x(H))^3*x(G)*E37+(x(H))^3*y(G)+(x(H))^2*(x(G))^2*E37-3*(x(H))^2*x(G)*y(G)--3*x(H)*(x(G))^2*y(H)-(x(G))^3*y(H)))/E21" prompt="d">Expression &quot;((-E33*(x(H))^2*(x(G))^2+E33*x(H)*(x(G))^3-(x(H))^3*x(G)*E37+(x(H))^3*y(G)+(x(H))^2*(x(G))^2*E37-3*(x(H))^2*x(G)*y(G)--3*x(H)*(x(G))^2*y(H)-(x(G))^3*y(H)))/E21&quot; à -7.13742, 1.43763 </Expression>
<Function name="f4" n="33" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E22*x^3+E23*x^2+E24*x+E25" var="x" min="x(G)" max="x(H)" d="0.1" shape="cross">Fonction ( x , E22*x^3+E23*x^2+E24*x+E25 )</Function>
<Point name="I" n="34" parameter="true" mainparameter="true" x="-0.7932011331444759" y="-3.6101983002832907">Point à -0.7932, -3.6102 </Point>
<Expression name="E58" n="35" color="1" type="thick" showname="true" parameter="true" mainparameter="true" x="0.0" y="0.0" value="0" prompt="Valeur">Expression &quot;0&quot; à 0.0, 0.0 </Expression>
<Expression name="E27" n="36" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="3.3995771670190282" value="-(x(H)-x(I))^3" prompt="sdet">Expression &quot;-(x(H)-x(I))^3&quot; à -7.13742, 3.39958 </Expression>
<Expression name="E28" n="37" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.959830866807611" value="((2*y(H)-x(H)*E33-x(H)*E58+x(I)*E33+x(I)*E58+-2*y(I)))/E27" prompt="a">Expression &quot;((2*y(H)-x(H)*E33-x(H)*E58+x(I)*E33+x(I)*E58+-2*y(I)))/E27&quot; à -7.13742, 2.95983 </Expression>
<Expression name="E29" n="38" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="2.452431289640593" value="((E33*(x(H))^2+E33*x(H)*x(I)+-2*E33*(x(I))^2+2*(x(H))^2*E58-x(H)*E58*x(I)+-3*x(H)*y(H)+3*x(H)*y(I)-E58*(x(I))^2+-3*y(H)*x(I)+3*x(I)*y(I)))/E27" prompt="b">Expression &quot;((E33*(x(H))^2+E33*x(H)*x(I)+-2*E33*(x(I))^2+2*(x(H))^2*E58-x(H)*E58*x(I)+-3*x(H)*y(H)+3*x(H)*y(I)-E58*(x(I))^2+-3*y(H)*x(I)+3*x(I)*y(I)))/E27&quot; à -7.13742, 2.45243 </Expression>
<Expression name="E30" n="39" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.9619450317124738" value="((-1)*(E58*(x(H))^3+E58*(x(H))^2*x(I)+-2*E58*x(H)*(x(I))^2+2*(x(H))^2*x(I)*E33-x(H)*(x(I))^2*E33+-6*x(H)*x(I)*y(H)+6*x(H)*x(I)*y(I)-(x(I))^3*E33))/E27" prompt="c">Expression &quot;((-1)*(E58*(x(H))^3+E58*(x(H))^2*x(I)+-2*E58*x(H)*(x(I))^2+2*(x(H))^2*x(I)*E33-x(H)*(x(I))^2*E33+-6*x(H)*x(I)*y(H)+6*x(H)*x(I)*y(I)-(x(I))^3*E33))/E27&quot; à -7.13742, 1.96195 </Expression>
<Expression name="E31" n="40" color="1" type="thick" hidden="super" showname="true" showvalue="true" x="-7.137420718816069" y="1.4376321353065546" value="((-E58*(x(I))^2*(x(H))^2+E58*x(I)*(x(H))^3-(x(I))^3*x(H)*E33+(x(I))^3*y(H)+(x(I))^2*(x(H))^2*E33-3*(x(I))^2*x(H)*y(H)--3*x(I)*(x(H))^2*y(I)-(x(H))^3*y(I)))/E27" prompt="d">Expression &quot;((-E58*(x(I))^2*(x(H))^2+E58*x(I)*(x(H))^3-(x(I))^3*x(H)*E33+(x(I))^3*y(H)+(x(I))^2*(x(H))^2*E33-3*(x(I))^2*x(H)*y(H)--3*x(I)*(x(H))^2*y(I)-(x(H))^3*y(I)))/E27&quot; à -7.13742, 1.43763 </Expression>
<Function name="f5" n="41" color="2" type="thick" hidden="super" showname="true" bold="true" large="true" ctag0="red" cexpr0="1" x="x" y="E28*x^3+E29*x^2+E30*x+E31" var="x" min="x(H)" max="x(I)" d="0.1" shape="cross">Fonction ( x , E28*x^3+E29*x^2+E30*x+E31 )</Function>
<Function name="f6" n="42" color="2" type="thick" showname="true" target="true" bold="true" large="true" x="x" y="if(x&lt;x(E),f1(x),if(x&lt;x(F),f2(x),if(x&lt;x(G),f3(x),if(x&lt;x(H),f4(x),f5(x)))))" var="x" min="x(D)" max="x(I)" d="0.1" shape="cross">Fonction ( x , if(x&lt;x(E),f1(x),if(x&lt;x(F),f2(x),if(x&lt;x(G),f3(x),if(x&lt;x(H),f4(x),f5(x))))) )</Function>
</Objects>
</Macro>
</CaR>
